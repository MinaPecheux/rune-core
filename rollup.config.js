const path = require("path");
const commonjs = require("@rollup/plugin-commonjs");
const { nodeResolve } = require("@rollup/plugin-node-resolve");
const { terser } = require("rollup-plugin-terser");
const postcss = require("rollup-plugin-postcss");

module.exports = [
  {
    input: "src/lib/index.js",
    output: [
      {
        file: "dist/rune-core.umd.js",
        format: "cjs"
      }, {
        file: "dist/rune-core.min.js",
        format: "iife",
        name: "Rune",
        plugins: [terser()]
      }
    ],
    plugins: [
      nodeResolve(),
      commonjs(),
      postcss({
        extract: path.resolve("dist/styles/main.css")
      })
    ]
  },

  {
    input: "src/lib/index.js",
    external: ["dagre"],
    output: [
      {
        file: "dist/rune-core.cjs.js",
        format: "cjs"
      },
      {
        file: "dist/rune-core.esm.js",
        format: "es"
      }
    ]
  }
];
