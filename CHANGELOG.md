# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.9.3](https://gitlab.com/MinaPecheux/rune-core/compare/v1.9.2...v1.9.3) (2021-02-18)


### Bug Fixes

* **graph:** add i/o in node data export ([03de33e](https://gitlab.com/MinaPecheux/rune-core/commit/03de33e8b1479726e5a94e960999acc4e6f93700))

### [1.9.2](https://gitlab.com/MinaPecheux/rune-core/compare/v1.9.1...v1.9.2) (2021-02-15)


### Bug Fixes

* **geometry:** make drawClosedSpline a ctx function ([3571ebd](https://gitlab.com/MinaPecheux/rune-core/commit/3571ebd9a07571d78e21642866bd42e12a2a51a8))
* **stroke:** extract drawGeometry function + fix node stroking ([7e04055](https://gitlab.com/MinaPecheux/rune-core/commit/7e04055b6aa7bcf6ce0a47a69eac7f0b508dcb61))

### [1.9.1](https://gitlab.com/MinaPecheux/rune-core/compare/v1.9.0...v1.9.1) (2021-02-15)


### Bug Fixes

* **icon:** allow for image src or svg paths for node icons ([ae3aef8](https://gitlab.com/MinaPecheux/rune-core/commit/ae3aef87a63973ff6b920fc9af53770fa900bc48))

## [1.9.0](https://gitlab.com/MinaPecheux/rune-core/compare/v1.8.0...v1.9.0) (2021-01-27)


### Features

* **execution:** handle async node exec functions + auto update node statuses ([a5d1a8c](https://gitlab.com/MinaPecheux/rune-core/commit/a5d1a8ce0958fa6ba46ee1935958052399c9dad4))
* **nodes:** add basic/api-caller node ([52e5a1a](https://gitlab.com/MinaPecheux/rune-core/commit/52e5a1a405191d450c32c8f4ae8382f4f2f886be))
* **utils:** add getRandomInt helper ([054fefb](https://gitlab.com/MinaPecheux/rune-core/commit/054fefb303522ba99c2e349a81e218c5376e1513))


### Bug Fixes

* **canvas:** fix title box render check ([735d405](https://gitlab.com/MinaPecheux/rune-core/commit/735d40551b3f9ccf6072278028ec20497bedda58))
* **core:** clean ups + add run button to toolbar ([9baac02](https://gitlab.com/MinaPecheux/rune-core/commit/9baac0236a2767d6046b515faf96daeb81d61dd0))

## [1.8.0](https://gitlab.com/MinaPecheux/rune-core/compare/v1.7.2...v1.8.0) (2020-12-20)


### Features

* **canvas:** add snake links for ROUNDED_LINK type ([86d196c](https://gitlab.com/MinaPecheux/rune-core/commit/86d196c6fe67303e2bf8942747634b14c00ec005))
* **docs:** add examples on node shapes+bulge+node colors ([3e92bdd](https://gitlab.com/MinaPecheux/rune-core/commit/3e92bddaa71645c6d06bc32b12fb65eb9d481adc))
* **layouts:** add nrows/ncols to grid layout ([eaf8d37](https://gitlab.com/MinaPecheux/rune-core/commit/eaf8d3773098763a3cd8a53ac7c619ef6b083447))
* **shapes:** add BULGE_SHAPE with node_bulge option + remove POINT_SHAPE ([f15f0f8](https://gitlab.com/MinaPecheux/rune-core/commit/f15f0f8cb66ff1ffcd605a26f935c4e5eef8ff20))


### Bug Fixes

* **canvas:** center node title for BULGE_SHAPE ([3155be6](https://gitlab.com/MinaPecheux/rune-core/commit/3155be6b38a7fd91ef10d8f35356ef9f10f7c1f2))
* **canvas:** fix snake links arrow direction + inverted ROUNDED_LINK ([7e936e6](https://gitlab.com/MinaPecheux/rune-core/commit/7e936e6588409832738b1c9acfb7d587e5c3f872))
* **canvas:** hide widgets for POINT_SHAPE by default ([c2e9de6](https://gitlab.com/MinaPecheux/rune-core/commit/c2e9de6a73620a74b222d065c05acad16045d09f))
* **docs:** add favicon+hero image+top bar logo ([c057af0](https://gitlab.com/MinaPecheux/rune-core/commit/c057af03c7474b30199518cc1e8c8d43c60a12fb))
* **docs:** add search bar + small improvements in examples ([d55fc0d](https://gitlab.com/MinaPecheux/rune-core/commit/d55fc0dfee0c0bf8dd4a5a7f0e1b5c5b0394fb22))
* **docs:** fix relative src import paths ([033d185](https://gitlab.com/MinaPecheux/rune-core/commit/033d1853cf71ebb4fb0df03f0f506277e336d6e3))
* **nodes:** fix BULGE_SHAPE position w.r.t. anchor + add offset to selection_style ([5824b14](https://gitlab.com/MinaPecheux/rune-core/commit/5824b14993018bfb30493ab6cc3b8848320143bf))

### [1.7.2](https://gitlab.com/MinaPecheux/rune-core/compare/v1.7.1...v1.7.2) (2020-12-08)


### Bug Fixes

* **layouts:** improve grid layout with snapping ([122b2dc](https://gitlab.com/MinaPecheux/rune-core/commit/122b2dc225f295bf0cd4e3a50841baba07e8ea66))
* **layouts:** small fix for grid layout undefined vars ([7aeabcb](https://gitlab.com/MinaPecheux/rune-core/commit/7aeabcba4b520f99633b9678018b5198ee9b71fb))

### [1.7.1](https://gitlab.com/MinaPecheux/rune-core/compare/v1.7.0...v1.7.1) (2020-12-08)


### Bug Fixes

* **canvas:** fix STRAIGHT_LINK and ROUNDED_LINK mid pos ([175f086](https://gitlab.com/MinaPecheux/rune-core/commit/175f0860b568c086c32720d7df9bd2bf67ba9065))
* **layouts:** fix grid layout if multiple node groups ([ce2a283](https://gitlab.com/MinaPecheux/rune-core/commit/ce2a283073a01807e351c296ef60461336c0a881))

## [1.7.0](https://gitlab.com/MinaPecheux/rune-core/compare/v1.6.4...v1.7.0) (2020-12-07)


### Features

* **canvas:** add ROUNDED_LINK link type ([2004c47](https://gitlab.com/MinaPecheux/rune-core/commit/2004c470cf244b24195824f7c46a49e6124d0c8c))


### Bug Fixes

* **canvas:** fix for image icon drawing ([03fe2b3](https://gitlab.com/MinaPecheux/rune-core/commit/03fe2b39594ced8c5785b5f747c35f6728aab598))
* **canvas:** improve ROUNDED_LINK points placement ([56a4649](https://gitlab.com/MinaPecheux/rune-core/commit/56a4649ca9069ea4c1f7a6cb3ee2783871efd546))
* **canvas:** use options for grid snapping on mouseup ([09227b8](https://gitlab.com/MinaPecheux/rune-core/commit/09227b8749c3d87dd81f3a7e618301f7e18aa7f6))

### [1.6.4](https://gitlab.com/MinaPecheux/rune-core/compare/v1.6.3...v1.6.4) (2020-12-07)


### Bug Fixes

* **canvas:** add some parameters to onDropItem callback function ([f88e6b2](https://gitlab.com/MinaPecheux/rune-core/commit/f88e6b29af512c952e014176f1a4f31a0d3c54a7))
* **canvas:** make toolbar enabled by default ([43bf534](https://gitlab.com/MinaPecheux/rune-core/commit/43bf534515ca6c943685ca77cf3ea2e5db1972a7))
* **graph:** add option to ignore link creation on node insert ([03acb2c](https://gitlab.com/MinaPecheux/rune-core/commit/03acb2c5c59723d624dcb3e91d7ee4cf6548e6bd))
* **icon:** load icons from images + small improvements in icon placement ([41d7c02](https://gitlab.com/MinaPecheux/rune-core/commit/41d7c0249a5367c58cc8011683fd6f0c4e9834f3))
* **layouts:** remove invalid options in dagre layout computation ([eeaffcf](https://gitlab.com/MinaPecheux/rune-core/commit/eeaffcfe918b20ee6fc849273f8fe251b05e1608))
* **node:** improve placement of icon+slots for POINT_SHAPE ([a903e5a](https://gitlab.com/MinaPecheux/rune-core/commit/a903e5a623d54b770f52d38ed19f41ae8c84670f))

### [1.6.3](https://gitlab.com/MinaPecheux/rune-core/compare/v1.6.2...v1.6.3) (2020-12-03)


### Bug Fixes

* **tags:** avoid registering an already-registered or undefined tag ([59d827b](https://gitlab.com/MinaPecheux/rune-core/commit/59d827b5376eb41837d35a53a468ceb5f600531c))

### [1.6.2](https://gitlab.com/MinaPecheux/rune-core/compare/v1.6.1...v1.6.2) (2020-11-29)


### Bug Fixes

* **canvas:** add infinite option to background grid ([47571f4](https://gitlab.com/MinaPecheux/rune-core/commit/47571f4b58cf5c387e767050c72fadbf82c3b3fe))
* **canvas:** combine background_color and background_grid ([f9bee1b](https://gitlab.com/MinaPecheux/rune-core/commit/f9bee1b28b24d9fc7194d3e7ac693f968dc33736))

### [1.6.1](https://gitlab.com/MinaPecheux/rune-core/compare/v1.6.0...v1.6.1) (2020-11-29)

## [1.6.0](https://gitlab.com/MinaPecheux/rune-core/compare/v1.5.1...v1.6.0) (2020-11-29)


### Features

* **canvas:** add background grid patterns ([02392eb](https://gitlab.com/MinaPecheux/rune-core/commit/02392ebdf417eb6c01ad96303ce4d7896c7a1b10))
* **canvas:** add basic toolbar ([bd5bed7](https://gitlab.com/MinaPecheux/rune-core/commit/bd5bed7f91c59afefa06bae83c3624f16fc96d9d))


### Bug Fixes

* **canvas:** avoid error on zoom ([dc5ff21](https://gitlab.com/MinaPecheux/rune-core/commit/dc5ff2139a71d01459ae2ede815c91fb46df59b7))

## [1.5.0](https://gitlab.com/MinaPecheux/rune-core/compare/v1.4.0...v1.5.0) (2020-11-20)


### Features

* **canvas:** add horizontal snake links ([c5e2de1](https://gitlab.com/MinaPecheux/rune-core/commit/c5e2de1381b6f693635484a598c549ba35e8640d))
* **canvas:** add styling option for link tooltips ([fad8c26](https://gitlab.com/MinaPecheux/rune-core/commit/fad8c265c4725497d0684bfc6044e999a8d94afa))


### Bug Fixes

* **docs:** update docs with callbacks ([8343eae](https://gitlab.com/MinaPecheux/rune-core/commit/8343eae48f39f2bf7a3999210307bec8d56ba313))

## [1.4.0](https://gitlab.com/MinaPecheux/rune-core/compare/v1.3.2...v1.4.0) (2020-11-20)


### Features

* **canvas:** add custom styling for link middle point ([b824dea](https://gitlab.com/MinaPecheux/rune-core/commit/b824dea42c86233bdf95a14a055a80ece3bbdf33))
* **canvas:** add options for selection marker styling ([e730aea](https://gitlab.com/MinaPecheux/rune-core/commit/e730aeacc8f272d514ac635b63eef1011b1ebcf6))
* **layouts:** add basic grid layout for sequential graphs ([3bf9f64](https://gitlab.com/MinaPecheux/rune-core/commit/3bf9f6419790f1686fb7d5824984c0ae9e583b6e))

### [1.3.2](https://gitlab.com/MinaPecheux/rune-core/compare/v1.3.1...v1.3.2) (2020-11-19)


### Bug Fixes

* **graph:** remove overwrite of getNodes function ([834a6a0](https://gitlab.com/MinaPecheux/rune-core/commit/834a6a085f1fde73af2686181d566f4b48ed57da))

### [1.3.1](https://gitlab.com/MinaPecheux/rune-core/compare/v1.3.0...v1.3.1) (2020-11-19)


### Bug Fixes

* **canvas:** small fix in slot variables ([50d9c0f](https://gitlab.com/MinaPecheux/rune-core/commit/50d9c0f7b00eac5c2f80905168be656837ee523f))
* **misc:** various fixes and additional utils ([4791089](https://gitlab.com/MinaPecheux/rune-core/commit/47910897c540897baa47e1aed3e138d2bb7965db))

## [1.3.0](https://gitlab.com/MinaPecheux/rune-core/compare/v1.2.0...v1.3.0) (2020-11-18)


### Features

* **basic:** add Icon example node ([6348103](https://gitlab.com/MinaPecheux/rune-core/commit/634810315c9d20f8918cec2189039f80c898ea4b))
* **graph:** add insertNode + tryReconnect functions ([c9683ed](https://gitlab.com/MinaPecheux/rune-core/commit/c9683ed8fe8c144ed11641d7356259d42a95683c))


### Bug Fixes

* **basic:** add onConnectInput handler to Output node type ([9a773d4](https://gitlab.com/MinaPecheux/rune-core/commit/9a773d4fb0f35d8f7898048f67f8ca9067a5f1c6))
* **canvas:** fix icon overall position + POINT_SHAPE slots + node stroke ([2af59dd](https://gitlab.com/MinaPecheux/rune-core/commit/2af59dd68e80829d4fc6f0fea05df38dc149409a))
* **canvas:** fix render_slots option usage ([4438d47](https://gitlab.com/MinaPecheux/rune-core/commit/4438d478ece72fcbd6100596b037bf56af231207))
* **canvas:** fix render_title_label to use user value if any ([9cca2e2](https://gitlab.com/MinaPecheux/rune-core/commit/9cca2e254cff3be6e7d14cbc983003a79130f1b2))
* **lib:** fix point shape in collapsed mode ([f00ee32](https://gitlab.com/MinaPecheux/rune-core/commit/f00ee32b20cf6af31f8595cf6fdb818418ca2380))
* **node:** improve node initialization + node insertion ([1ba11ab](https://gitlab.com/MinaPecheux/rune-core/commit/1ba11ab4ec6fb391e6a9bec6460e4bc38d7d212c))

## [1.2.0](https://gitlab.com/MinaPecheux/rune-core/compare/v1.1.1...v1.2.0) (2020-11-18)


### Features

* **canvas:** add more customization options ([c1d2c74](https://gitlab.com/MinaPecheux/rune-core/commit/c1d2c74ed3abc81c36cb74ade55cda41ee98423b))
* **canvas:** add onShowLinkMenu callback ([3042577](https://gitlab.com/MinaPecheux/rune-core/commit/304257725774af6484c85c87812271b995952d18))


### Bug Fixes

* **docs:** add doc about edge ports ([9f8dcc0](https://gitlab.com/MinaPecheux/rune-core/commit/9f8dcc06daa32fbb007a1ee84c3aa43de73541f9))
* **docs:** add rune-vue doc links ([617e267](https://gitlab.com/MinaPecheux/rune-core/commit/617e267dfe4f865e0eb76491f285c29190c5424d))
* **lib:** various fixes ([e3fe8f1](https://gitlab.com/MinaPecheux/rune-core/commit/e3fe8f17ee53bd0babd1ace0146c04972986d03e))

### [1.1.1](https://gitlab.com/MinaPecheux/rune-core/compare/v1.1.0...v1.1.1) (2020-11-17)


### Bug Fixes

* **config:** set umd as main entrypoint ([2428aaf](https://gitlab.com/MinaPecheux/rune-core/commit/2428aaf6387c9d3839deec00e4f23be64f4eab05))

## [1.1.0](https://gitlab.com/MinaPecheux/rune-core/compare/v1.0.1...v1.1.0) (2020-11-17)


### Features

* **docs:** add vuepress doc ([731d033](https://gitlab.com/MinaPecheux/rune-core/commit/731d0335e9dd000e8a6388e08f8418fc4d4c4ac1))
* **graph:** use passed tag or autoregister basic tag ([f3792c2](https://gitlab.com/MinaPecheux/rune-core/commit/f3792c2be08fa81b9620bc4f2701d6d07535d106))
* **lib:** export enums ([458099d](https://gitlab.com/MinaPecheux/rune-core/commit/458099d30f20b1d5b8f85f4e502d9a11ba913c5a))


### Bug Fixes

* **docs:** remove fixed cdn version ([bdda8bc](https://gitlab.com/MinaPecheux/rune-core/commit/bdda8bc915044cf81e9e79cf2257af5e1c80ff71))
* **graph:** inject data-based positions in nodes if any ([a69b6e2](https://gitlab.com/MinaPecheux/rune-core/commit/a69b6e216e77be1e6c0af2c2a6674089ef51b545))
* **graph:** separate initializer in populateFromNodesAndEdges ([8595428](https://gitlab.com/MinaPecheux/rune-core/commit/8595428d1bea1e46c2cddf7c19170464172583f2))

### 1.0.1 (2020-11-17)
