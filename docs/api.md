# Rune core API

This page describes the various options that can be passed to the canvas or the layout computors.

## Graph options

Rune provides lots of customization options for your graph at a global canvas level. Those can be passed to your `Rune.Canvas` instance and have the following defaults (colors are themed with a "light" and "dark" color mode):

```js
{
  anchor_point: "middle",
  font_family: "Arial",
  title_text_size: NODE_TEXT_SIZE,
  inner_text_size: NODE_SUBTEXT_SIZE,
  node_title_height: NODE_TITLE_HEIGHT,
  background_color: COLORS[color_mode].BACKGROUND_COLOR,
  background_grid: {
    enabled: false,
    type: "line",
    line_width: 1,
    dot_size: 2,
    cross_size: 4,
    major_ticks_size: 32, // in px
    major_ticks_color: "rgba(0, 0, 0, 0.05)",
    infinite: true
  },
  snap_grid: {
    enabled: false,
    size: CANVAS_GRID_SIZE,
  },
  toolbar: {
    enabled: true,
    placement: "bottom right",
    bgcolor: COLORS[color_mode].NODE_DEFAULT_COLOR,
    text_color: COLORS[color_mode].NODE_TEXT_COLOR,
    vertical: false,
    drag: true,
    zoom_in: true,
    zoom_out: true,
    fit_to_screen: true,
    reset: true,
    actions: []
  },
  node_default_color: COLORS[color_mode].NODE_DEFAULT_COLOR,
  node_default_bgcolor: COLORS[color_mode].NODE_DEFAULT_BGCOLOR,
  node_default_boxcolor: COLORS[color_mode].NODE_DEFAULT_BOXCOLOR,
  node_text_color: COLORS[color_mode].NODE_TEXT_COLOR,
  node_title_color: COLORS[color_mode].NODE_TITLE_COLOR,
  node_selected_title_color: COLORS[color_mode].NODE_SELECTED_TITLE_COLOR,
  default_link_color: COLORS[color_mode].LINK_COLOR,
  highlighted_link_color: null,
  default_highlighted_link_color: COLORS[color_mode].HIGHLIGHTED_LINK_COLOR,
  node_box_outline_color: COLORS[color_mode].NODE_BOX_OUTLINE_COLOR,
  event_link_color: COLORS[color_mode].EVENT_LINK_COLOR,
  connecting_link_color: COLORS[color_mode].CONNECTING_LINK_COLOR,
  widget_outline_color: COLORS[color_mode].WIDGET_OUTLINE_COLOR,
  widget_bgcolor: COLORS[color_mode].WIDGET_BGCOLOR,
  widget_text_color: COLORS[color_mode].WIDGET_TEXT_COLOR,
  widget_secondary_text_color: COLORS[color_mode].WIDGET_SECONDARY_TEXT_COLOR,

  node_border: null,
  title_mode: NORMAL_TITLE,

  highquality_render: true,
  use_gradients: false, //set to true to render titlebar with gradients
  editor_alpha: 1, //used for transition
  pause_rendering: false,
  clear_background: true,

  read_only: false, //if set to true users cannot modify the graph
  show_contextual_menu: true,
  render_only_selected: true,
  live_mode: false,
  show_info: false,
  allow_dragcanvas: true,
  allow_dragnodes: true,
  allow_zoom_on_wheel: false,
  allow_interaction: true, //allow to control widgets, buttons, collapse, etc
  allow_searchbox: true,
  allow_reconnect_links: false, //allows to change a connection with having to redo it again
  allow_link_interaction: true,
  allow_snake_links: true, //allows to draw links that go back to the left is path is too weirdly stretch
  render_links_arrows: true, //adds arrows to show flow direction on snake links
  snake_link_bend_start_offset_x: 8,
  snake_link_bend_mid_offset_x: 48,

  drag_mode: false,
  dragging_rectangle: null,

  filter: null, //allows to filter to only accept some type of nodes in a graph

  set_canvas_dirty_on_mouse_event: false, //forces to redraw the canvas if the mouse does anything
  always_render_background: false,
  render_shadows: true,
  render_shadows_if_collapsed: false,
  render_canvas_border: true,
  render_connections_shadows: false, //too much cpu
  render_connections_border: false,
  render_curved_connections: false,
  render_connection_arrows: false,
  render_slots: true,
  render_slot_labels: true,
  render_collapsed_slots: true,
  render_selection_marker: true,
  render_execution_order: false,
  render_title_colored: true,
  render_link_tooltip: true,

  links_render_mode: SPLINE_LINK,
  links_corner_radius_size: 16,
  link_middle_point_radius: 5,
  link_middle_point_label: "",
  link_middle_point_label_font_size: 14,
  link_middle_point_style: {
    fill: COLORS[color_mode].LINK_COLOR,
    stroke_color: "transparent",
    line_width: 0
  },
  link_tooltip_style: {
    font: "Arial",
    font_size: 14,
    with_point: true,
    with_arrow: true,
    text_color: COLORS[color_mode].LINK_COLOR,
    fill: COLORS[color_mode].BACKGROUND_COLOR,
    stroke_color: "transparent",
    line_width: 1,
    shape: ROUND_SHAPE,
  },
  link_tooltip_text: "", // global tooltip to display on all link middle point hover

  connections_width: 2,
  node_round_radius: 8,
  node_bulge: 0.5
}
```

## Layout options

The layout options depend on the type of layout you pick.

### Grid

| Property   | Description                      | Type   | Default       |
| ---------- | -------------------------------- | ------ | ------------- |
| `paddingx` | Horizontal padding for the graph | Number | 0             |
| `paddingy` | Vertical padding for the graph   | Number | 0             |
| `ranksep`  | Spacing between node ranks (i.e. nodes of different depth-levels)       | Number | 100          |
| `nodesep`  | Preferred spacing between nodes of the same rank (i.e. nodes with the same depth-level) - can be overwritten to insure the graph fits in the view  | Number | 100       |
| `ncols`  | User-specified number of columns (overrides all other parameters)  | Number | null       |
| `nrows`  | User-specified number of rows  | Number | null       |

### Dagre

| Property   | Description                      | Type   | Default       |
| ---------- | -------------------------------- | ------ | ------------- |
| `rankdir`  | Global graph direction           | String | `LR`          |
| `marginx`  | Top margin for the graph         | Number | 0             |
| `marginy`  | Left margin for the grap         | Number | 0             |
| `ranksep`  | Spacing between node ranks (i.e. nodes of different depth-levels)       | Number | 100          |
| `nodesep`  | Spacing between nodes of the same rank (i.e. nodes with the same depth-level)  | Number | 100       |

## Callbacks

Rune provides some global callbacks:

- `onNodeTypeRegistered()`: called whenever a new type of node is registered in Rune
- `onNodeTypeReplaced()`: called whenever a new type of node replaces a previous one in Rune

Rune's classes also have plenty of callback you can override to pass in your own functions and easily react to specific events from the graph. Below are lists of callbacks per type of objects.

### Graph

| Function name             | Triggered when                                                         |
| ------------------------- | ---------------------------------------------------------------------- |
| `onRemoved()`             | You remove the node from the graph (either manually or through script) |
| `onSelected()`            | You select the node (either manually or through script)                |
| `onDeselected()`          | You deselect the node (either manually or through script)              |
| `onGetInputs()`           | The node received new inputs                                           |
| `onGetOutputs()`          | The node received new outputs                                          |
| `onNodeAdded()`           | A node is added somewhere in the graph                                 |
| `onNodeRemoved()`         | A node is removed somewhere from the graph                             |
| `onInputAdded()`          | An input is added somewhere in the graph                               |
| `onInputRemoved()`        | An input is removed somewhere from the graph                           |
| `onInputRenamed()`        | An input has been renamed somewhere in the graph                       |
| `onInputTypeChanged()`    | An input type is modified somewhere in the graph                       |
| `onOutputAdded()`         | An output is added somewhere in the graph                              |
| `onOutputRemoved()`       | An output is removed somewhere from the graph                          |
| `onOutputTypeChanged()`   | An output type is modified somewhere in the graph                      |
| `onOutputRenamed()`       | An output has been renamed somewhere in the graph                      |
| `onInputsOutputsChange()` | Either inputs or outputs were updated somewhere in the graph           |
| `onBeforeChange()`        | Just before any change is applied on the graph                         |
| `onAfterChange()`         | Just after any change is applied on the graph                          |
| `onTrigger()`             | You call the `graph.trigger()` function                                |
| `onConnectionChange()`    | A connection changed somewhere in the graph                            |
| `onNodeConnectionChange()`| (same as `onConnectionChange()`)                                       |
| `onNodeTrace()`           | You call the `node.trace()` function                                   |
| `onChange()`              | Something changed visually in the graph                                |
| `onSerialize()`           | You call the `graph.serialize()` function                              |
| `onConfigure()`           | The graph was configured (through the `graph.configure()` function)    |

### Canvas

| Function name             | Triggered when                                                  |
| ------------------------- | --------------------------------------------------------------- |
| `onPlayEvent()`           | The graph starts running                                        |
| `onStopEvent()`           | The graph stops running                                         |
| `onBeforeStep()`          | Just before an execution step is processed                      |
| `onAfterStep()`           | Just after an execution step has been processed                 |
| `onCanvasClick()`         | You click on the canvas (outside of a node or an edge)          |
| `onShowLinkMenu()`<sup>*</sup>       | You click on the handler in the middle of a link                | 
| `onDrawLinkTooltip()`     | You hover the handler in the middle of a link                   |
| `onUndrawLinkTooltip()`   | You unhover the handler in the middle of a link                 |

<sup>*</sup> If your function returns `true`, then the default behavior will be chained after your custom action and the link menu will be opened (else the default behavior will be ignored).

### Node

| Function name             | Triggered when                                                         |
| ------------------------- | ---------------------------------------------------------------------- |
| `onAdded()`               | You add the node in the graph (either manually or through script)      |
| `onRemoved()`             | You remove the node from the graph (either manually or through script) |
| `onSelected()`            | You select the node (either manually or through script)                |
| `onDeselected()`          | You deselect the node (either manually or through script)              |
| `onGetInputs()`           | The node received new inputs                                           |
| `onGetOutputs()`          | The node received new outputs                                          |
| `onTrigger()`             | You call the `graph.triggerInput()` function                           |
| `onExecute()`             | The node is executed in the graph computation                          |
| `onExecuteStep()`         | The node is executed in a step of the graph computation                |
| `onAfterExecute()`        | After the node has been executed in the graph computation              |
| `onAction()`              | The node receives an action trigger                                    |
| `onInputAdded()`          | An input is added to the node                                          |
| `onInputRemoved()`        | An input is removed from the node                                      |
| `onInputRenamed()`        | An input has been renamed for the node                                 |
| `onInputTypeChanged()`    | An input type is modified for the node                                 |
| `onOutputAdded()`         | An output is added to the node                                         |
| `onOutputRemoved()`       | An output is removed from the node                                     |
| `onOutputTypeChanged()`   | An output type is modified for the node                                |
| `onOutputRenamed()`       | An output has been renamed for the node                                |
| `onConnectInput()`        | An inputs is connected to the node                                     |
| `onPropertyChanged()`     | The value of a property of the node has been modified                  |
| `onConnectionsChanged()`  | The node connections were modified                                     |
| `onGetPropertyInfo()`     | You get the node property with the `getPropertyInfo()` function        |
| `onResize()`              | The node size changed (through the `node.setSize()` function)          |
| `onBounding()`            | The node bounding is computed                                          |
| `onSerialize()`           | The node is serialized (probably during graph serialization)           |
| `onConfigure()`           | The node was configured (through the `node.configure()` function)      |
