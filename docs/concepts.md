# Main concepts

## Nodes, edges and tags

To create a graph with Rune, you need to define your data as an array of nodes and an array of edges. There can be no edges (in which case the graph is actually a bunch of nodes next to each other with no connection whatsoever). Nodes must have a unique `id` in the graph and this `id` is used to describe edges.

Nodes must also have a `type`: this is a string that references one of the registered node types. These node types can be organized using "tags":

- one node type can have one or more tags
- whenever you create a new graph instance, you provide it the tag of the sets of nodes you want to load and use in this graph (by default, the loaded nodes are the ones tagged as `basic`)

Whenever you create a graph, you load a tag to get all the associated nodes.

## Node types

### The `basic` tag

If you are satisfied with the default style and you only need basic operations, then you can take advantage of the basic nodes offered by Rune. All those nodes are tagged `basic` (there are in the `basic` scope, i.e. their node type string references start with `basic/`). Here is the list at the moment - these is a selection of [Litegraph.js's default nodes](https://github.com/jagenjo/litegraph.js/tree/master/src/nodes):

- I/O operations:
  - `basic/input-array`: a node with an array-text widget to allow the user to input an array
  - `basic/input-boolean`: a node with a toggle widget to allow the user to input a boolean
  - `basic/input-json`: a node with a json-text widget to allow the user to input a Json object
  - `basic/input-number`: a node with a number slider widget to allow the user to input a number
  - `basic/input-string`: a node with a text widget to allow the user to input a string
  - `basic/output`: a node that displays the value of its input
  - `basic/api-caller`: a node that calls an API to retrieve a result asynchronously
- data extraction operations:
  - `basic/get-array-element`: a node to extract an element at a given index from an array
  - `basic/get-2d-array-element`: a node to extract an element at a given row and column from a 2d array
  - `basic/get-object-keys`: a node to extract the list of keys of an object
  - `basic/get-object-property`: a node to extract a given property from an object
- math operations:
  - `basic/math-operation`: a node to perform various basic math operation (+, -, *, ÷...)
  - `basic/math-condition`: a node to evaluate a simple math condition between two numbers (<, >, =, ≤...)
  - `basic/clamp-number`: a node that clamps a number value between a min and a max
  - `basic/lerp-number`: a node that lerps a number between two values with a given ratio
  - `basic/random-number`: a node that computes a random number within a range

### Creating your own node types

Rune comes with a set of predefined node types - however, you can also easily define your own by creating a node type-class and registering it in your Rune environment.

First, to define your new node type, create a Javascript function. In the function, you can set inputs, outputs, properties, widgets... and then, you can add functions to the node (either custom ones or event handlers that are used by Rune at specific moments):

```js
function AffineFunction() {
  this.addInput("x", "number");
  this.addOutput("f", "number");
  const initial_a = 0.5;
  const initial_b = 0;
  this.addProperty("a", initial_a);
  this.addProperty("b", initial_b);
  this.widget = this.addWidget("number", "a", initial_a, "a");
  this.widget = this.addWidget("number", "b", initial_b, "b");
  this.widgets_up = true;
}

AffineFunction.prototype.onExecute = function() {
  let x = this.getInputData(0);
  if (x == null) {
    x = 0;
  }
  const { a, b } = this.properties;
  const f = a * x + b;
  this.setOutputData(0, f);
};

AffineFunction.title = "Affine function";
AffineFunction.desc = "Computes an affine function";
```

::: tip

If you use Rune in a NodeJS environment, you'll probably want to isolate this definition in its own Javascript file, for example `affine-function.js`. In that case, export the function as the default export at the end of the script:

```js
function AffineFunction() { ... }

export default AffineFunction;
```

:::

::: details Custom node types

There will soon be more details about writing custom node types. In the meantime, you can refer to the [Litegraph.js](https://github.com/jagenjo/litegraph.js/tree/master#how-to-code-a-new-node-type) Github repo for some basic pointers.

:::

### Registering and using the node type

To actually use your new node type, you need to register it in a given tag. To do this, create a Javascript for your tag; for example, `custom.js`. Then, in this file, simply import your newly created node type and create an initializing function in it. This function should export your node with the string reference you want for this node (this reference should be prefixed by your tag to easily scope your nodes in the contextual menus).

The function can receive a specific context for "dynamic" tags. For example, if you want to register node types for a list of items you don't know beforehand, you can loop through your items in the initializing function and it will be applied specifically to the items you pass in the context at the actual moment of initialization.

```js
function initializeCustom(context = {}) {
  return {
    'custom/affine-function': AffineFunction
  };
}
```

::: tip

Like before, if you use Rune in a NodeJS environment, you'll probably want to use separate files. In that case, import your previous function and re-export the initialization function as the default export of a new script, for example `custom.js`:

```js
import AffineFunction from './affine-function.js';

export default function initialize(context = {}) {
  return {
    'custom/affine-function': AffineFunction
  };
};
```

:::

Now, to use your custom tag in a graph, simply pass it to the constructor as a `tag` object. This object must contain two keys `name` and `initializer`:

```js
const graph = new Rune.Graph({
  tag: {
    name: 'custom',
    initializer: initializeCustom
  }
});
```

The tag object can also have two additional optional keys:

- `context`: to pass on a custom initialization context to the initializing function
- `clear_others`: to only keep the nodes with this tag and unregister all others

::: tip

If you're in a NodeJS environment and you isolated the definitions in several files, import the tag from its definition file:

```js
import initializeCustom from './rune-tags/custom.js';
```

:::

### Contextualized node types

You can make your node type a higher-order function to pass it specific properties from your context at the moment of initialization. For example, we can improve our current `AffineFunction` node to accept variable initial `a` and `b` parameters:

```js
function AffineFunctionFactory(context) {
  function AffineFunction() {
    this.addInput('x', 'number');
    this.addOutput('f', 'number');
    const initial_a = context.a || 0.5;
    const initial_b = context.b || 0;
    this.addProperty('a', initial_a);
    this.addProperty('b', initial_b);
    this.widget = this.addWidget('number', 'a', initial_a, 'a');
    this.widget = this.addWidget('number', 'b', initial_b, 'b');
    this.widgets_up = true;
  }

  AffineFunction.prototype.onExecute = function() {
    let x = this.getInputData(0);
    if (x == null) {
      x = 0;
    }
    const { a, b } = this.properties;
    const f = a * x + b;
    this.setOutputData(0, f);
  };

  AffineFunction.title = 'Affine function';
  AffineFunction.desc = 'Computes an affine function';

  return AffineFunction;
}
```

This function can now be called to instantiate a node type with our custom context (in `custom.js`):

```js
function initializeCustom(context = {}) {
  const { a, b } = context;
  return {
    'custom/affine-function': AffineFunctionFactory({ a, b })
  };
}
```

## Additional node properties

The required properties for a node are its unique `id` in the graph and its `type` (the string reference to the node type). However, you can provide several additional properties to have a custom instantiation of your node type:

- `pos`: if you're in the "data" layout mode (see the next section for more info), this is necessary and determines the initial position (in pixels) of your node on the canvas
- `options`: an object containing overrides for various data on the node:
  - `properties`: to pass on (and/or override) the node data properties (this data can be silently conveyed with no visual cues or displayed in widgets)
  - `shape`: to override the node type shape
  - `point_size`: if your node has a "point" shape, this can override the default `POINT_SIZE` variable (30 pixels) and set the node size

## Running API calls

If you use the `basic/api-caller` node type (or one derived from it), you will be able to directly call an API via the [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch): by providing a URL and a process function (plus optionally a specific method and custom headers), you will update the `value` property of the node based on the API response.

When using an API caller node, you need to provide two keys to the node's custom `options` dict: `url` and `processResult`. The `url` is the URL to call and the `processResult` is a synchronous function that receives the API JSON response and returns the processed value for the node.

The default method is `GET` and the default headers convey API calls data as JSON objects.

Here is an example of a simple API caller:

```js
{
  id: "example-GOT-api-caller",
  type: "basic/api-caller",
  options: {
    url: "https://www.anapioficeandfire.com/api/books",
    processResult: (result) => {
      const nItems = result.length;
      return result[getRandomInt(0, nItems)].name;
    }
  }
}
```

## Linking nodes with edges

When you link nodes, you use edges that have a "source" and a "target". The source is the unique graph id of the node the link starts from and the target is the unique graph id of the node the link goes to. So a basic edge description is an object that looks like:

```js
{ source: 'node-0', target: 'node-1' }
```

You can also supply `source_port` and `target_port` keys if you want the connection to go specifically from (resp. to) a given port on the source (resp. target) node. For example, if you have a node that sums its inputs, then you will want one edge to enter through port 0 and another to enter through port 1:

```js
edges = [
  { source: 'input-0', target: 'sum-node', target_port: 0 },
  { source: 'input-1', target: 'sum-node', target_port: 1 }
]
```

::: tip

By default, both `source_port` and `target_port` are equal to 0, so if you don't worry about ports you'll get a simple sequential and linear graph using nodes with 1 input and 1 output.

:::

## Layouts

When they are instantiated, nodes must be assigned a position. This can be done either by hand, by adding a `pos` field to your nodes, or automatically, by using a pre-defined layout mechanism.

To use manual info, you only need to add a `pos` field in each of your nodes that defines a 2D array with two numbers, the x- and y-position of your node (in pixels). This layout will be read automatically by the lib when nodes are instantiated.

To use a pre-defined layout (or to override the data-based positions), you can use the lib's layout-related functions:

- `getGridLayout(graphData, width, height, options = {})`: computes a basic grid layout for your nodes and edges by examining the edges one by one and posing the nodes sequentially from top to bottom and left to right
  
  ::: warning

  For now, this feature only works for "linear" graphs, meaning graphs where all nodes have at most one input and/or one output.

  :::

- `getDagreLayout(graphData, options = {})`: computes a well-balanced directed graph layout for your nodes and edges (with the [`dagre` package](https://github.com/dagrejs/dagre))
- `centerLayout(layout, width, height)`: recenters the computed positions to have them anchored in the middle of the canvas
- `applyLayout(nodes, layout)`: actually updates the graph nodes with the computed positions

## Graph and layout options

For more details on the available graph and layout options, take a look at the [`Rune` API doc](./api.md).
