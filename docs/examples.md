# Examples

This page presents a small selection of more advanced examples or configuration options to show some additional features or provide details on core concepts.

::: tip

In the following examples, we suppose that we have an `index.html` and a `script.js` similar to the ones described in the [Getting started](./guide.md) guide.

:::

## Light/dark theme

By default, a Rune graph is instantiated with a light theme. However, you can easily switch to a dark theme by providing the `color_mode` ("light" or "dark") option to the canvas.

:::: tabs

::: tab Result
<Demo component-name="examples-dark-theme" />
:::


::: tab "Javascript code"
```js
const { width, height } = document.querySelector("container").getBoundingClientRect();
// instantiate the graph in the html canvas
const graph = new Rune.Graph();
const canvas = new Rune.Canvas("#graph", graph, { color_mode: "dark" });
// define the nodes and edges data
const data = {
  nodes: [
    { id: "node-0", type: "basic/input-number" },
    { id: "node-1", type: "basic/output" }
  ],
  edges: [
    { source: "node-0", target: "node-1" }
  ]
};
graph.populateFromNodesAndEdges(data);
let layout = Rune.layouts.getDagreLayout(data);
layout = Rune.layouts.centerLayout(layout, width, height);
Rune.layouts.applyLayout(graph.getNodes(), layout);
```
:::

::::

## Node shapes: basic info

Rune currently offers 4 node shapes that contain more or less information:


| Shape         | Node title | Widgets | Width & height*  |
| ------------- | ---------- | ------- | ---------------- |
| `BOX_SHAPE`   | Included   | Yes     | Yes              |
| `ROUND_SHAPE` | Included   | Yes     | Yes              |
| `CARD_SHAPE`  | Included   | Yes     | Yes              |
| `BULGE_SHAPE` | Excentred  | No      | No               |

<small>&ast; if the type has both a width and an height, then you can specify a 2d-int array for its `size` property to set width and height separately; if the type does not distinguish between the two horizontal/vertical sizes, then use its `point_size` property</small>

The `BULGE_SHAPE` can easily be turned into a circle shape using the node's `node_bulge` property (see the next example for more details).

If none is specified in the node constructor, the default node shape is the `BOX_SHAPE`.

:::: tabs

::: tab Result
<Demo component-name="examples-node-shapes" />
:::


::: tab "Javascript code"
```js
const data = {
  nodes: [
    {
      id: "node-0",
      type: "basic/random-number",
      options: {
        title: "Box",
        shape: Rune.shapes.BOX_SHAPE,
        show_widgets: false,
        size: [100, 30],
      }
    },
    {
      id: "node-1",
      type: "basic/random-number",
      options: {
        title: "Round",
        shape: Rune.shapes.ROUND_SHAPE,
        show_widgets: false,
        size: [100, 30],
      }
    },
    {
      id: "node-2",
      type: "basic/random-number",
      options: {
        title: "Card",
        shape: Rune.shapes.CARD_SHAPE,
        show_widgets: false,
        size: [100, 30],
      }
    },
    {
      id: "node-3",
      type: "basic/random-number",
      options: {
        title: "Bulge",
        shape: Rune.shapes.BULGE_SHAPE,
        point_size: 60
      }
    },
    {
      id: "node-4",
      type: "basic/random-number",
      options: {
        title: "Bulge (circle)",
        shape: Rune.shapes.BULGE_SHAPE,
        point_size: 60,
        node_bulge: 1
      }
    }
  ],
  edges: [ ... ]
};
```
:::

::::
## Node shapes: bulge parameter

If you use the `BULGE_SHAPE` for your node, then you can use its `node_bulge` parameter to define the tension in the edges (i.e. how curved they are). The bulge parameter ranges from 0 to 1: 0 means you get straight lines and 1 means you get a circle.

By default, the `node_bulge` is 0.5.

:::: tabs

::: tab Result
<Demo component-name="examples-node-shapes-bulge" />
:::


::: tab "Javascript code"
```js
const data = {
  nodes: [
    {
      id: "node-0",
      type: "basic/random-number",
      options: {
        title: "bulge = 0",
        shape: Rune.shapes.BULGE_SHAPE,
        point_size: 60,
        node_bulge: 0
      }
    },
    {
      id: "node-1",
      type: "basic/random-number",
      options: {
        title: "bulge = 0.25",
        shape: Rune.shapes.BULGE_SHAPE,
        point_size: 60,
        node_bulge: 0.25
      }
    },
    {
      id: "node-2",
      type: "basic/random-number",
      options: {
        title: "bulge = 0.5",
        shape: Rune.shapes.BULGE_SHAPE,
        point_size: 60,
        node_bulge: 0.5
      }
    },
    {
      id: "node-3",
      type: "basic/random-number",
      options: {
        title: "bulge = 0.75",
        shape: Rune.shapes.BULGE_SHAPE,
        point_size: 60,
        node_bulge: 0.75
      }
    },
    {
      id: "node-4",
      type: "basic/random-number",
      options: {
        title: "bulge = 1",
        shape: Rune.shapes.BULGE_SHAPE,
        point_size: 60,
        node_bulge: 1
      }
    }
  ],
  edges: [ ... ]
};
```
:::

::::

## Node colors

Since Rune uses HTML behind the scenes, colors can be specified any common format: hexadecimal, RGB (and RGBA) or HSL. There are plenty of variables to set the colors of the various parts of a node, but the one you should probably try out first is `bgcolor`, that sets the global node background color. The following example displays a grid of nodes with varying background colors using HSL ranges.

:::: tabs

::: tab Result
<Demo component-name="examples-node-colors" />
:::


::: tab "Javascript code"
```js
// instantiate the graph in the html canvas
const graph = new Rune.Graph();
const canvas = new Rune.Canvas(this.$refs.graph, graph, {
  read_only: true,
  show_contextual_menu: false,
  allow_searchbox: false,
  render_slots: false,
  render_shadows: false,
  allow_link_interaction: false,
  title_mode: Rune.enums.TRANSPARENT_TITLE,
  point_size: 32,
  node_bulge: 0.6,
  snap_grid: {
    enabled: false,
  },
});
// define the nodes and edges data
let nodes = [];
let globalId = 0;
for (let i = 0; i < 8; i++) {
  const nodesLine = [];
  for (let j = 0; j < 3; j++) {
    nodesLine.push({
      id: `node-${globalId}`,
      type: "basic/random-number",
      options: {
        shape: Rune.shapes.BULGE_SHAPE,
        bgcolor: `hsl(${45 * i}, ${10 + j * 30}%, 50%)`,
      }
    });
    globalId++;
  }
  nodesLine.push({
    id: `node-${globalId}`,
    type: "basic/random-number",
    options: {
      shape: Rune.shapes.BULGE_SHAPE,
      bgcolor: `hsl(${45 * i}, 100%, 50%)`,
    }
  });
  globalId++;
  for (let j = 0; j < 3; j++) {
    nodesLine.push({
      id: `node-${globalId}`,
      type: "basic/random-number",
      options: {
        shape: Rune.shapes.BULGE_SHAPE,
        bgcolor: `hsl(${45 * i}, 100%, ${50 - j * 15}%)`,
      }
    });
    globalId++;
  }
  nodes = nodes.concat(nodesLine);
}
const data = {
  nodes,
  edges: []
};
graph.populateFromNodesAndEdges(data);
let layout = Rune.layouts.getGridLayout(data, width, height, {
  marginx: 0,
  marginy: 8,
  nodesep: 42,
  ranksep: 48,
  ncols: 7,
});
layout = Rune.layouts.centerLayout(layout, width, height);
Rune.layouts.applyLayout(graph.getNodes(), layout);
```
:::

::::

## Defining custom options for nodes

Node types have some inherent properties. However, sometimes, you want to pass in specific options to some of your nodes in the graph for styling, title display... Those should be given in your nodes data in an `options` subkey:

:::: tabs

::: tab Result
<Demo component-name="examples-custom-node-options" />
:::


::: tab "Javascript code"
```js
const data = {
  nodes: [
    {
      id: "node-0",
      type: "basic/random-number",
      options: {
        shape: Rune.shapes.BULGE_SHAPE,
        point_size: 40,
        node_bulge: 1
      }
    },
    {
      id: "node-1",
      type: "basic/output",
      options: {
        render_title_label: false
      }
    },
    ...
  ],
  edges: [ ... ]
};
```
:::

::::

## Using a custom node tag

As explained in the [main concepts page](./concepts.md), Rune relies on a system of tags to provide and group node types. One graph instance uses one node tag; one node tag might (theoretically) contain an illimited number of node types; node types can be registered in one or more tags.

To specify which tag to use in your graph, you can provide a `tag` object when you create your `Graph` instance:

```js
const graph = new Rune.Graph({
  tag: {
    name: 'custom',
    initializer: customInitialize,
    context: { a: 10, b: 5 },
    clear_others: true
  }
});
```

The `tag` definition contains the following keys:

- `name`: the name of the registered tag
- `initializer`: the initialization function to run when registering the tag
- `context` (optional): if necessary, a specific context to use at the moment of initialization (see the [concepts doc](./concepts.md) for more details)
- `clear_others` (optional): if true, all other tags (and their associated node types) will be unregistered

## Using canvas callbacks

The `Canvas` object has some available callbacks to help you interact better with the graph. Each of them can be defined simply by adding the function to your `Canvas` instance:

```js
const canvas = new Rune.Canvas("#graph", graph);
...
canvas.onCanvasClick = () => {
  console.log('clicked on canvas background!');
}
```

The available callbacks are listed in the [API detailed doc](./api.md).

## Defining a custom node type with an icon

If you want, you can create a custom node type with an icon. This icon is an image that can is loaded when the graph is instantiated. To define such a custom type, use the `icon` field with the image relative path:

```js
MyNode.icon = "./relative/path/to/my/image.jpg";
```

This icon can be any loadable image type: jpeg, png...

::: warning

The path is relative to the location of the graph instantiator, i.e. the HTML, Vue, React, etc. file that loads up the lib and creates the graph.

:::

By default, the icon is created with a 32x32 pixel size. If you want to specify your own size, use an object instead of a simple string:

```js
MyNode.icon = {
  src: "./relative/path/to/my/image.jpg",
  width: 40,
  height: 40
};
```
