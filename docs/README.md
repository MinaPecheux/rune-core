---
home: true
heroImage: /logo-500x500.png
actionText: Get Started →
actionLink: /guide
features:
  - title: 🔵 Super-simple data
    details: Create graphs from just a list of nodes and a list of edges :)
  - title: 🟠 A basic JS module
    details: Use it as a vanilla JS lib, from a CDN, in frontend frameworks...
  - title: 🟣 Styling & customization
    details: Define your own node types and CSS styling to suit your needs
footer: 2020 © Mina Pêcheux - MIT License
---
