module.exports = {
  base: '/rune-core/',
  dest: 'public',

  locales: {
    '/': {
      lang: 'en-US',
      title: 'Rune Core',
      description: 'A lightweight Javascript library for drawing graphs and flows'
    }
  },

  head: [
    ['link', { rel: 'icon', href: '/logo-64x64.png' }]
  ],

  plugins: [ 'tabs' ],

  themeConfig: {
    repoLabel: 'Contribute!',
    repo: 'https://gitlab.com/MinaPecheux/rune-core',
    docsDir: 'docs',
    logo: '/logo-64x64.png',
    editLinks: true,
    docsBranch: 'dev',
    editLinkText: 'Help us improve this page!',
    themeConfig: {
      search: true,
      searchMaxSuggestions: 10
    },
    displayAllHeaders: true,
    locales: {
      '/': {
        label: 'English',
        selectText: 'Languages',
        lastUpdated: 'Last Updated',
        // service worker is configured but will only register in production
        serviceWorker: {
          updatePopup: {
            message: 'New content is available.',
            buttonText: 'Refresh'
          }
        },
        nav: [
          { text: 'Getting Started', link: '/guide' },
          { text: 'Concepts', link: '/concepts' },
          { text: 'API', link: '/api/' },
          { text: 'Examples', link: '/examples/' }
        ]
      }
    },
    sidebar: [
      '/',
      '/guide',
      '/concepts',
      '/api',
      '/examples'
    ]
  }
}
