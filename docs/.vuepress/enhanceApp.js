import RuneCore from "../../src/lib";

export default ({ Vue }) => {
  Vue.prototype.$rune = RuneCore;
};
