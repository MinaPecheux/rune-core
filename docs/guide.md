# Getting Started

Rune is based on the [Litegraph.js](https://github.com/jagenjo/litegraph.js) JS library: it relies on the same core mechanisms but extends it to provide additional features and in particular more styling/CSS customization!

## Using Node JS and frontend frameworks

It can be used directly in a basic HTML/Javascript vanilla app, or with frontend frameworks. For now, the following frontend frameworks are supported:

- VueJS: [Rune Vue](https://minapecheux.gitlab.io/rune-vue/) is a basic Vue plugin wrapped around this lib

If you want to import the Node JS lib version directly, make sure to require the proper export:

```js
const RuneCore = require("@mpecheux/rune-core/dist/rune-core.umd");

// or:
import RuneCore from "@mpecheux/rune-core/dist/rune-core.umd";
```

To check it has loaded fine, you can debug the current version of the lib:

```js
console.log(RuneCore.VERSION) // 1
```

## Direct vanilla usage

If you are in a plain HTML/Javascript context and are not using any frontend framework (such as VueJS or ReactJS), you can still display your graph easily in a canvas. Simply use the CDN script (or [download it](https://unpkg.com/@mpecheux/rune-core/dist/rune-core.min.js) and use it locally): you will then have access to a global `Rune` object that contains the entire library.

You should also import or download the CSS stylesheet.

Here is a basic Rune example:

- first, create your `index.html` file:
  
  ```html
  <html>
    <head>
      <link rel="stylesheet" href="https://unpkg.com/@mpecheux/rune-core/dist/styles/main.css">
      <script src="https://unpkg.com/@mpecheux/rune-core/dist/rune-core.min.js"></script>
      <script src="./script.js"></script>
    </head>
    <body>
      <canvas id="graph" width="600" height="200"></canvas>
    </body>
  </html>
  ```

- and then add in the Javascript `script.js`:
  
  ```js
  window.onload = () => {
    // instantiate the graph in the html canvas
    const graph = new Rune.Graph();
    const canvas = new Rune.Canvas("#graph", graph);

    // define the nodes and edges data
    const data = {
      nodes: [
        { id: "node-0", type: "basic/input-number" },
        { id: "node-1", type: "basic/output" }
      ],
      edges: [
        { source: "node-0", target: "node-1" }
      ]
    };

    graph.populateFromNodesAndEdges(data);

    let layout = Rune.layouts.getDagreLayout(data);
    layout = Rune.layouts.centerLayout(layout, 600, 200);
    Rune.layouts.applyLayout(graph.getNodes(), layout);
  };
  ```

And you're done!

This creates the following graph with a basic input-to-output connection. You can interact with it using the toolbar on the bottom right. If you change the value of the input by dragging the slider widget, you can then re-execute the graph to update the label of the linked output node.

::: tip

Right-click the graph to add a new node or edit an already-existing one!

:::

<Demo component-name="examples-getting-started" />

## Author

Rune is a JS library by [Mina Pêcheux](https://www.minapecheux.com). The project started in November 2020.
