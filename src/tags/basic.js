const InputArray = require("../nodes/basic/input-array.js");
const InputBoolean = require("../nodes/basic/input-boolean.js");
const InputJson = require("../nodes/basic/input-json.js");
const InputNumber = require("../nodes/basic/input-number.js");
const InputString = require("../nodes/basic/input-string.js");
const Output = require("../nodes/basic/output.js");
const ApiCaller = require("../nodes/basic/api-caller.js");

const GetArrayElement = require("../nodes/basic/get-array-element.js");
const Get2dArrayElement = require("../nodes/basic/get-2d-array-element.js");
const GetObjectKeys = require("../nodes/basic/get-object-keys.js");
const GetObjectProperty = require("../nodes/basic/get-object-property.js");

const MathOperation = require("../nodes/basic/math-operation.js");
const MathCondition = require("../nodes/basic/math-condition.js");
const ClampNumber = require("../nodes/basic/clamp-number.js");
const LerpNumber = require("../nodes/basic/lerp-number.js");
const RandomNumber = require("../nodes/basic/random-number.js");

// eslint-disable-next-line no-unused-vars
module.exports = function(context = {}) {
  return {
    "basic/input-array": InputArray,
    "basic/input-boolean": InputBoolean,
    "basic/input-json": InputJson,
    "basic/input-number": InputNumber,
    "basic/input-string": InputString,
    "basic/output": Output,
    "basic/api-caller": ApiCaller,

    "basic/get-array-element": GetArrayElement,
    "basic/get-2d-array-element": Get2dArrayElement,
    "basic/get-object-keys": GetObjectKeys,
    "basic/get-object-property": GetObjectProperty,

    "basic/math-operation": MathOperation,
    "basic/math-condition": MathCondition,
    "basic/clamp-number": ClampNumber,
    "basic/lerp-number": LerpNumber,
    "basic/random-number": RandomNumber
  };
};
