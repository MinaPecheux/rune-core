const { getTitle, setValue } = require("../helpers/functions.js");

function InputNumber() {
  this.addOutput("value", "number");
  this.addProperty("value", 1.0);
  this.widget = this.addWidget("number", "value", 1, "value");
  this.widgets_up = true;
  this.size = [180, 30];
}

InputNumber.title = "Input Number";
InputNumber.desc = "Input of number type";

InputNumber.prototype.onExecute = function() {
  this.setOutputData(0, parseFloat(this.properties.value));
};

InputNumber.prototype.getTitle = function() {
  return getTitle.bind(this)();
};
InputNumber.prototype.setValue = function(v) {
  setValue.bind(this)(v);
};

InputNumber.prototype.onDrawBackground = function() {
  //show the current value
  this.outputs[0].label = this.properties.value.toFixed(2);
};

module.exports = InputNumber;
