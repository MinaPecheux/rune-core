const { RUNNING_STATUS, DONE_STATUS, ERROR_STATUS } = require("../../lib/helpers/enums.js");
const { getDefaultAPICallHeaders } = require("../../lib/config.js");
const { logNodeError } = require("../../lib/helpers/logger.js");
function ApiCaller() {
  this.value = null;
  this.addInput("value", 0, { label: "" });
  this.url = "https://www.anapioficeandfire.com/api/books";
  this.processResult = null;
}

ApiCaller.title = "ApiCaller";
ApiCaller.desc = "Calls an API to retrieve a result";

ApiCaller.prototype.onExecute = async function(graph) {
  if (!this.url) {
    logNodeError(this, "no URL specified (set the 'url' property of this node to allow it to execute)");
  }
  if (!this.processResult) {
    logNodeError(this, "no processing function specified (set the 'processResult' property of this node to allow it to execute)");
  }
  if (window.fetch) {
    const queryConfig = getDefaultAPICallHeaders();
    if (this.method) queryConfig.method = this.method;
    if (this.headers) queryConfig.headers = this.headers;
    this.setStatus(RUNNING_STATUS);
    graph.setDirtyCanvas(true, false);
    try {
      const response = await fetch(this.url, queryConfig);
      if (response.ok) {
        const jsonResp = await response.json();
        this.value = this.processResult(jsonResp);
        this.setStatus(DONE_STATUS);
      } else {
        this.setStatus(ERROR_STATUS);
      }
    } catch (error) {
      console.log("[ERROR] Invalid fetch: " + error.message);
      this.setStatus(ERROR_STATUS);
    }
  } else {
    console.log("[ERROR] Could not use the Fetch API");
  }
};

ApiCaller.prototype.getTitle = function() {
  if (this.flags.collapsed) {
    return this.inputs[0].label;
  }
  return this.title;
};

ApiCaller.toString = function(o) {
  if (o == null) {
    return "null";
  } else if (o.constructor === Number) {
    return o.toFixed(2);
  } else if (o.constructor === Array) {
    var str = "[";
    for (var i = 0; i < o.length; ++i) {
      str += ApiCaller.toString(o[i]) + (i + 1 != o.length ? "," : "");
    }
    str += "]";
    return str;
  } else {
    return String(o);
  }
};

ApiCaller.prototype.onDrawBackground = function() {
  //show the current value
  this.inputs[0].label = ApiCaller.toString(this.value);
};

module.exports = ApiCaller;
