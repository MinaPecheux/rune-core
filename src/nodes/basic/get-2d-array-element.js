function Get2dArrayElement() {
  this.addInput("table", "table");
  this.addInput("row", "number");
  this.addInput("col", "number");
  this.addOutput("value", "");
  this.addProperty("row", 0);
  this.addProperty("column", 0);
}

Get2dArrayElement.title = "Array[row][col]";
Get2dArrayElement.desc = "Returns an element from a 2d array";

Get2dArrayElement.prototype.onExecute = function() {
  let table = this.getInputData(0);
  let row = this.getInputData(1);
  let col = this.getInputData(2);
  if (row == null) row = this.properties.row;
  if (col == null) col = this.properties.column;
  if (table == null || row == null || col == null) return;
  row = table[Math.floor(Number(row))];
  if (row) this.setOutputData(0, row[Math.floor(Number(col))]);
  else this.setOutputData(0, null);
};

module.exports = Get2dArrayElement;
