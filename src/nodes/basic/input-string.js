const { getTitle, setValue } = require("../helpers/functions.js");

function InputString() {
  this.addOutput("value", "string");
  this.addProperty("value", "");
  this.widget = this.addWidget("text", "value", "", "value"); //link to property value
  this.widgets_up = true;
  this.size = [180, 30];
}

InputString.title = "Input String";
InputString.desc = "Input of boolean type";
InputString.prototype.getTitle = function() {
  return getTitle.bind(this)();
};

InputString.prototype.onExecute = function() {
  this.setOutputData(0, this.properties["value"]);
};

InputString.prototype.setValue = function(v) {
  setValue.bind(this)(v);
};

InputString.prototype.onDropFile = function(file) {
  var $this = this;
  var reader = new FileReader();
  reader.onload = function(e) {
    $this.setProperty("value", e.target.result);
  };
  reader.readAsText(file);
};

module.exports = InputString;
