function RandomNumber() {
  this.addOutput("value", "number");
  this.addProperty("min", 0);
  this.addProperty("max", 1);
  this.widget = this.addWidget("number", "min", 0, "min");
  this.widget = this.addWidget("number", "max", 1, "max");
  this.widgets_up = true;
  this.size = [80, 30];
}

RandomNumber.title = "Random number";
RandomNumber.desc = "Computed a random number within a range";

RandomNumber.prototype.onExecute = function() {
  if (this.inputs) {
    for (var i = 0; i < this.inputs.length; i++) {
      var input = this.inputs[i];
      var v = this.getInputData(i);
      if (v === undefined) {
        continue;
      }
      this.properties[input.name] = v;
    }
  }

  var min = this.properties.min;
  var max = this.properties.max;
  this._last_v = Math.random() * (max - min) + min;
  this.setOutputData(0, this._last_v);
};

RandomNumber.prototype.onDrawBackground = function() {
  //show the current value
  this.outputs[0].label = (this._last_v || 0).toFixed(2);
};

RandomNumber.prototype.onGetInputs = function() {
  return [
    ["min", "number"],
    ["max", "number"]
  ];
};

module.exports = RandomNumber;
