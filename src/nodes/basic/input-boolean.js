const { ACTION } = require("../../lib/helpers/enums.js");
const { getTitle, setValue } = require("../helpers/functions.js");

function InputBoolean() {
  this.addOutput("value", "boolean");
  this.addProperty("value", true);
  this.widget = this.addWidget("toggle", "value", true, "value");
  this.widgets_up = true;
  this.size = [140, 30];
}

InputBoolean.title = "Input Boolean";
InputBoolean.desc = "Input of boolean type";
InputBoolean.prototype.getTitle = function() {
  return getTitle.bind(this)();
};

InputBoolean.prototype.onExecute = function() {
  this.setOutputData(0, this.properties["value"]);
};

InputBoolean.prototype.setValue = function(v) {
  setValue.bind(this)(v);
};

InputBoolean.prototype.onGetInputs = function() {
  return [["toggle", ACTION]];
};

InputBoolean.prototype.onAction = function() {
  this.setValue(!this.properties.value);
};

module.exports = InputBoolean;
