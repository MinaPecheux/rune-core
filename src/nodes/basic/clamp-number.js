function ClampNumber() {
  this.addInput("in", "number");
  this.addOutput("out", "number");
  this.size = [80, 30];
  this.addProperty("min", 0);
  this.addProperty("max", 1);
}

ClampNumber.title = "Clamp number";
ClampNumber.desc = "Clamp number between min and max";

ClampNumber.prototype.onExecute = function() {
  var v = this.getInputData(0);
  if (v == null) {
    return;
  }
  v = Math.max(this.properties.min, v);
  v = Math.min(this.properties.max, v);
  this.setOutputData(0, v);
};

ClampNumber.prototype.getCode = function() {
  var code = "";
  if (this.isInputConnected(0)) {
    code +=
      "clamp({{0}}," + this.properties.min + "," + this.properties.max + ")";
  }
  return code;
};

module.exports = ClampNumber;
