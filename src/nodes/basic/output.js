function Output() {
  this.size = [60, 30];
  this.addInput("value", 0, { label: "" });
  this.value = 0;
}

Output.title = "Output";
Output.desc = "Shows the value of its input";

Output.prototype.onExecute = function() {
  if (this.inputs[0]) {
    this.value = this.getInputData(0);
  }
};

Output.prototype.getTitle = function() {
  if (this.flags.collapsed) {
    return this.inputs[0].label;
  }
  return this.title;
};

Output.toString = function(o) {
  if (o == null) {
    return "null";
  } else if (o.constructor === Number) {
    return o.toFixed(2);
  } else if (o.constructor === Array) {
    var str = "[";
    for (var i = 0; i < o.length; ++i) {
      str += Output.toString(o[i]) + (i + 1 != o.length ? "," : "");
    }
    str += "]";
    return str;
  } else {
    return String(o);
  }
};

Output.prototype.onDrawBackground = function() {
  //show the current value
  this.inputs[0].label = Output.toString(this.value);
};

Output.prototype.onConnectInput = function(target_slot, _, output, $this) {
  this.value = $this.properties.value;
  //show the current value
  this.inputs[0].label = Output.toString(this.value);
};

module.exports = Output;
