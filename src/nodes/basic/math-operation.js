const { NODE_TITLE_HEIGHT } = require("../../lib/helpers/defaults.js");
const { getAnchorOffset } = require("../../lib/helpers/utils.js");

function MathOperation() {
  this.addInput("A", "number,array,object");
  this.addInput("B", "number");
  this.addOutput("=", "number");
  this.addProperty("A", 1);
  this.addProperty("B", 1);
  this.addProperty("OP", "+", "enum", { values: MathOperation.values });
  this._func = function(A, B) {
    return A + B;
  };
  this._result = []; //only used for arrays
}

MathOperation.values = ["+", "-", "*", "/", "%", "^", "max", "min"];

MathOperation.title = "Math operation";
MathOperation.desc = "Basic math operators";
MathOperation["@OP"] = {
  type: "enum",
  title: "operation",
  values: MathOperation.values
};
MathOperation.size = [100, 60];

MathOperation.prototype.getTitle = function() {
  if (this.properties.OP == "max" || this.properties.OP == "min")
    return this.properties.OP + "(A,B)";
  return "A " + this.properties.OP + " B";
};

MathOperation.prototype.setValue = function(v) {
  if (typeof v == "string") {
    v = parseFloat(v);
  }
  this.properties["value"] = v;
};

MathOperation.prototype.onDblClick = function() {
  const curFunc = MathOperation.values.indexOf(this.properties.OP);
  const nextFunc = MathOperation.values[(curFunc + 1) % MathOperation.values.length];
  this.properties.OP = nextFunc;
  this.onPropertyChanged("OP");
  return true;
};

MathOperation.prototype.onPropertyChanged = function(name) {
  if (name != "OP") return;
  switch (this.properties.OP) {
  case "+":
    this._func = function(A, B) {
      return A + B;
    };
    break;
  case "-":
    this._func = function(A, B) {
      return A - B;
    };
    break;
  case "x":
  case "X":
  case "*":
    this._func = function(A, B) {
      return A * B;
    };
    break;
  case "/":
    this._func = function(A, B) {
      return A / B;
    };
    break;
  case "%":
    this._func = function(A, B) {
      return A % B;
    };
    break;
  case "^":
    this._func = function(A, B) {
      return Math.pow(A, B);
    };
    break;
  case "max":
    this._func = function(A, B) {
      return Math.max(A, B);
    };
    break;
  case "min":
    this._func = function(A, B) {
      return Math.min(A, B);
    };
    break;
  default:
    console.warn("Unknown operation: " + this.properties.OP);
    this._func = function(A) {
      return A;
    };
    break;
  }
};

MathOperation.prototype.onExecute = function() {
  var A = this.getInputData(0);
  var B = this.getInputData(1);
  if (A != null) {
    if (A.constructor === Number) this.properties["A"] = A;
  } else {
    A = this.properties["A"];
  }

  if (B != null) {
    this.properties["B"] = B;
  } else {
    B = this.properties["B"];
  }

  var result;
  if (A.constructor === Number) {
    result = 0;
    result = this._func(A, B);
  } else if (A.constructor === Array) {
    result = this._result;
    result.length = A.length;
    for (var i = 0; i < A.length; ++i) result[i] = this._func(A[i], B);
  } else {
    result = {};
    for (const i in A) result[i] = this._func(A[i], B);
  }
  this.setOutputData(0, result);
};

MathOperation.prototype.onDrawBackground = function(ctx) {
  if (this.flags.collapsed) {
    return;
  }

  let title_height =
    this.node_title_height ||
    this.constructor.node_title_height ||
    NODE_TITLE_HEIGHT;

  const size = ["w", "middle", "e"].includes(ctx.anchor_point)
    ? [this.size[0], this.size[1] - title_height]
    : this.size;
  const { x: offX, y: offY } = getAnchorOffset(ctx.anchor_point, size);

  ctx.font = "24px Arial";
  ctx.fillStyle = "#666";
  ctx.textAlign = "center";
  ctx.fillText(
    this.properties.OP,
    this.size[0] * 0.5 + offX,
    this.size[1] * 0.5 + offY
  );
  ctx.textAlign = "left";
};

module.exports = MathOperation;
