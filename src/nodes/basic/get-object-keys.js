function GetObjectKeys() {
  this.addInput("obj", "");
  this.addOutput("keys", "array");
  this.size = [140, 30];
}

GetObjectKeys.title = "Object keys";
GetObjectKeys.desc = "Outputs an array with the keys of an object";

GetObjectKeys.prototype.onExecute = function() {
  var data = this.getInputData(0);
  if (data != null) {
    this.setOutputData(0, Object.keys(data));
  }
};

module.exports = GetObjectKeys;
