function MathCondition() {
  this.addInput("A", "number");
  this.addInput("B", "number");
  this.addOutput("true", "boolean");
  this.addOutput("false", "boolean");
  this.addProperty("A", 1);
  this.addProperty("B", 1);
  this.addProperty("OP", ">", "enum", { values: MathCondition.values });
  this.addWidget("combo", "Cond.", this.properties.OP, {
    property: "OP",
    values: MathCondition.values
  });

  this.size = [80, 60];
}

MathCondition.values = [">", "<", "==", "!=", "<=", ">=", "||", "&&"];
MathCondition["@OP"] = {
  type: "enum",
  title: "operation",
  values: MathCondition.values
};

MathCondition.title = "Math condition";
MathCondition.desc = "Basic math condition between A and B";

MathCondition.prototype.getTitle = function() {
  return "A " + this.properties.OP + " B";
};

MathCondition.prototype.onExecute = function() {
  var A = this.getInputData(0);
  if (A === undefined) {
    A = this.properties.A;
  } else {
    this.properties.A = A;
  }

  var B = this.getInputData(1);
  if (B === undefined) {
    B = this.properties.B;
  } else {
    this.properties.B = B;
  }

  var result = true;
  switch (this.properties.OP) {
    case ">":
      result = A > B;
      break;
    case "<":
      result = A < B;
      break;
    case "==":
      result = A == B;
      break;
    case "!=":
      result = A != B;
      break;
    case "<=":
      result = A <= B;
      break;
    case ">=":
      result = A >= B;
      break;
    case "||":
      result = A || B;
      break;
    case "&&":
      result = A && B;
      break;
  }

  this.setOutputData(0, result);
  this.setOutputData(1, !result);
};

module.exports = MathCondition;
