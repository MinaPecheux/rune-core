function GetObjectProperty() {
  this.addInput("obj", "");
  this.addOutput("", "");
  this.addProperty("value", "");
  this.widget = this.addWidget("text", "prop.", "", this.setValue.bind(this));
  this.widgets_up = true;
  this.size = [140, 30];
  this._value = null;
}

GetObjectProperty.title = "Object property";
GetObjectProperty.desc = "Outputs the property of an object";

GetObjectProperty.prototype.setValue = function(v) {
  this.properties.value = v;
  this.widget.value = v;
};

GetObjectProperty.prototype.getTitle = function() {
  if (this.flags.collapsed) {
    return "in." + this.properties.value;
  }
  return this.title;
};

GetObjectProperty.prototype.onPropertyChanged = function(name, value) {
  this.widget.value = value;
};

GetObjectProperty.prototype.onExecute = function() {
  var data = this.getInputData(0);
  if (data != null) {
    this.setOutputData(0, data[this.properties.value]);
  }
};

module.exports = GetObjectProperty;
