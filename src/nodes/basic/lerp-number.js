function LerpNumber() {
  this.addProperty("ratio", 0.5);
  this.widget = this.addWidget("number", "ratio", 0.5, "ratio");
  this.widgets_up = true;
  this.addInput("A", "number");
  this.addInput("B", "number");

  this.addOutput("out", "number");
  this.size = [160, 60];
}

LerpNumber.title = "Lerp number";
LerpNumber.desc = "Linear Interpolation";

LerpNumber.prototype.onExecute = function() {
  var v1 = this.getInputData(0);
  if (v1 == null) {
    v1 = 0;
  }
  var v2 = this.getInputData(1);
  if (v2 == null) {
    v2 = 0;
  }

  var f = this.properties.ratio;

  var _f = this.getInputData(2);
  if (_f !== undefined) {
    f = _f;
  }

  this.setOutputData(0, v1 * (1 - f) + v2 * f);
};

LerpNumber.prototype.onGetInputs = function() {
  return [["ratio", "number"]];
};

module.exports = LerpNumber;
