const { setValue } = require("../helpers/functions.js");

function InputJson() {
  this.addOutput("", "");
  this.addProperty("value", "");
  this.widget = this.addWidget("text", "json", "", "value");
  this.widgets_up = true;
  this.size = [140, 30];
  this._value = null;
}

InputJson.title = "Input Json";
InputJson.desc = "Input of json type";

InputJson.prototype.onPropertyChanged = function(_, value) {
  this.widget.value = value;
  if (value == null || value == "") {
    return;
  }

  try {
    this._value = JSON.parse(value);
    this.boxcolor = "#AEA";
  } catch (err) {
    this.boxcolor = "red";
  }
};

InputJson.prototype.onExecute = function() {
  this.setOutputData(0, this._value);
};

InputJson.prototype.setValue = function(v) {
  setValue.bind(this)(v);
};

module.exports = InputJson;
