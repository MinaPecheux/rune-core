function getTitle() {
  if (this.flags.collapsed) {
    return this.properties.value;
  }
  return this.title;
}

function setValue(v) {
  this.setProperty("value", v);
}

module.exports = {
  getTitle,
  setValue
};
