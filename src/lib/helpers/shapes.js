//shapes are used for nodes but also for slots
module.exports = {
  BOX_SHAPE: 1,
  ROUND_SHAPE: 2,
  CIRCLE_SHAPE: 3,
  CARD_SHAPE: 4,
  ARROW_SHAPE: 5,
  BULGE_SHAPE: 6,

  VALID_SHAPES: ["default", "box", "round", "card", "bulge"]
};
