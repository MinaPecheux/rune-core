function logNodeError(node, message) {
  console.log(`[ERROR] ${node.title} - ${message}`);
}

module.exports = {
  logNodeError
};
