const { getOptions } = require("../config.js");
const { EVENT, ACTION } = require("./enums.js");

function compareObjects(a, b) {
  for (let i in a) {
    if (a[i] != b[i]) {
      return false;
    }
  }
  return true;
}

function cloneObject(obj, target) {
  if (obj == null) {
    return null;
  }
  var r = JSON.parse(JSON.stringify(obj));
  if (!target) {
    return r;
  }

  for (let i in r) {
    target[i] = r[i];
  }
  return target;
}

/**
 * Returns if the types of two slots are compatible (taking into account wildcards, etc)
 * @method isValidConnection
 * @param {String} type_a
 * @param {String} type_b
 * @return {Boolean} true if they can be connected
 */
function isValidConnection(type_a, type_b) {
  if (
    !type_a || //generic output
    !type_b || //generic input
    type_a == type_b || //same type (is valid for triggers)
    (type_a == EVENT && type_b == ACTION)
  ) {
    return true;
  }

  // Enforce string type to handle toLowerCase call (-1 number not ok)
  type_a = String(type_a);
  type_b = String(type_b);
  type_a = type_a.toLowerCase();
  type_b = type_b.toLowerCase();

  // For nodes supporting multiple connection types
  if (type_a.indexOf(",") == -1 && type_b.indexOf(",") == -1) {
    return type_a == type_b;
  }

  // Check all permutations to see if one is valid
  var supported_types_a = type_a.split(",");
  var supported_types_b = type_b.split(",");
  for (let i = 0; i < supported_types_a.length; ++i) {
    for (let j = 0; j < supported_types_b.length; ++j) {
      if (supported_types_a[i] == supported_types_b[j]) {
        return true;
      }
    }
  }

  return false;
}

function closeAllContextMenus(ref_window) {
  ref_window = ref_window || window;

  var elements = ref_window.document.querySelectorAll(".runecontextmenu");
  if (!elements.length) {
    return;
  }

  var result = [];
  for (let i = 0; i < elements.length; i++) {
    result.push(elements[i]);
  }

  for (let i = 0; i < result.length; i++) {
    if (result[i].close) {
      result[i].close();
    } else if (result[i].parentNode) {
      result[i].parentNode.removeChild(result[i]);
    }
  }
}

//used to create nodes from wrapping functions
function getParameterNames(func) {
  return (func + "")
    .replace(/[/][/].*$/gm, "") // strip single-line comments
    .replace(/\s+/g, "") // strip white space
    .replace(/[/][*][^/*]*[*][/]/g, "") // strip multi-line comments  /**/
    .split("){", 1)[0]
    .replace(/^[^(]*[(]/, "") // extract the parameters
    .replace(/=[^,]+/g, "") // strip any ES6 defaults
    .split(",")
    .filter(Boolean); // split & filter [""]
}

/**
 * Wrapper to load files (from url using fetch or from file using FileReader)
 * @method fetchFile
 * @param {String|File|Blob} url the url of the file (or the file itself)
 * @param {String} type an string to know how to fetch it: "text","arraybuffer","json","blob"
 * @param {Function} on_complete callback(data)
 * @param {Function} on_error in case of an error
 * @return {FileReader|Promise} returns the object used to
 */
function fetchFile(url, type, on_complete, on_error) {
  if (!url) return null;

  type = type || "text";
  if (url.constructor === String) {
    if (url.substr(0, 4) == "http" && getOptions().proxy) {
      url = getOptions().proxy + url.substr(url.indexOf(":") + 3);
    }
    return fetch(url)
      .then(function(response) {
        if (!response.ok) throw new Error("File not found"); //it will be catch below
        if (type == "arraybuffer") return response.arrayBuffer();
        else if (type == "text" || type == "string") return response.text();
        else if (type == "json") return response.json();
        else if (type == "blob") return response.blob();
      })
      .then(function(data) {
        if (on_complete) on_complete(data);
      })
      .catch(function(error) {
        console.error("error fetching file:", url);
        if (on_error) on_error(error);
      });
  } else if (url.constructor === File || url.constructor === Blob) {
    var reader = new FileReader();
    reader.onload = function(e) {
      var v = e.target.result;
      if (type == "json") v = JSON.parse(v);
      if (on_complete) on_complete(v);
    };
    if (type == "arraybuffer") return reader.readAsArrayBuffer(url);
    else if (type == "text" || type == "json") return reader.readAsText(url);
    else if (type == "blob") return reader.readAsBinaryString(url);
  }
  return null;
}

function getAnchorOffset(anchor, size) {
  let x = 0;
  let y = 0;
  switch (anchor) {
  case "nw":
    y += size[1];
    break;
  case "n":
    x -= size[0] * 0.5;
    y += size[1];
    break;
  case "ne":
    x -= size[0];
    y += size[1];
    break;
  case "w":
    y -= size[1] * 0.5;
    break;
  case "middle":
    x -= size[0] * 0.5;
    y -= size[1] * 0.5;
    break;
  case "e":
    x -= size[0];
    y -= size[1] * 0.5;
    break;
  case "sw":
    y -= size[1];
    break;
  case "s":
    x -= size[0] * 0.5;
    y -= size[1];
    break;
  case "se":
    x -= size[0];
    y -= size[1];
    break;
  default:
    break;
  }
  return { x, y };
}

function getFirstMatchingIndices(
  list1,
  list2,
  ignored_values = [],
  allmatching_values = []
) {
  for (let i = 0; i < list1.length; i++) {
    for (let j = 0; j < list2.length; j++) {
      if (
        !ignored_values.includes(list1[i]) &&
        !ignored_values.includes(list2[j]) &&
        (list1[i] === list2[j] ||
          allmatching_values.includes(list1[i]) ||
          allmatching_values.includes(list2[j]))
      ) {
        return [i, j];
      }
    }
  }
  return [-1, -1];
}

/* COLORS */
function colorToString(c) {
  return (
    "rgba(" +
    Math.round(c[0] * 255).toFixed() +
    "," +
    Math.round(c[1] * 255).toFixed() +
    "," +
    Math.round(c[2] * 255).toFixed() +
    "," +
    (c.length == 4 ? c[3].toFixed(2) : "1.0") +
    ")"
  );
}

//Convert a hex value to its decimal value - the inputted hex must be in the
//	format of a hex triplet - the kind we use for HTML colours. The function
//	will return an array with three values.
function hex2num(hex) {
  if (hex.charAt(0) == "#") {
    hex = hex.slice(1);
  } //Remove the '#' char - if there is one.
  hex = hex.toUpperCase();
  var hex_alphabets = "0123456789ABCDEF";
  var value = new Array(3);
  var k = 0;
  var int1, int2;
  for (let i = 0; i < 6; i += 2) {
    int1 = hex_alphabets.indexOf(hex.charAt(i));
    int2 = hex_alphabets.indexOf(hex.charAt(i + 1));
    value[k] = int1 * 16 + int2;
    k++;
  }
  return value;
}

//Give a array with three values as the argument and the function will return
//	the corresponding hex triplet.
function num2hex(triplet) {
  var hex_alphabets = "0123456789ABCDEF";
  var hex = "#";
  var int1, int2;
  for (let i = 0; i < 3; i++) {
    int1 = triplet[i] / 16;
    int2 = triplet[i] % 16;

    hex += hex_alphabets.charAt(int1) + hex_alphabets.charAt(int2);
  }
  return hex;
}

//timer that works everywhere
let _getTime;
if (typeof performance != "undefined") {
  _getTime = performance.now.bind(performance);
} else if (typeof Date != "undefined" && Date.now) {
  _getTime = Date.now.bind(Date);
} else if (typeof process != "undefined") {
  _getTime = function() {
    var t = process.hrtime();
    return t[0] * 0.001 + t[1] * 1e-6;
  };
} else {
  _getTime = function getTime() {
    return new Date().getTime();
  };
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

module.exports = {
  compareObjects,
  cloneObject,
  isValidConnection,
  closeAllContextMenus,
  getParameterNames,
  fetchFile,
  getAnchorOffset,
  getFirstMatchingIndices,
  colorToString,
  hex2num,
  num2hex,
  getRandomInt,
  getTime: _getTime
};
