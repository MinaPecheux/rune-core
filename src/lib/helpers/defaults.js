const { BOX_SHAPE } = require("./shapes.js");
const { RUNNING_STATUS, DONE_STATUS, ERROR_STATUS } = require("./enums.js");

module.exports = {
  CANVAS_GRID_SIZE: 32,
  POINT_SIZE: 30,

  NODE_TITLE_HEIGHT: 30,
  NODE_TITLE_TEXT_Y: 20,
  NODE_SLOT_HEIGHT: 20,
  NODE_WIDGET_HEIGHT: 20,
  NODE_WIDTH: 140,
  NODE_MIN_WIDTH: 50,
  NODE_COLLAPSED_RADIUS: 10,
  NODE_COLLAPSED_WIDTH: 80,
  NODE_TEXT_SIZE: 14,
  NODE_TEXT_COLOR: "#aaa",
  NODE_SUBTEXT_SIZE: 12,
  NODE_DEFAULT_SHAPE: BOX_SHAPE,
  DEFAULT_SHADOW_COLOR: "rgba(0,0,0,0.5)",
  DEFAULT_GROUP_FONT_SIZE: 24,

  RENDER_TEXT: true,
  RENDER_TITLE_BOX: true,

  MAX_NUMBER_OF_NODES: 1000, //avoid infinite loops
  DEFAULT_POSITION: [100, 100], //default node position

  COLORS: {
    dark: {
      BACKGROUND_COLOR: "#111",
      NODE_TITLE_COLOR: "#bbb",
      NODE_TEXT_COLOR: "#bbb",
      NODE_SELECTED_TITLE_COLOR: "#fff",
      NODE_DEFAULT_COLOR: "#333",
      NODE_DEFAULT_BGCOLOR: "#353535",
      NODE_DEFAULT_BOXCOLOR: "#666",
      NODE_BOX_OUTLINE_COLOR: "#fff",
      WIDGET_BGCOLOR: "#222",
      WIDGET_OUTLINE_COLOR: "#666",
      WIDGET_TEXT_COLOR: "#ddd",
      WIDGET_SECONDARY_TEXT_COLOR: "#999",
      LINK_COLOR: "#9a9",
      HIGHLIGHTED_LINK_COLOR: "#bfb",
      EVENT_LINK_COLOR: "#a86",
      CONNECTING_LINK_COLOR: "#afa",
      CONNECTION_COLORS: {
        input_off: "#778",
        input_on: "#7f7",
        output_off: "#778",
        output_on: "#7f7",
        collapsed: "#686"
      },
      NODE_STATUS_COLORS: {
        [RUNNING_STATUS]: "#77f",
        [DONE_STATUS]: "#080",
        [ERROR_STATUS]: "#b00"
      }
    },
    light: {
      BACKGROUND_COLOR: "#fff",
      NODE_TITLE_COLOR: "#222",
      NODE_TEXT_COLOR: "#222",
      NODE_SELECTED_TITLE_COLOR: "#444",
      NODE_DEFAULT_COLOR: "#d2d2d2",
      NODE_DEFAULT_BGCOLOR: "#efefef",
      NODE_DEFAULT_BOXCOLOR: "#666",
      NODE_BOX_OUTLINE_COLOR: "#666",
      WIDGET_BGCOLOR: "#efefef",
      WIDGET_OUTLINE_COLOR: "#888",
      WIDGET_TEXT_COLOR: "#333",
      WIDGET_SECONDARY_TEXT_COLOR: "#444",
      LINK_COLOR: "#99a",
      HIGHLIGHTED_LINK_COLOR: "#bbf",
      EVENT_LINK_COLOR: "#a86",
      CONNECTING_LINK_COLOR: "#aaf",
      CONNECTION_COLORS: {
        input_off: "#877",
        input_on: "#77f",
        output_off: "#877",
        output_on: "#77f",
        collapsed: "#668"
      },
      NODE_STATUS_COLORS: {
        [RUNNING_STATUS]: "#77f",
        [DONE_STATUS]: "#080",
        [ERROR_STATUS]: "#b00"
      }
    }
  }
};
