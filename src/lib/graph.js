// *************************************************************
//   RuneGraph CLASS                                     *******
// *************************************************************

const { getOptions } = require("./config.js");
const RGraphCanvas = require("./canvas.js");
const RGraphGroup = require("./group.js");
const RGraphLink = require("./link.js");
const { RGraphNode, initNode, createNode } = require("./node.js");
const { getRegisteredNodeTypes } = require("./node-types.js");
const { registerTag, defineTag } = require("./tags.js");
const { getTime, getFirstMatchingIndices } = require("./helpers/utils.js");
const {
  MAX_NUMBER_OF_NODES,
  NODE_TITLE_HEIGHT,
  NODE_DEFAULT_SHAPE,
  POINT_SIZE
} = require("./helpers/defaults.js");
const { ALWAYS, ERROR_STATUS, DONE_STATUS, RUNNING_STATUS } = require("./helpers/enums.js");
const { BULGE_SHAPE } = require("./helpers/shapes.js");
const basicTag = require("../tags/basic.js");
const { logNodeError } = require("./helpers/logger.js");

//*********************************************************************************
// RGraph CLASS
//*********************************************************************************

/**
 * RGraph is the class that contain a full graph. We instantiate one and add nodes to it, and then we can run the execution loop.
 * supported callbacks:
    + onNodeAdded: when a new node is added to the graph
    + onNodeRemoved: when a node inside this graph is removed
    + onNodeConnectionChange: some connection has changed in the graph (connected or disconnected)
    *
    * @class RGraph
    * @constructor
    * @param {Object} o data from previous serialization [optional]
    */

function RGraph(o) {
  if (getOptions().debug) {
    console.log("Graph created");
  }
  this.list_of_graphcanvas = null;
  this.clear();

  // register+load required node tag
  let tag = o ? o.tag : null;
  if (!tag) {
    tag = { name: "basic", initializer: basicTag };
  }
  const context = tag.context || {};
  const clear_other_tags = tag.clear_others || false;
  registerTag(tag.name, tag.initializer);
  defineTag(tag.name, context, clear_other_tags);
  
  if (o) {
    this.configure(o);
  }
}

//default supported types
RGraph.supported_types = ["number", "string", "boolean"];

//used to know which types of connections support this graph (some graphs do not allow certain types)
RGraph.prototype.getSupportedTypes = function() {
  return this.supported_types || RGraph.supported_types;
};

RGraph.STATUS_STOPPED = 1;
RGraph.STATUS_RUNNING = 2;

/**
 * Removes all nodes from this graph
 * @method clear
 */

RGraph.prototype.clear = function() {
  this.stop();
  this.status = RGraph.STATUS_STOPPED;

  this.last_node_id = 0;
  this.last_link_id = 0;

  this._version = -1; //used to detect changes

  //safe clear
  if (this._nodes) {
    for (let i = 0; i < this._nodes.length; ++i) {
      var node = this._nodes[i];
      if (node.onRemoved) {
        node.onRemoved();
      }
    }
  }

  //nodes
  this._nodes = [];
  this._nodes_by_id = {};
  this._nodes_in_order = []; //nodes sorted in execution order
  this._nodes_executable = null; //nodes that contain onExecute sorted in execution order

  //other scene stuff
  this._groups = [];

  //links
  this.links = {}; //container with all the links

  //iterations
  this.iteration = 0;

  //custom data
  this.config = {};
  this.vars = {};
  this.extra = {}; //to store custom data

  //timing
  this.globaltime = 0;
  this.runningtime = 0;
  this.fixedtime = 0;
  this.fixedtime_lapse = 0.01;
  this.elapsed_time = 0.01;
  this.last_update_time = 0;
  this.starttime = 0;

  this.catch_errors = true;

  //subgraph_data
  this.inputs = {};
  this.outputs = {};

  //notify canvas to redraw
  this.change();

  this.sendActionToCanvas("clear");
};

/**
 * Attach Canvas to this graph
 * @method attachCanvas
 * @param {GraphCanvas} graph_canvas
 */

RGraph.prototype.attachCanvas = function(graphcanvas) {
  if (graphcanvas.constructor != RGraphCanvas) {
    throw "attachCanvas expects a RGraphCanvas instance";
  }
  if (graphcanvas.graph && graphcanvas.graph != this) {
    graphcanvas.graph.detachCanvas(graphcanvas);
  }

  graphcanvas.graph = this;

  if (!this.list_of_graphcanvas) {
    this.list_of_graphcanvas = [];
  }
  this.list_of_graphcanvas.push(graphcanvas);
};

/**
 * Detach Canvas from this graph
 * @method detachCanvas
 * @param {GraphCanvas} graph_canvas
 */
RGraph.prototype.detachCanvas = function(graphcanvas) {
  if (!this.list_of_graphcanvas) {
    return;
  }

  var pos = this.list_of_graphcanvas.indexOf(graphcanvas);
  if (pos == -1) {
    return;
  }
  graphcanvas.graph = null;
  this.list_of_graphcanvas.splice(pos, 1);
};

/**
 * Populate graph from nodes and edges data. You may also pass in a specific
 * node initializer to extend the default behavior.
 * @method populateFromNodesAndEdges
 * @param {Object} data nodes and edges to populate the graph with
 * @param {Function} initializer [object] custom node initialization behavior
 *                                        (in addition to the default one)
 */
RGraph.prototype.populateFromNodesAndEdges = function(data, initializer) {
  const { nodes, edges } = data;
  const nodes_by_id = {};
  let index = 0;
  for (let node of nodes) {
    // create a new node
    const graphNode = createNode(node.type);
    // init node
    initNode(node, graphNode, index, initializer);

    this.add(graphNode);
    index++;
    nodes_by_id[node.id] = graphNode;
  }
  for (let edge of edges) {
    const { source, target } = edge;
    const source_port = edge.source_port || 0;
    const target_port = edge.target_port || 0;
    nodes_by_id[source].connect(source_port, nodes_by_id[target], target_port);
  }

  return nodes_by_id;
};

/**
 * Starts running this graph every interval milliseconds.
 * @method start
 * @param {number} interval amount of milliseconds between executions, if 0 then it renders to the monitor refresh rate
 */

RGraph.prototype.start = async function(interval = 0) {
  if (this.status == RGraph.STATUS_RUNNING) {
    return;
  }
  this.status = RGraph.STATUS_RUNNING;

  if (this.onPlayEvent) {
    this.onPlayEvent();
  }

  this.sendEventToAllNodes("onStart");

  //launch
  this.starttime = getTime();
  this.last_update_time = this.starttime;
  const $this = this;

  //execute once per frame
  if (
    interval == 0 &&
    typeof window != "undefined" &&
    window.requestAnimationFrame
  ) {
    /* eslint-disable-next-line no-inner-declarations */
    async function on_frame() {
      if ($this.execution_timer_id != -1) {
        return;
      }
      window.requestAnimationFrame(on_frame);
      if ($this.onBeforeStep) $this.onBeforeStep();
      await $this.runStep(1, !$this.catch_errors);
      if ($this.onAfterStep) $this.onAfterStep();
    }
    this.execution_timer_id = -1;
    await on_frame();
  } else {
    //execute every 'interval' ms
    this.execution_timer_id = setInterval(async function() {
      //execute
      if ($this.onBeforeStep) $this.onBeforeStep();
      await $this.runStep(1, !$this.catch_errors);
      if ($this.onAfterStep) $this.onAfterStep();
    }, interval);
  }
};

/**
 * Stops the execution loop of the graph
 * @method stop execution
 */

RGraph.prototype.stop = function() {
  if (this.status == RGraph.STATUS_STOPPED) {
    return;
  }

  this.status = RGraph.STATUS_STOPPED;

  if (this.onStopEvent) {
    this.onStopEvent();
  }

  if (this.execution_timer_id != null) {
    if (this.execution_timer_id != -1) {
      clearInterval(this.execution_timer_id);
    }
    this.execution_timer_id = null;
  }

  this.sendEventToAllNodes("onStop");
};

/**
 * Run N steps (cycles) of the graph
 * @method runStep
 * @param {number} num number of steps to run, default is 1
 * @param {Boolean} do_not_catch_errors [optional] if you want to try/catch errors
 * @param {number} limit max number of nodes to execute (used to execute from start to a node)
 */

RGraph.prototype.runStep = async function(num, do_not_catch_errors, limit) {
  num = num || 1;

  var start = getTime();
  this.globaltime = 0.001 * (start - this.starttime);

  var nodes = this._nodes_executable ? this._nodes_executable : this._nodes;
  if (!nodes) {
    return;
  }

  limit = limit || nodes.length;

  const autoUpdateNodeStatuses = getOptions().auto_update_node_statuses;

  let t0, t1, current_node;
  if (do_not_catch_errors) {
    //iterations
    for (let i = 0; i < num; i++) {
      for (let j = 0; j < limit; ++j) {
        let node = nodes[j];
        if (autoUpdateNodeStatuses) {
          node.setStatus(RUNNING_STATUS);
          this.setDirtyCanvas(true, false);
        }
        t0 = performance.now();
        if (node.mode == ALWAYS && node.onExecute) {
          // await async functions
          if (node.onExecute.constructor.name === "AsyncFunction") {
            await node.onExecute(this);
          }
          // else execute normally
          else {
            node.onExecute();
          }
        }
        t1 = performance.now();
        if (autoUpdateNodeStatuses && node._status === RUNNING_STATUS) {
          node.setStatus(DONE_STATUS);
          this.setDirtyCanvas(true, false);
        }
      }

      this.fixedtime += t1 - t0;
      if (this.onExecuteStep) {
        this.onExecuteStep();
      }
    }

    if (this.onAfterExecute) {
      this.onAfterExecute();
    }
  } else {
    try {
      //iterations
      for (let i = 0; i < num; i++) {
        for (let j = 0; j < limit; ++j) {
          let node = nodes[j];
          current_node = node;
          if (autoUpdateNodeStatuses) {
            node.setStatus(RUNNING_STATUS);
            this.setDirtyCanvas(true, false);
          }
          t0 = performance.now();
          if (node.mode == ALWAYS && node.onExecute) {
            // await async functions
            if (node.onExecute.constructor.name === "AsyncFunction") {
              await node.onExecute(this);
            }
            // else execute normally
            else {
              node.onExecute();
            }
          }
          t1 = performance.now();
          if (autoUpdateNodeStatuses && node._status === RUNNING_STATUS) {
            node.setStatus(DONE_STATUS);
            this.setDirtyCanvas(true, false);
          }
        }

        this.fixedtime += t1 - t0;
        if (this.onExecuteStep) {
          this.onExecuteStep();
        }
      }

      if (this.onAfterExecute) {
        this.onAfterExecute();
      }
      this.errors_in_execution = false;
    } catch (err) {
      this.errors_in_execution = true;
      if (getOptions().throw_errors) {
        if (autoUpdateNodeStatuses) {
          current_node.setStatus(ERROR_STATUS);
          logNodeError(current_node, err);
          this.setDirtyCanvas(true, false);
        }
        throw err;
      }
      if (getOptions().debug) {
        console.log("Error during execution: " + err);
      }
      this.stop();
    }
  }

  var now = getTime();
  var elapsed = now - start;
  if (elapsed == 0) {
    elapsed = 1;
  }
  this.execution_time = 0.001 * elapsed;
  this.globaltime += 0.001 * elapsed;
  this.iteration += 1;
  this.elapsed_time = (now - this.last_update_time) * 0.001;
  this.last_update_time = now;
};

/**
 * Returns all the nodes in the graph
 * @method getNodes
 * @param {Boolean} clean_up [optional] if true, remove all graph-related
 *                                      info to get back pure abstract data
 * @return {Array} an array with all the RGraphNodes in the graph
 */
RGraph.prototype.getNodes = function(clean_up = false) {
  let nodes = this._nodes;
  if (clean_up) {
    nodes = nodes.map((node) => ({
      id: node.id,
      type: node.type,
      properties: node.properties,
      data: node.user_data,
      inputs: node.inputs,
      outputs: node.outputs
    }));
  }
  return nodes;
};
/**
 * Returns all the nodes in the graph
 * @method getEdges
 * @param {Boolean} clean_up [optional] if true, remove all graph-related
 *                                      info to get back pure abstract data
 * @return {Array} an array with all the RGraphLinks in the graph
 */
RGraph.prototype.getEdges = function(clean_up = false) {
  let edges = this.links;
  if (clean_up) {
    edges = Object.keys(edges).map((edgeRef) => {
      const edge = edges[edgeRef];
      return {
        source: edge.origin_id,
        source_port: edge.origin_slot,
        target: edge.target_id,
        target_port: edge.target_slot,
      };
    });
  }
  return edges;
};

/**
 * Updates the graph execution order according to relevance of the nodes (nodes with only outputs have more relevance than
 * nodes with only inputs.
 * @method updateExecutionOrder
 */
RGraph.prototype.updateExecutionOrder = function() {
  this._nodes_in_order = this.computeExecutionOrder(false);
  this._nodes_executable = [];
  for (let i = 0; i < this._nodes_in_order.length; ++i) {
    if (this._nodes_in_order[i].onExecute) {
      this._nodes_executable.push(this._nodes_in_order[i]);
    }
  }
};

//This is more internal, it computes the executable nodes in order and returns it
RGraph.prototype.computeExecutionOrder = function(only_onExecute, set_level) {
  var L = [];
  var S = [];
  var M = {};
  var visited_links = {}; //to avoid repeating links
  var remaining_links = {}; //to a

  //search for the nodes without inputs (starting nodes)
  for (let i = 0, l = this._nodes.length; i < l; ++i) {
    let node = this._nodes[i];
    if (only_onExecute && !node.onExecute) {
      continue;
    }

    M[node.id] = node; //add to pending nodes

    var num = 0; //num of input connections
    if (node.inputs) {
      for (let j = 0, l2 = node.inputs.length; j < l2; j++) {
        if (node.inputs[j] && node.inputs[j].link != null) {
          num += 1;
        }
      }
    }

    if (num == 0) {
      //is a starting node
      S.push(node);
      if (set_level) {
        node._level = 1;
      }
    } //num of input links
    else {
      if (set_level) {
        node._level = 0;
      }
      remaining_links[node.id] = num;
    }
  }

  /* eslint-disable-next-line no-constant-condition */
  while (true) {
    if (S.length == 0) {
      break;
    }

    //get an starting node
    var node = S.shift();
    L.push(node); //add to ordered list
    delete M[node.id]; //remove from the pending nodes

    if (!node.outputs) {
      continue;
    }

    //for every output
    for (let i = 0; i < node.outputs.length; i++) {
      var output = node.outputs[i];
      //not connected
      if (output == null || output.links == null || output.links.length == 0) {
        continue;
      }

      //for every connection
      for (let j = 0; j < output.links.length; j++) {
        var link_id = output.links[j];
        var link = this.links[link_id];
        if (!link) {
          continue;
        }

        //already visited link (ignore it)
        if (visited_links[link.id]) {
          continue;
        }

        var target_node = this.getNodeById(link.target_id);
        if (target_node == null) {
          visited_links[link.id] = true;
          continue;
        }

        if (
          set_level &&
          (!target_node._level || target_node._level <= node._level)
        ) {
          target_node._level = node._level + 1;
        }

        visited_links[link.id] = true; //mark as visited
        remaining_links[target_node.id] -= 1; //reduce the number of links remaining
        if (remaining_links[target_node.id] == 0) {
          S.push(target_node);
        } //if no more links, then add to starters array
      }
    }
  }

  //the remaining ones (loops)
  for (let i in M) {
    L.push(M[i]);
  }

  if (L.length != this._nodes.length && getOptions().debug) {
    console.warn("something went wrong, nodes missing");
  }

  var l = L.length;

  //save order number in the node
  for (let i = 0; i < l; ++i) {
    L[i].order = i;
  }

  //sort now by priority
  L = L.sort(function(A, B) {
    var Ap = A.constructor.priority || A.priority || 0;
    var Bp = B.constructor.priority || B.priority || 0;
    if (Ap == Bp) {
      //if same priority, sort by order
      return A.order - B.order;
    }
    return Ap - Bp; //sort by priority
  });

  //save order number in the node, again...
  for (let i = 0; i < l; ++i) {
    L[i].order = i;
  }

  return L;
};

/**
 * Returns all the nodes that could affect this one (ancestors) by crawling all the inputs recursively.
 * It doesn't include the node itself
 * @method getAncestors
 * @return {Array} an array with all the RGraphNodes that affect this node, in order of execution
 */
RGraph.prototype.getAncestors = function(node) {
  var ancestors = [];
  var pending = [node];
  var visited = {};

  while (pending.length) {
    var current = pending.shift();
    if (!current.inputs) {
      continue;
    }
    if (!visited[current.id] && current != node) {
      visited[current.id] = true;
      ancestors.push(current);
    }

    for (let i = 0; i < current.inputs.length; ++i) {
      var input = current.getInputNode(i);
      if (input && ancestors.indexOf(input) == -1) {
        pending.push(input);
      }
    }
  }

  ancestors.sort(function(a, b) {
    return a.order - b.order;
  });
  return ancestors;
};

/**
 * Positions every node in a more readable manner
 * @method arrange
 */
RGraph.prototype.arrange = function(margin) {
  margin = margin || 100;

  var nodes = this.computeExecutionOrder(false, true);
  var columns = [];
  for (let i = 0; i < nodes.length; ++i) {
    let node = nodes[i];
    let col = node._level || 1;
    if (!columns[col]) {
      columns[col] = [];
    }
    columns[col].push(node);
  }

  var x = margin;

  for (let i = 0; i < columns.length; ++i) {
    let column = columns[i];
    if (!column) {
      continue;
    }
    var max_size = 100;
    var y = margin + NODE_TITLE_HEIGHT;
    for (let j = 0; j < column.length; ++j) {
      let node = column[j];
      node.pos[0] = x;
      node.pos[1] = y;
      if (node.size[0] > max_size) {
        max_size = node.size[0];
      }
      y += node.size[1] + margin + NODE_TITLE_HEIGHT;
    }
    x += max_size + margin;
  }

  this.setDirtyCanvas(true, true);
};

/**
 * Returns the amount of time the graph has been running in milliseconds
 * @method getTime
 * @return {number} number of milliseconds the graph has been running
 */
RGraph.prototype.getTime = function() {
  return this.globaltime;
};

/**
 * Returns the amount of time accumulated using the fixedtime_lapse var. This is used in context where the time increments should be constant
 * @method getFixedTime
 * @return {number} number of milliseconds the graph has been running
 */

RGraph.prototype.getFixedTime = function() {
  return this.fixedtime;
};

/**
 * Returns the amount of time it took to compute the latest iteration. Take into account that this number could be not correct
 * if the nodes are using graphical actions
 * @method getElapsedTime
 * @return {number} number of milliseconds it took the last cycle
 */

RGraph.prototype.getElapsedTime = function() {
  return this.elapsed_time;
};

/**
 * Sends an event to all the nodes, useful to trigger stuff
 * @method sendEventToAllNodes
 * @param {String} eventname the name of the event (function to be called)
 * @param {Array} params parameters in array format
 */
RGraph.prototype.sendEventToAllNodes = function(eventname, params, mode) {
  mode = mode || ALWAYS;

  var nodes = this._nodes_in_order ? this._nodes_in_order : this._nodes;
  if (!nodes) {
    return;
  }

  for (let j = 0, l = nodes.length; j < l; ++j) {
    var node = nodes[j];

    if (!node[eventname] || node.mode != mode) {
      continue;
    }
    if (params === undefined) {
      node[eventname]();
    } else if (params && params.constructor === Array) {
      node[eventname].apply(node, params);
    } else {
      node[eventname](params);
    }
  }
};

RGraph.prototype.sendActionToCanvas = function(action, params) {
  if (!this.list_of_graphcanvas) {
    return;
  }

  for (let i = 0; i < this.list_of_graphcanvas.length; ++i) {
    var c = this.list_of_graphcanvas[i];
    if (c[action]) {
      c[action].apply(c, params);
    }
  }
};

/**
 * Adds a new node instance to this graph
 * @method add
 * @param {RGraphNode} node the instance of the node
 */

RGraph.prototype.add = function(node, skip_compute_order) {
  if (!node) {
    return;
  }

  //groups
  if (node.constructor === RGraphGroup) {
    this._groups.push(node);
    this.setDirtyCanvas(true);
    this.change();
    node.graph = this;
    this._version++;
    return;
  }

  //nodes
  if (node.id != -1 && this._nodes_by_id[node.id] != null) {
    console.warn(
      "RuneGraph: there is already a node with this ID, changing it"
    );
    node.id = ++this.last_node_id;
  }

  if (this._nodes.length >= MAX_NUMBER_OF_NODES) {
    throw "RuneGraph: max number of nodes in a graph reached";
  }

  //give him an id
  if (node.id == null || node.id == -1) {
    node.id = ++this.last_node_id;
  } else if (this.last_node_id < node.id) {
    this.last_node_id = node.id;
  }

  node.graph = this;
  this._version++;

  this._nodes.push(node);
  this._nodes_by_id[node.id] = node;

  // special case: if shape is a point, then the size
  // is given by the POINT_SIZE variable
  let shape = node._shape || node.constructor.shape || NODE_DEFAULT_SHAPE;
  if (shape === BULGE_SHAPE) {
    const point_size =
      node.point_size || node.constructor.point_size || POINT_SIZE;
    node.size = [point_size, point_size];
  }

  // if node is collapsed on init, pre-compute collapsed width
  if (node.flags.collapsed) {
    var title = node.getTitle ? node.getTitle() : node.title;
    if (title != null) {
      node._collapsed_width =
        node.collapsed_width || node.constructor.collapsed_width;
    }
  }

  if (node.onAdded) {
    node.onAdded(this);
  }

  if (this.config.align_to_grid) {
    node.alignToGrid();
  }

  if (!skip_compute_order) {
    this.updateExecutionOrder();
  }

  if (this.onNodeAdded) {
    this.onNodeAdded(node);
  }

  this.setDirtyCanvas(true);
  this.change();

  return node; //to chain actions
};

/**
 * Removes a node from the graph
 * @method remove
 * @param {RGraphNode} node the instance of the node
 */

RGraph.prototype.remove = function(node) {
  if (node.constructor === RGraphGroup) {
    var index = this._groups.indexOf(node);
    if (index != -1) {
      this._groups.splice(index, 1);
    }
    node.graph = null;
    this._version++;
    this.setDirtyCanvas(true, true);
    this.change();
    return;
  }

  if (this._nodes_by_id[node.id] == null) {
    return;
  } //not found

  if (node.ignore_remove) {
    return;
  } //cannot be removed

  // if auto connection is enabled, check for previous
  // and next nodes for reconnection
  // (for now: reconnect first input to first type-matching output;
  // if no types match, ignore reconnection)
  if (getOptions().allow_auto_connect && node.inputs && node.outputs) {
    // get all node's link types
    const input_types = node.inputs.map(i => (i.link === null ? null : i.type));
    const output_types = node.outputs.map(o =>
      o.links === null || o.links.length === 0 ? null : o.type
    );
    const [i, j] = getFirstMatchingIndices(input_types, output_types, [null]);
    if (i !== -1 && j !== -1) {
      const inp = this.links[node.inputs[i].link];
      const out = this.links[node.outputs[j].links[0]];
      this.getNodeById(inp.origin_id).connect(
        inp.origin_slot,
        this.getNodeById(out.target_id),
        out.target_slot
      );
    }
  }

  //disconnect inputs
  if (node.inputs) {
    for (let i = 0; i < node.inputs.length; i++) {
      let slot = node.inputs[i];
      if (slot.link != null) {
        node.disconnectInput(i);
      }
    }
  }

  //disconnect outputs
  if (node.outputs) {
    for (let i = 0; i < node.outputs.length; i++) {
      let slot = node.outputs[i];
      if (slot.links != null && slot.links.length) {
        node.disconnectOutput(i);
      }
    }
  }

  //node.id = -1; //why?

  //callback
  if (node.onRemoved) {
    node.onRemoved();
  }

  node.graph = null;
  this._version++;

  //remove from canvas render
  if (this.list_of_graphcanvas) {
    for (let i = 0; i < this.list_of_graphcanvas.length; ++i) {
      var canvas = this.list_of_graphcanvas[i];
      if (canvas.selected_nodes[node.id]) {
        delete canvas.selected_nodes[node.id];
      }
      if (canvas.node_dragged == node) {
        canvas.node_dragged = null;
      }
    }
  }

  //remove from containers
  var pos = this._nodes.indexOf(node);
  if (pos != -1) {
    this._nodes.splice(pos, 1);
  }
  delete this._nodes_by_id[node.id];

  if (this.onNodeRemoved) {
    this.onNodeRemoved(node);
  }

  //close panels
  this.sendActionToCanvas("checkPanels");

  this.setDirtyCanvas(true, true);
  this.change();

  this.updateExecutionOrder();
};

/**
 * Returns a node by its id.
 * @method getNodeById
 * @param {Number} id
 */

RGraph.prototype.getNodeById = function(id) {
  if (id == null) {
    return null;
  }
  return this._nodes_by_id[id];
};

/**
 * Returns a list of nodes that matches a class
 * @method findNodesByClass
 * @param {Class} classObject the class itself (not an string)
 * @return {Array} a list with all the nodes of this type
 */
RGraph.prototype.findNodesByClass = function(classObject, result) {
  result = result || [];
  result.length = 0;
  for (let i = 0, l = this._nodes.length; i < l; ++i) {
    if (this._nodes[i].constructor === classObject) {
      result.push(this._nodes[i]);
    }
  }
  return result;
};

/**
 * Returns a list of nodes that matches a type
 * @method findNodesByType
 * @param {String} type the name of the node type
 * @return {Array} a list with all the nodes of this type
 */
RGraph.prototype.findNodesByType = function(type, result) {
  type.toLowerCase();
  result = result || [];
  result.length = 0;
  for (let i = 0, l = this._nodes.length; i < l; ++i) {
    if (this._nodes[i].type.toLowerCase() == type) {
      result.push(this._nodes[i]);
    }
  }
  return result;
};

/**
 * Returns the first node that matches a name in its title
 * @method findNodeByTitle
 * @param {String} name the name of the node to search
 * @return {Node} the node or null
 */
RGraph.prototype.findNodeByTitle = function(title) {
  for (let i = 0, l = this._nodes.length; i < l; ++i) {
    if (this._nodes[i].title == title) {
      return this._nodes[i];
    }
  }
  return null;
};

/**
 * Returns a list of nodes that matches a name
 * @method findNodesByTitle
 * @param {String} name the name of the node to search
 * @return {Array} a list with all the nodes with this name
 */
RGraph.prototype.findNodesByTitle = function(title) {
  var result = [];
  for (let i = 0, l = this._nodes.length; i < l; ++i) {
    if (this._nodes[i].title == title) {
      result.push(this._nodes[i]);
    }
  }
  return result;
};

/**
 * Returns the top-most node in this position of the canvas
 * @method getNodeOnPos
 * @param {number} x the x coordinate in canvas space
 * @param {number} y the y coordinate in canvas space
 * @param {string} anchor anchor point of the node
 * @param {Array} nodes_list [optional] a list with all the nodes to search from, by default is all the nodes in the graph
 * @param {number} margin [optional] area check specific margin
 * @return {RGraphNode} the node at this position or null
 */
RGraph.prototype.getNodeOnPos = function(x, y, anchor, nodes_list, margin) {
  nodes_list = nodes_list || this._nodes;
  for (let i = nodes_list.length - 1; i >= 0; i--) {
    var n = nodes_list[i];
    if (n.isPointInside(x, y, anchor, margin)) {
      return n;
    }
  }
  return null;
};

/**
 * Returns the top-most group in that position
 * @method getGroupOnPos
 * @param {number} x the x coordinate in canvas space
 * @param {number} y the y coordinate in canvas space
 * @return {RGraphGroup} the group or null
 */
RGraph.prototype.getGroupOnPos = function(x, y) {
  for (let i = this._groups.length - 1; i >= 0; i--) {
    var g = this._groups[i];
    if (g.isPointInside(x, y, 2, true)) {
      return g;
    }
  }
  return null;
};

/**
 * Checks that the node type matches the node type registered, used when replacing a nodetype by a newer version during execution
 * this replaces the ones using the old version with the new version
 * @method checkNodeTypes
 */
RGraph.prototype.checkNodeTypes = function() {
  for (let i = 0; i < this._nodes.length; i++) {
    var node = this._nodes[i];
    var ctor = getRegisteredNodeTypes()[node.type];
    if (node.constructor == ctor) {
      continue;
    }
    console.log("node being replaced by newer version: " + node.type);
    var newnode = createNode(node.type);
    this._nodes[i] = newnode;
    newnode.configure(node.serialize());
    newnode.graph = this;
    this._nodes_by_id[newnode.id] = newnode;
    if (node.inputs) {
      newnode.inputs = node.inputs.concat();
    }
    if (node.outputs) {
      newnode.outputs = node.outputs.concat();
    }
  }
  this.updateExecutionOrder();
};

// ********** GLOBALS *****************

RGraph.prototype.trigger = function(action, param) {
  if (this.onTrigger) {
    this.onTrigger(action, param);
  }
};

/**
 * Tell this graph it has a global graph input of this type
 * @method addGlobalInput
 * @param {String} name
 * @param {String} type
 * @param {*} value [optional]
 */
RGraph.prototype.addInput = function(name, type, value) {
  var input = this.inputs[name];
  if (input) {
    //already exist
    return;
  }

  this.beforeChange();
  this.inputs[name] = { name: name, type: type, value: value };
  this._version++;
  this.afterChange();

  if (this.onInputAdded) {
    this.onInputAdded(name, type);
  }

  if (this.onInputsOutputsChange) {
    this.onInputsOutputsChange();
  }
};

/**
 * Assign a data to the global graph input
 * @method setGlobalInputData
 * @param {String} name
 * @param {*} data
 */
RGraph.prototype.setInputData = function(name, data) {
  var input = this.inputs[name];
  if (!input) {
    return;
  }
  input.value = data;
};

/**
 * Returns the current value of a global graph input
 * @method getInputData
 * @param {String} name
 * @return {*} the data
 */
RGraph.prototype.getInputData = function(name) {
  var input = this.inputs[name];
  if (!input) {
    return null;
  }
  return input.value;
};

/**
 * Changes the name of a global graph input
 * @method renameInput
 * @param {String} old_name
 * @param {String} new_name
 */
RGraph.prototype.renameInput = function(old_name, name) {
  if (name == old_name) {
    return;
  }

  if (!this.inputs[old_name]) {
    return false;
  }

  if (this.inputs[name]) {
    console.error("there is already one input with that name");
    return false;
  }

  this.inputs[name] = this.inputs[old_name];
  delete this.inputs[old_name];
  this._version++;

  if (this.onInputRenamed) {
    this.onInputRenamed(old_name, name);
  }

  if (this.onInputsOutputsChange) {
    this.onInputsOutputsChange();
  }
};

/**
 * Changes the type of a global graph input
 * @method changeInputType
 * @param {String} name
 * @param {String} type
 */
RGraph.prototype.changeInputType = function(name, type) {
  if (!this.inputs[name]) {
    return false;
  }

  if (
    this.inputs[name].type &&
    String(this.inputs[name].type).toLowerCase() == String(type).toLowerCase()
  ) {
    return;
  }

  this.inputs[name].type = type;
  this._version++;
  if (this.onInputTypeChanged) {
    this.onInputTypeChanged(name, type);
  }
};

/**
 * Removes a global graph input
 * @method removeInput
 * @param {String} name
 * @param {String} type
 */
RGraph.prototype.removeInput = function(name) {
  if (!this.inputs[name]) {
    return false;
  }

  delete this.inputs[name];
  this._version++;

  if (this.onInputRemoved) {
    this.onInputRemoved(name);
  }

  if (this.onInputsOutputsChange) {
    this.onInputsOutputsChange();
  }
  return true;
};

/**
 * Creates a global graph output
 * @method addOutput
 * @param {String} name
 * @param {String} type
 * @param {*} value
 */
RGraph.prototype.addOutput = function(name, type, value) {
  this.outputs[name] = { name: name, type: type, value: value };
  this._version++;

  if (this.onOutputAdded) {
    this.onOutputAdded(name, type);
  }

  if (this.onInputsOutputsChange) {
    this.onInputsOutputsChange();
  }
};

/**
 * Assign a data to the global output
 * @method setOutputData
 * @param {String} name
 * @param {String} value
 */
RGraph.prototype.setOutputData = function(name, value) {
  var output = this.outputs[name];
  if (!output) {
    return;
  }
  output.value = value;
};

/**
 * Returns the current value of a global graph output
 * @method getOutputData
 * @param {String} name
 * @return {*} the data
 */
RGraph.prototype.getOutputData = function(name) {
  var output = this.outputs[name];
  if (!output) {
    return null;
  }
  return output.value;
};

/**
 * Renames a global graph output
 * @method renameOutput
 * @param {String} old_name
 * @param {String} new_name
 */
RGraph.prototype.renameOutput = function(old_name, name) {
  if (!this.outputs[old_name]) {
    return false;
  }

  if (this.outputs[name]) {
    console.error("there is already one output with that name");
    return false;
  }

  this.outputs[name] = this.outputs[old_name];
  delete this.outputs[old_name];
  this._version++;

  if (this.onOutputRenamed) {
    this.onOutputRenamed(old_name, name);
  }

  if (this.onInputsOutputsChange) {
    this.onInputsOutputsChange();
  }
};

/**
 * Changes the type of a global graph output
 * @method changeOutputType
 * @param {String} name
 * @param {String} type
 */
RGraph.prototype.changeOutputType = function(name, type) {
  if (!this.outputs[name]) {
    return false;
  }

  if (
    this.outputs[name].type &&
    String(this.outputs[name].type).toLowerCase() == String(type).toLowerCase()
  ) {
    return;
  }

  this.outputs[name].type = type;
  this._version++;
  if (this.onOutputTypeChanged) {
    this.onOutputTypeChanged(name, type);
  }
};

/**
 * Removes a global graph output
 * @method removeOutput
 * @param {String} name
 */
RGraph.prototype.removeOutput = function(name) {
  if (!this.outputs[name]) {
    return false;
  }
  delete this.outputs[name];
  this._version++;

  if (this.onOutputRemoved) {
    this.onOutputRemoved(name);
  }

  if (this.onInputsOutputsChange) {
    this.onInputsOutputsChange();
  }
  return true;
};

RGraph.prototype.triggerInput = function(name, value) {
  var nodes = this.findNodesByTitle(name);
  for (let i = 0; i < nodes.length; ++i) {
    nodes[i].onTrigger(value);
  }
};

RGraph.prototype.setCallback = function(name, func) {
  var nodes = this.findNodesByTitle(name);
  for (let i = 0; i < nodes.length; ++i) {
    nodes[i].setTrigger(func);
  }
};

//used for undo, called before any change is made to the graph
RGraph.prototype.beforeChange = function(info) {
  if (this.onBeforeChange) {
    this.onBeforeChange(this, info);
  }
  this.sendActionToCanvas("onBeforeChange", this);
};

//used to resend actions, called after any change is made to the graph
RGraph.prototype.afterChange = function(info) {
  if (this.onAfterChange) {
    this.onAfterChange(this, info);
  }
  this.sendActionToCanvas("onAfterChange", this);
};

RGraph.prototype.connectionChange = function(node) {
  this.updateExecutionOrder();
  if (this.onConnectionChange) {
    this.onConnectionChange(node);
  }
  this._version++;
  this.sendActionToCanvas("onConnectionChange");
};

/**
 * returns if the graph is in live mode
 * @method isLive
 */

RGraph.prototype.isLive = function() {
  if (!this.list_of_graphcanvas) {
    return false;
  }

  for (let i = 0; i < this.list_of_graphcanvas.length; ++i) {
    var c = this.list_of_graphcanvas[i];
    if (c.live_mode) {
      return true;
    }
  }
  return false;
};

/**
 * clears the triggered slot animation in all links (stop visual animation)
 * @method clearTriggeredSlots
 */
RGraph.prototype.clearTriggeredSlots = function() {
  for (let i in this.links) {
    let link_info = this.links[i];
    if (!link_info) {
      continue;
    }
    if (link_info._last_time) {
      link_info._last_time = 0;
    }
  }
};

/* Called when something changed visually (not the graph!) */
RGraph.prototype.change = function() {
  if (getOptions().debug) {
    console.log("Graph changed");
  }
  this.sendActionToCanvas("setDirty", [true, true]);
  if (this.onChange) {
    this.onChange(this);
  }
};

RGraph.prototype.setDirtyCanvas = function(fg, bg) {
  this.sendActionToCanvas("setDirty", [fg, bg]);
};

/**
 * Inserts a node on a link
 * @method insertNode
 * @param {String} node_type type of node to insert
 * @param {RGraphLink} link
 * @param {event} event on-click event
 */
RGraph.prototype.insertNode = function(canvas, node_type, link, event, data = {}) {
  const node = createNode(node_type);
  if (!node) return;
  // init node
  initNode(data, node, this._nodes.length, data.initializer);

  node.pos = canvas.convertEventToCanvasOffset(event);
  canvas.graph.add(node);

  if (link && getOptions().allow_auto_connect) {
    let node_left = this.getNodeById(link.origin_id);
    let node_right = this.getNodeById(link.target_id);
    this.tryReconnect(node, node_left, node_right);
  }
};

/**
 * Destroys a link
 * @method removeLink
 * @param {Number} link_id
 */
RGraph.prototype.tryReconnect = function(node, node_left, node_right) {
  if (
    !node.inputs ||
    !node.inputs.length ||
    !node.outputs ||
    !node.outputs.length
  )
    return;
  // get all neighbor node's link types
  const left_types = node_left.outputs.map(o =>
    o.links === null || o.links.length === 0 ? null : o.type
  );
  const right_types = node_right.inputs.map(i =>
    i.link === null ? null : i.type
  );
  const input_types = node.inputs.map(i => i.type);
  const output_types = node.outputs.map(o => o.type);
  const [il, jl] = getFirstMatchingIndices(
    left_types,
    input_types,
    [null],
    [0]
  );
  const [ir, jr] = getFirstMatchingIndices(
    output_types,
    right_types,
    [null],
    [0]
  );
  if (il !== -1 && jl !== -1 && ir !== -1 && jr !== -1) {
    const links = this.links;
    const left = links[node_left.outputs[il].links[0]];
    const right = links[node_right.inputs[jr].link];
    node_left.connect(left.origin_slot, node, jl);
    node.connect(ir, node_right, right.target_slot);
  }
};

/**
 * Destroys a link
 * @method removeLink
 * @param {Number} link_id
 */
RGraph.prototype.removeLink = function(link_id) {
  var link = this.links[link_id];
  if (!link) {
    return;
  }
  var node = this.getNodeById(link.target_id);
  if (node) {
    node.disconnectInput(link.target_slot);
  }
};

//save and recover app state ***************************************
/**
 * Creates a Object containing all the info about this graph, it can be serialized
 * @method serialize
 * @return {Object} value of the node
 */
RGraph.prototype.serialize = function() {
  var nodes_info = [];
  for (let i = 0, l = this._nodes.length; i < l; ++i) {
    nodes_info.push(this._nodes[i].serialize());
  }

  //pack link info into a non-verbose format
  var links = [];
  for (let i in this.links) {
    //links is an OBJECT
    var link = this.links[i];
    if (!link.serialize) {
      //weird bug I havent solved yet
      console.warn(
        "weird RGraphLink bug, link info is not a RGraphLink but a regular object"
      );
      var link2 = new RGraphLink();
      for (let j in link) {
        link2[j] = link[j];
      }
      this.links[i] = link2;
      link = link2;
    }

    links.push(link.serialize());
  }

  var groups_info = [];
  for (let i = 0; i < this._groups.length; ++i) {
    groups_info.push(this._groups[i].serialize());
  }

  var data = {
    last_node_id: this.last_node_id,
    last_link_id: this.last_link_id,
    nodes: nodes_info,
    links: links,
    groups: groups_info,
    config: this.config,
    extra: this.extra
  };

  if (this.onSerialize) this.onSerialize(data);

  return data;
};

/**
 * Configure a graph from a JSON string
 * @method configure
 * @param {String} str configure a graph from a JSON string
 * @param {Boolean} returns if there was any error parsing
 */
RGraph.prototype.configure = function(data, keep_old) {
  if (!data) {
    return;
  }

  if (!keep_old) {
    this.clear();
  }

  var nodes = data.nodes;

  //decode links info (they are very verbose)
  if (data.links && data.links.constructor === Array) {
    var links = [];
    for (let i = 0; i < data.links.length; ++i) {
      var link_data = data.links[i];
      if (!link_data) {
        //weird bug
        console.warn("serialized graph link data contains errors, skipping.");
        continue;
      }
      var link = new RGraphLink();
      link.configure(link_data);
      links[link.id] = link;
    }
    data.links = links;
  }

  //copy all stored fields
  for (let i in data) {
    if (i == "nodes" || i == "groups")
      //links must be accepted
      continue;
    this[i] = data[i];
  }

  var error = false;

  //create nodes
  this._nodes = [];
  if (nodes) {
    for (let i = 0, l = nodes.length; i < l; ++i) {
      let n_info = nodes[i]; //stored info
      let node = createNode(n_info.type, n_info.title);
      if (!node) {
        if (getOptions().debug) {
          console.log("Node not found or has errors: " + n_info.type);
        }

        //in case of error we create a replacement node to avoid losing info
        node = new RGraphNode();
        node.last_serialization = n_info;
        node.has_errors = true;
        error = true;
        //continue;
      }

      node.id = n_info.id; //id it or it will create a new id
      this.add(node, true); //add before configure, otherwise configure cannot create links
    }

    //configure nodes afterwards so they can reach each other
    for (let i = 0, l = nodes.length; i < l; ++i) {
      let n_info = nodes[i];
      let node = this.getNodeById(n_info.id);
      if (node) {
        node.configure(n_info);
      }
    }
  }

  //groups
  this._groups.length = 0;
  if (data.groups) {
    for (let i = 0; i < data.groups.length; ++i) {
      var group = new RGraphGroup();
      group.configure(data.groups[i]);
      this.add(group);
    }
  }

  this.updateExecutionOrder();

  this.extra = data.extra || {};

  if (this.onConfigure) this.onConfigure(data);

  this._version++;
  this.setDirtyCanvas(true, true);
  return error;
};

RGraph.prototype.load = function(url, callback) {
  var that = this;

  //from file
  if (url.constructor === File || url.constructor === Blob) {
    var reader = new FileReader();
    reader.addEventListener("load", function(event) {
      var data = JSON.parse(event.target.result);
      that.configure(data);
      if (callback) callback();
    });

    reader.readAsText(url);
    return;
  }

  //is a string, then an URL
  var req = new XMLHttpRequest();
  req.open("GET", url, true);
  req.send(null);
  req.onload = function() {
    if (req.status !== 200) {
      console.error("Error loading graph:", req.status, req.response);
      return;
    }
    var data = JSON.parse(req.response);
    that.configure(data);
    if (callback) callback();
  };
  req.onerror = function(err) {
    console.error("Error loading graph:", err);
  };
};

RGraph.prototype.onNodeTrace = function(/*node, msg, color*/) {
  //TODO
};

if (typeof window != "undefined" && !window["requestAnimationFrame"]) {
  window.requestAnimationFrame =
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    function(callback) {
      window.setTimeout(callback, 1000 / 60);
    };
}

module.exports = RGraph;
