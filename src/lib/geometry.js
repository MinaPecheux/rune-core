const {
  BOX_SHAPE,
  ROUND_SHAPE,
  CIRCLE_SHAPE,
  CARD_SHAPE,
  BULGE_SHAPE
} = require("./helpers/shapes.js");

//Scale and Offset
function DragAndScale(element, skip_events) {
  this.offset = new Float32Array([0, 0]);
  this.scale = 1;
  this.max_scale = 10;
  this.min_scale = 0.1;
  this.onredraw = null;
  this.enabled = true;
  this.last_mouse = [0, 0];
  this.element = null;
  this.visible_area = new Float32Array(4);

  if (element) {
    this.element = element;
    if (!skip_events) {
      this.bindEvents(element);
    }
  }
}

DragAndScale.prototype.bindEvents = function(element) {
  this.last_mouse = new Float32Array(2);

  this._binded_mouse_callback = this.onMouse.bind(this);

  element.addEventListener("mousedown", this._binded_mouse_callback);
  element.addEventListener("mousemove", this._binded_mouse_callback);

  element.addEventListener("mousewheel", this._binded_mouse_callback, false);
  element.addEventListener("wheel", this._binded_mouse_callback, false);
};

DragAndScale.prototype.computeVisibleArea = function() {
  if (!this.element) {
    this.visible_area[0] = this.visible_area[1] = this.visible_area[2] = this.visible_area[3] = 0;
    return;
  }
  var width = this.element.width;
  var height = this.element.height;
  var startx = -this.offset[0];
  var starty = -this.offset[1];
  var endx = startx + width / this.scale;
  var endy = starty + height / this.scale;
  this.visible_area[0] = startx;
  this.visible_area[1] = starty;
  this.visible_area[2] = endx - startx;
  this.visible_area[3] = endy - starty;
};

DragAndScale.prototype.onMouse = function(e) {
  if (!this.enabled) {
    return;
  }

  var canvas = this.element;
  var rect = canvas.getBoundingClientRect();
  var x = e.clientX - rect.left;
  var y = e.clientY - rect.top;
  e.canvasx = x;
  e.canvasy = y;
  e.dragging = this.dragging;

  var ignore = false;
  if (this.onmouse) {
    ignore = this.onmouse(e);
  }

  if (e.type == "mousedown") {
    this.dragging = true;
    canvas.removeEventListener("mousemove", this._binded_mouse_callback);
    document.body.addEventListener("mousemove", this._binded_mouse_callback);
    document.body.addEventListener("mouseup", this._binded_mouse_callback);
  } else if (e.type == "mousemove") {
    if (!ignore) {
      var deltax = x - this.last_mouse[0];
      var deltay = y - this.last_mouse[1];
      if (this.dragging) {
        this.mouseDrag(deltax, deltay);
      }
    }
  } else if (e.type == "mouseup") {
    this.dragging = false;
    document.body.removeEventListener("mousemove", this._binded_mouse_callback);
    document.body.removeEventListener("mouseup", this._binded_mouse_callback);
    canvas.addEventListener("mousemove", this._binded_mouse_callback);
  } else if (
    e.type == "mousewheel" ||
    e.type == "wheel" ||
    e.type == "DOMMouseScroll"
  ) {
    e.eventType = "mousewheel";
    if (e.type == "wheel") {
      e.wheel = -e.deltaY;
    } else {
      e.wheel = e.wheelDeltaY != null ? e.wheelDeltaY : e.detail * -60;
    }

    //from stack overflow
    e.delta = e.wheelDelta ? e.wheelDelta / 40 : e.deltaY ? -e.deltaY / 3 : 0;
    this.changeDeltaScale(1.0 + e.delta * 0.05);
  }

  this.last_mouse[0] = x;
  this.last_mouse[1] = y;

  e.preventDefault();
  e.stopPropagation();
  return false;
};

DragAndScale.prototype.toCanvasContext = function(ctx) {
  ctx.scale(this.scale, this.scale);
  ctx.translate(this.offset[0], this.offset[1]);
};

DragAndScale.prototype.convertOffsetToCanvas = function(pos) {
  //return [pos[0] / this.scale - this.offset[0], pos[1] / this.scale - this.offset[1]];
  return [
    (pos[0] + this.offset[0]) * this.scale,
    (pos[1] + this.offset[1]) * this.scale
  ];
};

DragAndScale.prototype.convertCanvasToOffset = function(pos, out) {
  out = out || [0, 0];
  out[0] = pos[0] / this.scale - this.offset[0];
  out[1] = pos[1] / this.scale - this.offset[1];
  return out;
};

DragAndScale.prototype.mouseDrag = function(x, y) {
  this.offset[0] += x / this.scale;
  this.offset[1] += y / this.scale;

  if (this.onredraw) {
    this.onredraw(this);
  }
};

DragAndScale.prototype.changeScale = function(value, zooming_center) {
  if (value < this.min_scale) {
    value = this.min_scale;
  } else if (value > this.max_scale) {
    value = this.max_scale;
  }

  if (value == this.scale) {
    return;
  }

  if (!this.element) {
    return;
  }

  var rect = this.element.getBoundingClientRect();
  if (!rect) {
    return;
  }

  zooming_center = zooming_center || [rect.width * 0.5, rect.height * 0.5];
  var center = this.convertCanvasToOffset(zooming_center);
  this.scale = value;
  if (Math.abs(this.scale - 1) < 0.01) {
    this.scale = 1;
  }

  var new_center = this.convertCanvasToOffset(zooming_center);
  var delta_offset = [new_center[0] - center[0], new_center[1] - center[1]];

  this.offset[0] += delta_offset[0];
  this.offset[1] += delta_offset[1];

  if (this.onredraw) {
    this.onredraw(this);
  }
};

DragAndScale.prototype.changeDeltaScale = function(value, zooming_center) {
  this.changeScale(this.scale * value, zooming_center);
};

DragAndScale.prototype.reset = function() {
  this.scale = 1;
  this.offset[0] = 0;
  this.offset[1] = 0;
};

//used by some widgets to render a curve editor
function CurveEditor(points) {
  this.points = points;
  this.selected = -1;
  this.nearest = -1;
  this.size = null; //stores last size used
  this.must_update = true;
  this.margin = 5;
}

CurveEditor.sampleCurve = function(f, points) {
  if (!points) return;
  for (let i = 0; i < points.length - 1; ++i) {
    var p = points[i];
    var pn = points[i + 1];
    if (pn[0] < f) continue;
    var r = pn[0] - p[0];
    if (Math.abs(r) < 0.00001) return p[1];
    var local_f = (f - p[0]) / r;
    return p[1] * (1.0 - local_f) + pn[1] * local_f;
  }
  return 0;
};

CurveEditor.prototype.draw = function(
  ctx,
  size,
  graphcanvas,
  background_color,
  line_color,
  inactive
) {
  var points = this.points;
  if (!points) return;
  this.size = size;
  var w = size[0] - this.margin * 2;
  var h = size[1] - this.margin * 2;

  line_color = line_color || "#666";

  ctx.save();
  ctx.translate(this.margin, this.margin);

  if (background_color) {
    ctx.fillStyle = "#111";
    ctx.fillRect(0, 0, w, h);
    ctx.fillStyle = "#222";
    ctx.fillRect(w * 0.5, 0, 1, h);
    ctx.strokeStyle = "#333";
    ctx.strokeRect(0, 0, w, h);
  }
  ctx.strokeStyle = line_color;
  if (inactive) ctx.globalAlpha = 0.5;
  ctx.beginPath();
  for (let i = 0; i < points.length; ++i) {
    let p = points[i];
    ctx.lineTo(p[0] * w, (1.0 - p[1]) * h);
  }
  ctx.stroke();
  ctx.globalAlpha = 1;
  if (!inactive)
    for (let i = 0; i < points.length; ++i) {
      let p = points[i];
      ctx.fillStyle =
        this.selected == i ? "#FFF" : this.nearest == i ? "#DDD" : "#AAA";
      ctx.beginPath();
      ctx.arc(p[0] * w, (1.0 - p[1]) * h, 2, 0, Math.PI * 2);
      ctx.fill();
    }
  ctx.restore();
};

//localpos is mouse in curve editor space
CurveEditor.prototype.onMouseDown = function(localpos, graphcanvas) {
  let points = this.points;
  if (!points) return;
  if (localpos[1] < 0) return;

  //this.captureInput(true);
  let w = this.size[0] - this.margin * 2;
  let h = this.size[1] - this.margin * 2;
  let x = localpos[0] - this.margin;
  let y = localpos[1] - this.margin;
  let pos = [x, y];
  let max_dist = 30 / graphcanvas.ds.scale;
  //search closer one
  this.selected = this.getCloserPoint(pos, max_dist);
  //create one
  if (this.selected == -1) {
    let point = [x / w, 1 - y / h];
    points.push(point);
    points.sort(function(a, b) {
      return a[0] - b[0];
    });
    this.selected = points.indexOf(point);
    this.must_update = true;
  }
  if (this.selected != -1) return true;
};

CurveEditor.prototype.onMouseMove = function(localpos, graphcanvas) {
  let points = this.points;
  if (!points) return;
  let s = this.selected;
  if (s < 0) return;
  let x = (localpos[0] - this.margin) / (this.size[0] - this.margin * 2);
  let y = (localpos[1] - this.margin) / (this.size[1] - this.margin * 2);
  let curvepos = [localpos[0] - this.margin, localpos[1] - this.margin];
  let max_dist = 30 / graphcanvas.ds.scale;
  this._nearest = this.getCloserPoint(curvepos, max_dist);
  let point = points[s];
  if (point) {
    let is_edge_point = s == 0 || s == points.length - 1;
    if (
      !is_edge_point &&
      (localpos[0] < -10 ||
        localpos[0] > this.size[0] + 10 ||
        localpos[1] < -10 ||
        localpos[1] > this.size[1] + 10)
    ) {
      points.splice(s, 1);
      this.selected = -1;
      return;
    }
    if (!is_edge_point)
      //not edges
      point[0] = Math.clamp(x, 0, 1);
    else point[0] = s == 0 ? 0 : 1;
    point[1] = 1.0 - Math.clamp(y, 0, 1);
    points.sort(function(a, b) {
      return a[0] - b[0];
    });
    this.selected = points.indexOf(point);
    this.must_update = true;
  }
};

CurveEditor.prototype.onMouseUp = function(/*localpos, graphcanvas*/) {
  this.selected = -1;
  return false;
};

CurveEditor.prototype.getCloserPoint = function(pos, max_dist) {
  let points = this.points;
  if (!points) return -1;
  max_dist = max_dist || 30;
  let w = this.size[0] - this.margin * 2;
  let h = this.size[1] - this.margin * 2;
  let num = points.length;
  let p2 = [0, 0];
  let min_dist = 1000000;
  let closest = -1;
  for (let i = 0; i < num; ++i) {
    let p = points[i];
    p2[0] = p[0] * w;
    p2[1] = (1.0 - p[1]) * h;
    let dist = distance(pos, p2);
    if (dist > min_dist || dist > max_dist) continue;
    closest = i;
    min_dist = dist;
  }
  return closest;
};

//utils

Math.clamp = function(v, a, b) {
  return a > v ? a : b < v ? b : v;
};

function distance(a, b) {
  return Math.sqrt(
    (b[0] - a[0]) * (b[0] - a[0]) + (b[1] - a[1]) * (b[1] - a[1])
  );
}

function getAnchoredPosition(
  anchor,
  containerWidth,
  containerHeight,
  textWidth,
  textHeight,
  padding
) {
  switch (anchor) {
  case "nw":
    return [padding, -containerHeight + textHeight + padding];
  case "n":
    return [
      (containerWidth - textWidth) * 0.5,
      -containerHeight + textHeight + padding
    ];
  case "ne":
    return [
      containerWidth - textWidth - padding,
      -containerHeight + textHeight + padding
    ];
  case "w":
    return [padding, -(containerHeight - textHeight) * 0.5];
  case "middle":
    return [
      (containerWidth - textWidth) * 0.5,
      -(containerHeight - textHeight) * 0.5
    ];
  case "e":
    return [
      containerWidth - textWidth - padding,
      -(containerHeight - textHeight) * 0.5
    ];
  case "sw":
    return [padding, -padding];
  case "s":
    return [(containerWidth - textWidth) * 0.5, -padding];
  case "se":
    return [containerWidth - textWidth - padding, -padding];
  }
}

function isInsideRectangle(x, y, left, top, width, height) {
  if (left < x && left + width > x && top < y && top + height > y) {
    return true;
  }
  return false;
}

//[minx,miny,maxx,maxy]
function growBounding(bounding, x, y) {
  if (x < bounding[0]) {
    bounding[0] = x;
  } else if (x > bounding[2]) {
    bounding[2] = x;
  }

  if (y < bounding[1]) {
    bounding[1] = y;
  } else if (y > bounding[3]) {
    bounding[3] = y;
  }
}

//point inside bounding box
function isInsideBounding(p, bb) {
  if (
    p[0] < bb[0][0] ||
    p[1] < bb[0][1] ||
    p[0] > bb[1][0] ||
    p[1] > bb[1][1]
  ) {
    return false;
  }
  return true;
}

//bounding overlap, format: [ startx, starty, width, height ]
function overlapBounding(a, b) {
  var A_end_x = a[0] + a[2];
  var A_end_y = a[1] + a[3];
  var B_end_x = b[0] + b[2];
  var B_end_y = b[1] + b[3];

  if (a[0] > B_end_x || a[1] > B_end_y || A_end_x < b[0] || A_end_y < b[1]) {
    return false;
  }
  return true;
}

function roundToNearest(value, step) {
  const inv = 1.0 / step;
  return Math.round(value * inv) / inv;
}

/*
	Copyright 2010 by Robin W. Spencer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You can find a copy of the GNU General Public License
    at http://www.gnu.org/licenses/.

*/
function getControlPoints(x0,y0,x1,y1,x2,y2,t){
  //  x0,y0,x1,y1 are the coordinates of the end (knot) pts of this segment
  //  x2,y2 is the next knot -- not connected here but needed to calculate p2
  //  p1 is the control point calculated here, from x1 back toward x0.
  //  p2 is the next control point, calculated here and returned to become the 
  //  next segment's p1.
  //  t is the 'tension' which controls how far the control points spread.
  
  //  Scaling factors: distances from this knot to the previous and following knots.
  var d01=Math.sqrt(Math.pow(x1-x0,2)+Math.pow(y1-y0,2));
  var d12=Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
  
  var fa=t*d01/(d01+d12);
  var fb=t-fa;

  var p1x=x1+fa*(x0-x2);
  var p1y=y1+fa*(y0-y2);

  var p2x=x1-fb*(x0-x2);
  var p2y=y1-fb*(y0-y2);  
  
  return [p1x,p1y,p2x,p2y];
}

// Special shapes *************************************************
if (typeof window != "undefined" && window.CanvasRenderingContext2D) {
  window.CanvasRenderingContext2D.prototype.roundRect = function(
    x,
    y,
    width,
    height,
    radius,
    radius_low
  ) {
    if (radius === undefined) {
      radius = 5;
    }

    if (radius_low === undefined) {
      radius_low = radius;
    }

    this.moveTo(x + radius, y);
    this.lineTo(x + width - radius, y);
    this.quadraticCurveTo(x + width, y, x + width, y + radius);

    this.lineTo(x + width, y + height - radius_low);
    this.quadraticCurveTo(
      x + width,
      y + height,
      x + width - radius_low,
      y + height
    );
    this.lineTo(x + radius_low, y + height);
    this.quadraticCurveTo(x, y + height, x, y + height - radius_low);
    this.lineTo(x, y + radius);
    this.quadraticCurveTo(x, y, x + radius, y);
  };

  window.CanvasRenderingContext2D.prototype.drawClosedSpline = function (pts, t) {
    this.save();
    let cp = [];   // array of control points, as x0,y0,x1,y1,...
    const n = pts.length;
  
    // Append and prepend knots and control points to close the curve
    pts.push(pts[0], pts[1], pts[2], pts[3]);
    pts.unshift(pts[n-1]);
    pts.unshift(pts[n-1]);
    for(let i=0;i<n;i+=2){
      cp=cp.concat(getControlPoints(pts[i],pts[i+1],pts[i+2],pts[i+3],pts[i+4],pts[i+5],t));
    }
    cp=cp.concat(cp[0],cp[1]);   
    for(let i=2;i<n+2;i+=2){
      // ctx.beginPath();
      this.moveTo(pts[i],pts[i+1]);
      this.bezierCurveTo(cp[2*i-2],cp[2*i-1],cp[2*i],cp[2*i+1],pts[i+2],pts[i+3]);
      // ctx.closePath();
    }
    this.restore();
  };

  window.CanvasRenderingContext2D.prototype.drawGeometry = function ({
    node,
    shape,
    area,
    size,
    stroke,
    low_quality
  }) {
    this.beginPath();
    if (shape == BOX_SHAPE || low_quality) {
      this.rect(area[0], area[1], area[2], area[3]);
    } else if (shape == ROUND_SHAPE || shape == CARD_SHAPE) {
      this.roundRect(
        area[0],
        area[1],
        area[2],
        area[3],
        this.node_round_radius,
        shape == CARD_SHAPE ? 0 : this.node_round_radius
      );
    } else if (shape == CIRCLE_SHAPE) {
      this.arc(
        area[0] + size[0] * 0.5,
        area[1] + size[1] * 0.5,
        size[0] * 0.5,
        0,
        Math.PI * 2
      );
    } else if (shape === BULGE_SHAPE) {
      let bulge = this.node_bulge;
      // eslint-disable-next-line no-prototype-builtins
      if (node.hasOwnProperty("node_bulge")) {
        bulge = node.node_bulge;
      }
      // eslint-disable-next-line no-prototype-builtins
      else if (node.constructor.hasOwnProperty("node_bulge")) {
        bulge = node.constructor.node_bulge;
      }
      const offset = bulge * 0.125 * size[0];
      this.drawClosedSpline([
        area[0] + offset,
        area[1] + offset,
        area[0] - offset + size[0],
        area[1] + offset,
        area[0] - offset + size[0],
        area[1] - offset + size[1],
        area[0] + offset,
        area[1] - offset + size[1],
      ], bulge * 0.5);

      // for bulge shape, we need to draw the stroke
      // before the inner fill to avoid a double stroke
      if (stroke) {
        this.strokeStyle = stroke.color;
        this.lineWidth = stroke.width;
        this.stroke();
      }
      this.rect(
        area[0] + offset,
        area[1] + offset,
        size[0] - offset * 2,
        size[1] - offset * 2
      );
    }
  };
}

function getTextWidth(text, font) {
  const tmp = document.createElement("div");
  tmp.innerHTML = text;
  tmp.style.position = "absolute";
  tmp.style.float = "left";
  tmp.style.whiteSpace = "nowrap";
  tmp.style.visibility = "hidden";
  tmp.style.font = font;
  document.querySelector("body").appendChild(tmp);
  const width = tmp.clientWidth + 1;
  tmp.parentNode.removeChild(tmp);
  return width;
}

module.exports = {
  DragAndScale,
  CurveEditor,
  distance,
  getAnchoredPosition,
  isInsideBounding,
  isInsideRectangle,
  growBounding,
  overlapBounding,
  roundToNearest,
  getTextWidth
};
