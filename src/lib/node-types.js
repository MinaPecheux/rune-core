const { getOptions, setSearchboxExtras } = require("./config.js");

//nodetypes by string
var REGISTERED_NODE_TYPES = {};
var NODE_TYPES_BY_FILE_EXTENSION = {}; //used for dropping files in the canvas
var NODES = {}; //node types by classname

function getRegisteredNodeTypes() {
  return REGISTERED_NODE_TYPES;
}

function getNodeTypesByFileExtension() {
  return NODE_TYPES_BY_FILE_EXTENSION;
}

/**
 * Returns a registered node type with a given name
 * @method getNodeType
 * @param {String} type full name of the node class. p.e. "math/sin"
 * @return {Class} the node class
 */
function getNodeType(type) {
  return REGISTERED_NODE_TYPES[type];
}

/**
 * Register a node class so it can be listed when the user wants to create a new one
 * @method setNode
 * @param {String} type name of the node and path
 * @param {RGraphNode} node_class prepared node class
 */

function setNode(type, node_class) {
  NODES[type] = node_class;
}

/**
 * Returns a list of node types matching one category
 * @method getNodeType
 * @param {String} category category name
 * @return {Array} array with all the node classes
 */

function getNodeTypesInCategory(category, filter) {
  var r = [];
  for (let i in REGISTERED_NODE_TYPES) {
    var type = REGISTERED_NODE_TYPES[i];
    if (type.filter != filter) {
      continue;
    }

    if (category == "") {
      if (type.category == null) {
        r.push(type);
      }
    } else if (type.category == category) {
      r.push(type);
    }
  }

  return getOptions().auto_sort_node_types ? r.sort() : r;
}

/**
 * Returns a list with all the node type categories
 * @method getNodeTypesCategories
 * @param {String} filter only nodes with ctor.filter equal can be shown
 * @return {Array} array with all the names of the categories
 */
function getNodeTypesCategories(filter) {
  var categories = { "": 1 };
  for (let i in REGISTERED_NODE_TYPES) {
    var type = REGISTERED_NODE_TYPES[i];
    if (type.category && !type.skip_list) {
      if (type.filter != filter) continue;
      categories[type.category] = 1;
    }
  }
  var result = [];
  for (let i in categories) {
    result.push(i);
  }
  return getOptions().auto_sort_node_types ? result.sort() : result;
}

/**
 * Register a node class so it can be listed when the user wants to create a new one
 * @method registerNodeType
 * @param {String} type name of the node and path
 * @param {RGraphNode} node_class prepared node class
 */

function registerNodeType(type, node_class) {
  REGISTERED_NODE_TYPES[type] = node_class;
  if (getOptions().debug) {
    console.log(`Registered node: ${type}`);
  }
}

/**
 * removes a node type from the system
 * @method unregisterNodeType
 * @param {String|Object} type name of the node or the node constructor itself
 */
function unregisterNodeType(type) {
  var base_class =
    type.constructor === String ? REGISTERED_NODE_TYPES[type] : type;
  if (!base_class) throw "node type not found: " + type;
  delete REGISTERED_NODE_TYPES[base_class.type];
  if (base_class.constructor.name) delete NODES[base_class.constructor.name];
}

/**
 * Removes all previously registered node's types
 */
function clearRegisteredTypes() {
  REGISTERED_NODE_TYPES = {};
  NODE_TYPES_BY_FILE_EXTENSION = {};
  NODES = {};
  setSearchboxExtras({});
}

module.exports = {
  getRegisteredNodeTypes,
  getNodeTypesByFileExtension,
  getNodeType,
  setNode,
  getNodeTypesInCategory,
  getNodeTypesCategories,
  registerNodeType,
  unregisterNodeType,
  clearRegisteredTypes
};
