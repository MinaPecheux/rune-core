const { createNodeType } = require("./node.js");
const { clearRegisteredTypes } = require("./node-types.js");

const TAGS = {};
var current_tags = [];

function getCurrentTags() {
  return current_tags;
}

function hasRegisteredTag(tag) {
  return tag in TAGS && typeof TAGS[tag] !== "undefined";
}

function hasDefinedTag(tag) {
  return current_tags.includes(tag);
}

function registerTag(name, initializer) {
  if (Object.prototype.hasOwnProperty.call(TAGS, name) || !initializer) return;
  TAGS[name] = initializer;
}

function defineTag(tag, context = null, clear_others = false) {
  if (hasDefinedTag(tag)) {
    return;
  }
  if (clear_others) {
    clearRegisteredTypes();
    current_tags = [];
  }
  const nodes = TAGS[tag](context);
  for (let node_name of Object.keys(nodes)) {
    createNodeType(node_name, nodes[node_name]);
  }
  current_tags.push(tag);
}

module.exports = {
  getCurrentTags,
  hasRegisteredTag,
  hasDefinedTag,
  registerTag,
  defineTag
};
