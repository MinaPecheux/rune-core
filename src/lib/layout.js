const dagre = require("dagre");
const { roundToNearest } = require("./geometry");

function centerLayout(layout, width, height) {
  const xs = Object.values(layout).map(pos => pos[0]);
  const minX = Math.min(...xs);
  const maxX = Math.max(...xs);
  const ys = Object.values(layout).map(pos => pos[1]);
  const minY = Math.min(...ys);
  const maxY = Math.max(...ys);
  const cx = (maxX - minX) * 0.5 + minX;
  const cy = (maxY - minY) * 0.5 + minY;
  const offX = width * 0.5 - cx;
  const offY = height * 0.5 - cy;
  for (let v of Object.values(layout)) {
    v[0] += offX;
    v[1] += offY;
  }
  return layout;
}

function applyLayout(nodes, layout, canvas = null) {
  const grid_size = (canvas && canvas.snap_grid.enabled) ? canvas.snap_grid.size : null;
  for (let node of nodes) {
    let [x, y] = layout[node.id];
    if (grid_size !== null) {
      x = roundToNearest(x, grid_size);
      y = roundToNearest(y, grid_size);
    }
    node.pos = [x, y];
  }
}

function getNodesSequence(entrypoint, nodes, edges, return_modified_nodes = false) {
  const remaining_nodes = [...nodes];
  const nodes_sequence = [entrypoint];
  remaining_nodes.splice(remaining_nodes.findIndex((n) => n.id === entrypoint.id), 1);
  let last_node = entrypoint;
  let w = 0; // escape var
  while (last_node.outputs && last_node.outputs.length > 0 && w < 100) {
    const linked_node = remaining_nodes.find((node) => {
      return edges.some((e) => e.source === last_node.id && e.target === node.id);
    });
    if (!linked_node) break;
    nodes_sequence.push(linked_node);
    remaining_nodes.splice(remaining_nodes.findIndex((n) => n.id === linked_node.id), 1);
    last_node = linked_node;
    w++;
  }
  if (return_modified_nodes) {
    return [nodes_sequence, remaining_nodes];
  } else {
    return nodes_sequence;
  }
}

function getGridLayout(graphData, width, height, options = {}) {
  const opt = Object.assign({
    paddingx: 0,
    paddingy: 0,
    ranksep: 128,
    nodesep: 128,
    ncols: null,
    nrows: null
  }, options);

  // find all entrypoints (i.e. nodes with no input and at least one
  // connected output)
  const entrypoints = graphData.nodes.filter((n) => {
    const connected_inputs = n.inputs ? n.inputs.filter((i) => i.link !== null) : [];
    const connected_outputs = n.outputs ? n.outputs.filter((o) => o.links && o.links.length > 0) : [];
    return connected_inputs.length === 0 && connected_outputs.length > 0;
  });

  // foreach entrypoint:
  // - sort node ids to start with entry node
  // - chain all remaining node ids in this group of nodes sequentially
  // according to graph edges
  let remaining_nodes = [...graphData.nodes];
  const sorted_nodes = [];
  for (const entrypoint of entrypoints) {
    const seq = getNodesSequence(entrypoint, remaining_nodes, graphData.edges, true);
    remaining_nodes = seq[1];
    sorted_nodes.push(seq[0]);
  }
  // create final group with all not linked nodes
  sorted_nodes.push(remaining_nodes);

  const max_x = (width - 2 * opt.paddingx) * 1.0 / (opt.ranksep + 1);
  const n = sorted_nodes.reduce((acc, group) => acc + group.length, 0);
  const optimal_nodesep = (height - 2 * opt.paddingy) / (n / max_x);
  opt.nodesep = Math.min(opt.nodesep, optimal_nodesep);
  let max_cols = opt.ncols;
  if (opt.ncols === null && opt.nrows !== null) {
    max_cols = n / opt.nrows;
  }
  const layout = {};
  let y = 0; // current y position (in grid cell, from top)
  for (const group of sorted_nodes) {
    let x = 0; // current x position (in grid cell, from left)
    let c = 0; // current col
    let i = 0; // current index in group
    for (let node of group) {
      layout[node.id] = [x * opt.ranksep, y * opt.nodesep];
      x++;
      c++;
      i++;
      if ((max_cols !== null && c >= max_cols) || x > max_x || i === group.length) {
        x = 0;
        c = 0;
        y++;
      }
    }
  }
  return layout;
}

function getDagreLayout(graphData, options = {}) {
  // create graph
  var g = new dagre.graphlib.Graph();
  g.setGraph(Object.assign({
    rankdir: "LR",
  }, options));
  // Default to assigning a new object as a label for each new edge.
  g.setDefaultEdgeLabel(function() {
    return {};
  });

  // set nodes+edges in graph
  const layout = {};
  for (let node of graphData.nodes) {
    layout[node.id] = node;
    g.setNode(node.id, { width: node.size[0], height: node.size[1] });
  }
  for (let { source, target } of graphData.edges) {
    g.setEdge(source, target);
  }

  // compute layout
  dagre.layout(g);

  // return nodes placement
  g.nodes().forEach(function(v) {
    const n = g.node(v);
    layout[v] = [n.x, n.y];
  });
  return layout;
}

module.exports = {
  centerLayout,
  applyLayout,
  getNodesSequence,
  getGridLayout,
  getDagreLayout
};
