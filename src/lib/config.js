// ---------------------------------------------------------
// global graph options
var OPTIONS = {
  debug: false,
  node_images_path: "",
  catch_exceptions: true,
  throw_errors: true,
  allow_auto_connect: true, //if set to true, removing/adding nodes will automatically links to keep a continuous flow (if possible with the I/O types)
  allow_scripts: false, //if set to true some nodes like Formula would be allowed to evaluate code that comes from unsafe sources (like node configuration), which could lead to exploits
  auto_sort_node_types: false, // If set to true, will automatically sort node types / categories in the context menus
  proxy: null, //used to redirect calls,
  use_json_for_apis: true, //suppose that APIs communicate via JSON payloads
  auto_update_node_statuses: true, //automatically set nodes to "pending" or "null" status when executing the graph

  current_node_set: null
};

function getOptions() {
  return OPTIONS;
}

function setOptions(options) {
  OPTIONS = Object.assign(OPTIONS, options);
}

function setOption(name, value) {
  OPTIONS[name] = value;
}

function enableDebug() {
  OPTIONS.debug = true;
}

function disableDebug() {
  OPTIONS.debug = false;
}

function getDefaultAPICallHeaders() {
  return {
    headers: OPTIONS.use_json_for_apis ? {"Content-Type": "application/json"} : {},
    method: "GET"
  };
}

// ---------------------------------------------------------
// global graph callbacks
var CALLBACKS = {
  onNodeTypeRegistered: () => {},
  onNodeTypeReplaced: () => {}
};

function getCallbacks() {
  return CALLBACKS;
}

function setCallback(name, handler) {
  CALLBACKS[name] = handler;
}

// ---------------------------------------------------------
// extra options to show in the searchbox
var SEARCHBOX_EXTRAS = {}; //used to add extra features to the search box

function getSearchboxExtras() {
  return SEARCHBOX_EXTRAS;
}

function setSearchboxExtras(extras) {
  SEARCHBOX_EXTRAS = extras;
}

/**
 * Register a string in the search box so when the user types it it will recommend this node
 * @method registerSearchboxExtra
 * @param {String} node_type the node recommended
 * @param {String} description text to show next to it
 * @param {Object} data it could contain info of how the node should be configured
 * @return {Boolean} true if they can be connected
 */
function registerSearchboxExtra(node_type, description, data) {
  this.searchbox_extras[description.toLowerCase()] = {
    type: node_type,
    desc: description,
    data: data
  };
}

module.exports = {
  getOptions,
  setOption,
  setOptions,
  enableDebug,
  disableDebug,
  getDefaultAPICallHeaders,
  getCallbacks,
  setCallback,
  getSearchboxExtras,
  setSearchboxExtras,
  registerSearchboxExtra
};
