//*********************************************************************************
// RGraphCanvas: RGraph renderer CLASS
//*********************************************************************************

const { getOptions, getSearchboxExtras } = require("./config.js");
const RGraphGroup = require("./group.js");
const ContextMenu = require("./context-menu.js");
const {
  DragAndScale,
  distance,
  overlapBounding,
  isInsideRectangle,
  getAnchoredPosition,
  getTextWidth
} = require("./geometry.js");
const { createNode } = require("./node.js");
const {
  getRegisteredNodeTypes,
  getNodeTypesByFileExtension,
  getNodeTypesInCategory,
  getNodeTypesCategories
} = require("./node-types.js");
const {
  BOX_SHAPE,
  ROUND_SHAPE,
  CIRCLE_SHAPE,
  CARD_SHAPE,
  ARROW_SHAPE,
  BULGE_SHAPE,
  VALID_SHAPES
} = require("./helpers/shapes.js");
const {
  CENTER,
  LEFT,
  UP,
  RIGHT,
  DOWN,
  ACTION,
  EVENT,
  ALWAYS,
  NEVER,
  ON_EVENT,
  ON_TRIGGER,
  NORMAL_TITLE,
  TRANSPARENT_TITLE,
  AUTOHIDE_TITLE,
  LINEAR_LINK,
  STRAIGHT_LINK,
  SPLINE_LINK,
  ROUNDED_LINK
} = require("./helpers/enums.js");
const {
  CANVAS_GRID_SIZE,
  POINT_SIZE,
  RENDER_TEXT,
  RENDER_TITLE_BOX,
  NODE_DEFAULT_SHAPE,
  NODE_COLLAPSED_WIDTH,
  NODE_SLOT_HEIGHT,
  NODE_TEXT_SIZE,
  NODE_SUBTEXT_SIZE,
  NODE_TITLE_HEIGHT,
  NODE_TEXT_COLOR,
  DEFAULT_SHADOW_COLOR,
  NODE_WIDGET_HEIGHT,
  DEFAULT_GROUP_FONT_SIZE,
  COLORS
} = require("./helpers/defaults.js");
const {
  isValidConnection,
  closeAllContextMenus,
  getAnchorOffset,
  getTime
} = require("./helpers/utils.js");

function getCanvasDefaultOptions(color_mode) {
  return {
    anchor_point: "middle",
    font_family: "Arial",
    title_text_size: NODE_TEXT_SIZE,
    inner_text_size: NODE_SUBTEXT_SIZE,
    node_title_height: NODE_TITLE_HEIGHT,
    background_color: COLORS[color_mode].BACKGROUND_COLOR,
    background_grid: {
      enabled: false,
      type: "line",
      line_width: 1,
      dot_size: 2,
      cross_size: 4,
      major_ticks_size: 32, // in px
      major_ticks_color: "rgba(0, 0, 0, 0.05)",
      infinite: true
    },
    snap_grid: {
      enabled: false,
      size: CANVAS_GRID_SIZE,
    },
    toolbar: {
      enabled: true,
      placement: "bottom right",
      bgcolor: COLORS[color_mode].NODE_DEFAULT_COLOR,
      text_color: COLORS[color_mode].NODE_TEXT_COLOR,
      vertical: false,
      drag: true,
      run: true,
      zoom_in: true,
      zoom_out: true,
      fit_to_screen: true,
      reset: true,
      actions: []
    },
    node_default_color: COLORS[color_mode].NODE_DEFAULT_COLOR,
    node_default_bgcolor: COLORS[color_mode].NODE_DEFAULT_BGCOLOR,
    node_default_boxcolor: COLORS[color_mode].NODE_DEFAULT_BOXCOLOR,
    node_text_color: COLORS[color_mode].NODE_TEXT_COLOR,
    node_title_color: COLORS[color_mode].NODE_TITLE_COLOR,
    node_selected_title_color: COLORS[color_mode].NODE_SELECTED_TITLE_COLOR,
    default_link_color: COLORS[color_mode].LINK_COLOR,
    highlighted_link_color: null,
    default_highlighted_link_color: COLORS[color_mode].HIGHLIGHTED_LINK_COLOR,
    node_box_outline_color: COLORS[color_mode].NODE_BOX_OUTLINE_COLOR,
    event_link_color: COLORS[color_mode].EVENT_LINK_COLOR,
    connecting_link_color: COLORS[color_mode].CONNECTING_LINK_COLOR,
    widget_outline_color: COLORS[color_mode].WIDGET_OUTLINE_COLOR,
    widget_bgcolor: COLORS[color_mode].WIDGET_BGCOLOR,
    widget_text_color: COLORS[color_mode].WIDGET_TEXT_COLOR,
    widget_secondary_text_color: COLORS[color_mode].WIDGET_SECONDARY_TEXT_COLOR,

    node_border: null,
    title_mode: NORMAL_TITLE,

    highquality_render: true,
    use_gradients: false, //set to true to render titlebar with gradients
    editor_alpha: 1, //used for transition
    pause_rendering: false,
    clear_background: true,

    read_only: false, //if set to true users cannot modify the graph
    show_contextual_menu: true,
    render_only_selected: true,
    live_mode: false,
    show_info: false,
    allow_dragcanvas: true,
    allow_dragnodes: true,
    allow_zoom_on_wheel: false,
    allow_interaction: true, //allow to control widgets, buttons, collapse, etc
    allow_searchbox: true,
    allow_reconnect_links: false, //allows to change a connection with having to redo it again
    allow_link_interaction: true,
    allow_snake_links: true, //allows to draw links that go back to the left is path is too weirdly stretch
    render_links_arrows: true, //adds arrows to show flow direction on snake links
    snake_link_bend_start_offset_x: 8,
    snake_link_bend_mid_offset_x: 48,

    drag_mode: false,
    dragging_rectangle: null,

    filter: null, //allows to filter to only accept some type of nodes in a graph

    set_canvas_dirty_on_mouse_event: false, //forces to redraw the canvas if the mouse does anything
    always_render_background: false,
    render_shadows: true,
    render_shadows_if_collapsed: false,
    render_canvas_border: true,
    render_connections_shadows: false, //too much cpu
    render_connections_border: false,
    render_curved_connections: false,
    render_connection_arrows: false,
    render_slots: true,
    render_slot_labels: true,
    render_collapsed_slots: true,
    render_selection_marker: true,
    render_execution_order: false,
    render_title_colored: true,
    render_link_tooltip: true,

    links_render_mode: SPLINE_LINK,
    links_corner_radius_size: 16,
    link_middle_point_radius: 5,
    link_middle_point_label: "",
    link_middle_point_label_font_size: 14,
    link_middle_point_style: {
      fill: COLORS[color_mode].LINK_COLOR,
      stroke_color: "transparent",
      line_width: 0
    },
    link_tooltip_style: {
      font: "Arial",
      font_size: 14,
      with_point: true,
      with_arrow: true,
      text_color: COLORS[color_mode].LINK_COLOR,
      fill: COLORS[color_mode].BACKGROUND_COLOR,
      stroke_color: "transparent",
      line_width: 1,
      shape: ROUND_SHAPE,
    },
    link_tooltip_text: "", // global tooltip to display on all link middle point hover

    connections_width: 2,
    node_round_radius: 8,
    node_bulge: 0.5,
  };
}

const TOOLBAR_BUTTONS = {
  run: {
    icon: "M 18.0476,9.9003743 9.9827705,4.5221395 A 2.5226242,2.5226242 0 0 0 6.0600899,6.6260081 V 17.379955 a 2.5226242,2.5226242 0 0 0 3.9226806,2.098823 L 18.0476,14.100544 a 2.5226242,2.5226242 0 0 0 0,-4.1976471 z",
    callback: (_this) => () => {
      _this.graph.runStep().then(() => {
        _this.setDirty(true);
      });
    }
  },
  zoom_in: {
    icon: "M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0zM10 7v3m0 0v3m0-3h3m-3 0H7",
    callback: (_this) => () => {
      const { scale } = _this.ds;
      _this.ds.changeScale(scale * 1.1);
      _this.graph.change();
    }
  },
  zoom_out: {
    icon: "M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0zM13 10H7",
    callback: (_this) => () => {
      const { scale } = _this.ds;
      _this.ds.changeScale(scale * 0.9);
      _this.graph.change();
    }
  },
  fit_to_screen: {
    icon: "M4 8V4m0 0h4M4 4l5 5m11-1V4m0 0h-4m4 0l-5 5M4 16v4m0 0h4m-4 0l5-5m11 5l-5-5m5 5v-4m0 4h-4",
    callback: (_this) => () => {
      const { ds } = _this;
      const optimal_margin = 40; // in px
      const bbox = _this.getNodesBoundingBox();
      if (bbox === null) return;
      const [x, y, w, h] = bbox;
      ds.computeVisibleArea();
      const { element, visible_area } = ds;
      const rect = element.getBoundingClientRect();
      // compute offset delta
      const new_center = [
        x + w * 0.5,
        y + h * 0.5
      ];
      const center = [
        visible_area[0] + visible_area[2] * 0.5,
        visible_area[1] + visible_area[3] * 0.5
      ];
      const delta_offset = [new_center[0] - center[0], new_center[1] - center[1]];
      if (Math.abs(delta_offset[0]) > 1e-3) {
        ds.offset[0] -= delta_offset[0];
      }
      if (Math.abs(delta_offset[1]) > 1e-3) {
        ds.offset[1] -= delta_offset[1];
      }
      // compute new scale
      const closest_coord = Math.min(x, y, rect.width - (x + w), rect.height - (y + h));
      ds.changeScale(1.0 + Math.abs(closest_coord - optimal_margin) / Math.max(closest_coord, optimal_margin));
      _this.graph.change();
    }
  },
  reset: {
    icon: "M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15",
    callback: (_this) => () => {
      _this.ds.reset();
      _this.graph.change();
    }
  }
};

/**
 * This class is in charge of rendering one graph inside a canvas. And provides all the interaction required.
 * Valid callbacks are: onNodeSelected, onNodeDeselected, onShowNodePanel, onNodeDblClicked
 *
 * @class RGraphCanvas
 * @constructor
 * @param {HTMLCanvas} canvas the canvas where you want to render (it accepts a selector in string format or the canvas element itself)
 * @param {LGraph} graph [optional]
 * @param {Object} options [optional] { ... }
 */
function RGraphCanvas(canvas, graph, options) {
  options = options || {};

  this.color_mode = options.color_mode || "light";
  const defaultOptions = getCanvasDefaultOptions(this.color_mode);
  for (const key of Object.keys(defaultOptions)) {
    if (!Array.isArray(defaultOptions[key]) && defaultOptions[key] != null && typeof defaultOptions[key] == "object") {
      this[key] = Object.assign(defaultOptions[key], options[key] || {});
    } else {
      this[key] = key in options ? options[key] : defaultOptions[key];
    }
  }

  if (canvas && canvas.constructor == String) {
    canvas = document.querySelector(canvas);
  }

  const container = document.createElement("div");
  container.id = "rgraph-container";
  canvas.parentNode.replaceChild(container, canvas);
  const { width, height } = container.getBoundingClientRect();
  canvas.width = width;
  canvas.height = Math.max(canvas.height, height);
  container.appendChild(canvas);
  if (this.toolbar.enabled) {
    container.style.position = "relative";

    const toolbarElement = document.createElement("div");
    toolbarElement.classList.add("rgraphcanvas-toolbar");
    toolbarElement.classList.add(`rgraphcanvas-toolbar-${this.toolbar.vertical ? "vertical" : "horizontal"}`);
    const top = this.toolbar.placement.includes("top");
    const left = this.toolbar.placement.includes("left");
    toolbarElement.style[top ? "top" : "bottom"] = "4px";
    toolbarElement.style[left ? "left" : "right"] = "4px";
    toolbarElement.style.backgroundColor = this.toolbar.bgcolor;
    for (let base_action of ["run", "zoom_in", "zoom_out", "fit_to_screen", "reset"]) {
      if (this.toolbar[base_action]) {
        const btnData = TOOLBAR_BUTTONS[base_action];
        const btn = document.createElement("div");
        btn.classList.add("rgraphcanvas-toolbar-button");
        btn.style.color = this.toolbar.text_color;
        btn.innerHTML = "<svg xmlns=\"http://www.w3.org/2000/svg\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">" +
          `<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="${btnData.icon}" />` +
          "</svg>";
        btn.onclick = btnData.callback(this);
        toolbarElement.appendChild(btn);
      }
    }
    container.appendChild(toolbarElement);
  }
  this.container = container;

  this.canvas_images = {};

  this.ds = new DragAndScale();
  this.zoom_modify_alpha = true; //otherwise it generates ugly patterns when scaling down too much

  this.title_text_font = `${this.title_text_size}px ${this.font_family}`;
  this.inner_text_font = `normal ${this.inner_text_size}px ${this.font_family}`;
  this.default_connection_color = COLORS[this.color_mode].CONNECTION_COLORS;

  this.link_type_colors = {
    "-1": this.event_link_color,
    number: "#aaa",
    node: "#dca"
  };

  this.mouse = [0, 0]; //mouse in canvas coordinates, where 0,0 is the top-left corner of the blue rectangle
  this.graph_mouse = [0, 0]; //mouse in graph coordinates, where 0,0 is the top-left corner of the blue rectangle
  this.canvas_mouse = this.graph_mouse; //LEGACY: REMOVE THIS, USE GRAPH_MOUSE INSTEAD

  //to personalize the search box
  this.onSearchBox = null;
  this.onSearchBoxSelection = null;

  //callbacks
  this.onMouse = null;
  this.onDrawBackground = null; //to render background objects (behind nodes and connections) in the canvas affected by transform
  this.onDrawForeground = null; //to render foreground objects (above nodes and connections) in the canvas affected by transform
  this.onDrawOverlay = null; //to render foreground objects not affected by transform (for GUIs)
  this.onDrawLinkTooltip = null; //called when rendering a tooltip
  this.onNodeMoved = null; //called after moving a node
  this.onSelectionChange = null; //called if the selection changes
  this.onConnectingChange = null; //called before any link changes
  this.onBeforeChange = null; //called before modifying the graph
  this.onAfterChange = null; //called after modifying the graph

  this.current_node = null;
  this.node_widget = null; //used for widgets
  this.over_link_center = null;
  this.last_mouse_position = [0, 0];
  this.visible_area = this.ds.visible_area;
  this.visible_links = [];

  //link canvas and graph
  if (graph) {
    graph.attachCanvas(this);
  }

  this.setCanvas(canvas);
  this.clear();

  if (!options.skip_render) {
    this.startRendering();
  }

  this.autoresize = options.autoresize;
}

RGraphCanvas.gradients = {}; //cache of gradients

/**
 * clears all the data inside
 *
 * @method clear
 */
RGraphCanvas.prototype.clear = function() {
  this.frame = 0;
  this.last_draw_time = 0;
  this.render_time = 0;
  this.fps = 0;

  //this.scale = 1;
  //this.offset = [0,0];

  this.dragging_rectangle = null;

  this.selected_nodes = {};
  this.selected_group = null;

  this.visible_nodes = [];
  this.node_dragged = null;
  this.node_over = null;
  this.node_capturing_input = null;
  this.connecting_node = null;
  this.highlighted_links = {};

  this.dragging_canvas = false;

  this.dirty_canvas = true;
  this.dirty_bgcanvas = true;
  this.dirty_area = null;

  this.node_in_panel = null;
  this.node_widget = null;

  this.last_mouse = [0, 0];
  this.last_mouseclick = 0;
  this.visible_area.set([0, 0, 0, 0]);

  if (this.onClear) {
    this.onClear();
  }
};

/**
 * assigns a graph, you can reassign graphs to the same canvas
 *
 * @method setGraph
 * @param {LGraph} graph
 */
RGraphCanvas.prototype.setGraph = function(graph, skip_clear) {
  if (this.graph == graph) {
    return;
  }

  if (!skip_clear) {
    this.clear();
  }

  if (!graph && this.graph) {
    this.graph.detachCanvas(this);
    return;
  }

  graph.attachCanvas(this);

  this.setDirty(true, true);
};

/**
 * returns the visualy active graph (in case there are more in the stack)
 * @method getCurrentGraph
 * @return {LGraph} the active graph
 */
RGraphCanvas.prototype.getCurrentGraph = function() {
  return this.graph;
};

/**
 * returns the bounding box around all current nodes
 * @method getNodesBoundingBox
 * @return {Array} the nodes bounding box
 */
RGraphCanvas.prototype.getNodesBoundingBox = function() {
  if (!this.graph) return null;
  const xs = this.graph._nodes.map((node) => node.pos[0]);
  const ys = this.graph._nodes.map((node) => node.pos[1]);
  const x = Math.min(...xs);
  const y = Math.min(...ys);
  const w = Math.max(...xs) - x;
  const h = Math.max(...ys) - y;
  return [x, y, w, h];
};

/**
 * assigns a canvas
 *
 * @method setCanvas
 * @param {Canvas} assigns a canvas (also accepts the ID of the element (not a selector)
 */
RGraphCanvas.prototype.setCanvas = function(canvas, skip_events) {
  if (canvas) {
    if (canvas.constructor == String) {
      canvas = document.getElementById(canvas);
      if (!canvas) {
        throw "Error creating LiteGraph canvas: Canvas not found";
      }
    }
  }

  if (canvas == this.canvas) {
    return;
  }

  if (!canvas && this.canvas) {
    //maybe detach events from old_canvas
    if (!skip_events) {
      this.unbindEvents();
    }
  }

  this.canvas = canvas;
  this.ds.element = canvas;

  if (!canvas) {
    return;
  }

  //this.canvas.tabindex = "1000";
  canvas.className += " rgraphcanvas";
  canvas.data = this;
  canvas.tabindex = "1"; //to allow key events

  //bg canvas: used for non changing stuff
  this.bgcanvas = null;
  if (!this.bgcanvas) {
    this.bgcanvas = document.createElement("canvas");
    this.bgcanvas.width = this.canvas.width;
    this.bgcanvas.height = this.canvas.height;
  }

  if (canvas.getContext == null) {
    if (canvas.localName != "canvas") {
      throw "Element supplied for RGraphCanvas must be a <canvas> element, you passed a " +
        canvas.localName;
    }
    throw "This browser doesn't support Canvas";
  }

  var ctx = (this.ctx = canvas.getContext("2d"));
  if (ctx == null) {
    if (!canvas.webgl_enabled) {
      console.warn("This canvas seems to be WebGL, enabling WebGL renderer");
    }
    this.enableWebGL();
  }

  //input:  (move and up could be unbinded)
  this._mousemove_callback = this.processMouseMove.bind(this);
  this._mouseup_callback = this.processMouseUp.bind(this);

  if (!skip_events) {
    this.bindEvents();
  }
};

//used in some events to capture them
RGraphCanvas.prototype._doNothing = function doNothing(e) {
  e.preventDefault();
  return false;
};
RGraphCanvas.prototype._doReturnTrue = function doNothing(e) {
  e.preventDefault();
  return true;
};

/**
 * binds mouse, keyboard, touch and drag events to the canvas
 * @method bindEvents
 **/
RGraphCanvas.prototype.bindEvents = function() {
  if (this._events_binded) {
    console.warn("RGraphCanvas: events already binded");
    return;
  }

  var canvas = this.canvas;

  var ref_window = this.getCanvasWindow();
  var document = ref_window.document; //hack used when moving canvas between windows

  this._mousedown_callback = this.processMouseDown.bind(this);
  this._mousewheel_callback = this.processMouseWheel.bind(this);

  canvas.addEventListener("mousedown", this._mousedown_callback, true); //down do not need to store the binded
  canvas.addEventListener("mousemove", this._mousemove_callback);
  canvas.addEventListener("mousewheel", this._mousewheel_callback, false);

  canvas.addEventListener("contextmenu", this._doNothing);
  canvas.addEventListener("DOMMouseScroll", this._mousewheel_callback, false);

  //touch events
  //if( 'touchstart' in document.documentElement )
  {
    canvas.addEventListener("touchstart", this.touchHandler, true);
    canvas.addEventListener("touchmove", this.touchHandler, true);
    canvas.addEventListener("touchend", this.touchHandler, true);
    canvas.addEventListener("touchcancel", this.touchHandler, true);
  }

  //Keyboard ******************
  this._key_callback = this.processKey.bind(this);

  canvas.addEventListener("keydown", this._key_callback, true);
  document.addEventListener("keyup", this._key_callback, true); //in document, otherwise it doesn't fire keyup

  //Dropping Stuff over nodes ************************************
  this._ondrop_callback = this.processDrop.bind(this);

  canvas.addEventListener("dragover", this._doNothing, false);
  canvas.addEventListener("dragend", this._doNothing, false);
  canvas.addEventListener("drop", this._ondrop_callback, false);
  canvas.addEventListener("dragenter", this._doReturnTrue, false);

  this._events_binded = true;
};

/**
 * unbinds mouse events from the canvas
 * @method unbindEvents
 **/
RGraphCanvas.prototype.unbindEvents = function() {
  if (!this._events_binded) {
    console.warn("RGraphCanvas: no events binded");
    return;
  }

  var ref_window = this.getCanvasWindow();
  var document = ref_window.document;

  this.canvas.removeEventListener("mousedown", this._mousedown_callback);
  this.canvas.removeEventListener("mousewheel", this._mousewheel_callback);
  this.canvas.removeEventListener("DOMMouseScroll", this._mousewheel_callback);
  this.canvas.removeEventListener("keydown", this._key_callback);
  document.removeEventListener("keyup", this._key_callback);
  this.canvas.removeEventListener("contextmenu", this._doNothing);
  this.canvas.removeEventListener("drop", this._ondrop_callback);
  this.canvas.removeEventListener("dragenter", this._doReturnTrue);

  this.canvas.removeEventListener("touchstart", this.touchHandler);
  this.canvas.removeEventListener("touchmove", this.touchHandler);
  this.canvas.removeEventListener("touchend", this.touchHandler);
  this.canvas.removeEventListener("touchcancel", this.touchHandler);

  this._mousedown_callback = null;
  this._mousewheel_callback = null;
  this._key_callback = null;
  this._ondrop_callback = null;

  this._events_binded = false;
};

RGraphCanvas.getFileExtension = function(url) {
  var question = url.indexOf("?");
  if (question != -1) {
    url = url.substr(0, question);
  }
  var point = url.lastIndexOf(".");
  if (point == -1) {
    return "";
  }
  return url.substr(point + 1).toLowerCase();
};

/**
 * this function allows to render the canvas using WebGL instead of Canvas2D
 * this is useful if you plant to render 3D objects inside your nodes, it uses litegl.js for webgl and canvas2DtoWebGL to emulate the Canvas2D calls in webGL
 * @method enableWebGL
 **/
RGraphCanvas.prototype.enableWebGL = function() {
  if (typeof GL == undefined) {
    throw "litegl.js must be included to use a WebGL canvas";
  }
  if (typeof enableWebGLCanvas == undefined) {
    throw "webglCanvas.js must be included to use this feature";
  }

  /* eslint-disable-next-line no-undef */
  this.gl = this.ctx = enableWebGLCanvas(this.canvas);
  this.ctx.webgl = true;
  this.bgcanvas = this.canvas;
  this.bgctx = this.gl;
  this.canvas.webgl_enabled = true;

  /*
GL.create({ canvas: this.bgcanvas });
this.bgctx = enableWebGLCanvas( this.bgcanvas );
window.gl = this.gl;
*/
};

/**
 * Recomputes the grid to use the current zoom and drag
 *
 * @class RGraphCanvas
 * @method recomputeGrid
 * @param {CanvasContext} ctx - current canvas context
 */
RGraphCanvas.prototype.recomputeGrid = function(ctx) {
  const {
    type,
    line_width,
    dot_size,
    cross_size,
    major_ticks_size,
    major_ticks_color,
    infinite
  } = this.background_grid;
  let min_x, min_y, max_x, max_y;
  this.ds.computeVisibleArea();
  const { visible_area } = this.ds;
  if (infinite) {
    min_x = Math.floor(visible_area[0] / major_ticks_size) * major_ticks_size;
    min_y = Math.floor(visible_area[1] / major_ticks_size) * major_ticks_size;
    max_x = Math.floor((visible_area[0] + visible_area[2]) / major_ticks_size) * major_ticks_size;
    max_y = Math.floor((visible_area[1] + visible_area[3]) / major_ticks_size) * major_ticks_size;
  } else {
    min_x = 0;
    min_y = 0;
    max_x = this.canvas.width;
    max_y = this.canvas.height;
  }
  ctx.save();
  ctx.fillStyle = major_ticks_color;
  ctx.strokeStyle = major_ticks_color;
  ctx.lineWidth = line_width;
  let x, y;
  switch (type) {
  case "line":
    x = min_x + major_ticks_size;
    while (x < max_x) {
      ctx.beginPath();
      ctx.moveTo(x, 0);
      ctx.lineTo(x, max_y);
      x += major_ticks_size;
      ctx.stroke();
    }
    y = min_y + major_ticks_size;
    while (y < max_y) {
      ctx.beginPath();
      ctx.moveTo(0, y);
      ctx.lineTo(max_x, y);
      y += major_ticks_size;
      ctx.stroke();
    }
    break;
  case "dot":
    x = min_x + major_ticks_size;
    while (x < max_x) {
      y = min_y + major_ticks_size;
      while (y < max_y) {
        ctx.beginPath();
        ctx.arc(x, y, dot_size, 0, Math.PI * 2);
        ctx.fill();
        y += major_ticks_size;
      }
      x += major_ticks_size;
    }
    break;
  case "cross":
    x = min_x + major_ticks_size;
    while (x < max_x) {
      ctx.beginPath();
      y = min_y + major_ticks_size;
      while (y < max_y) {
        ctx.moveTo(x - cross_size, y);
        ctx.lineTo(x + cross_size, y);
        ctx.moveTo(x, y - cross_size);
        ctx.lineTo(x, y + cross_size);
        y += major_ticks_size;
      }
      x += major_ticks_size;
      ctx.stroke();
    }
    break;
  default:
    break;
  }
  ctx.restore();
};

/**
 * marks as dirty the canvas, this way it will be rendered again
 *
 * @class RGraphCanvas
 * @method setDirty
 * @param {bool} fgcanvas if the foreground canvas is dirty (the one containing the nodes)
 * @param {bool} bgcanvas if the background canvas is dirty (the one containing the wires)
 */
RGraphCanvas.prototype.setDirty = function(fgcanvas, bgcanvas) {
  if (fgcanvas) {
    this.dirty_canvas = true;
  }
  if (bgcanvas) {
    this.dirty_bgcanvas = true;
  }
};

/**
 * Used to attach the canvas in a popup
 *
 * @method getCanvasWindow
 * @return {window} returns the window where the canvas is attached (the DOM root node)
 */
RGraphCanvas.prototype.getCanvasWindow = function() {
  if (!this.canvas) {
    return window;
  }
  var doc = this.canvas.ownerDocument;
  return doc.defaultView || doc.parentWindow;
};

/**
 * starts rendering the content of the canvas when needed
 *
 * @method startRendering
 */
RGraphCanvas.prototype.startRendering = function() {
  if (this.is_rendering) {
    return;
  } //already rendering

  this.is_rendering = true;
  renderFrame.call(this);

  function renderFrame() {
    if (!this.pause_rendering) {
      this.draw();
    }

    var window = this.getCanvasWindow();
    if (this.is_rendering) {
      window.requestAnimationFrame(renderFrame.bind(this));
    }
  }
};

/**
 * stops rendering the content of the canvas (to save resources)
 *
 * @method stopRendering
 */
RGraphCanvas.prototype.stopRendering = function() {
  this.is_rendering = false;
  /*
if(this.rendering_timer_id)
{
    clearInterval(this.rendering_timer_id);
    this.rendering_timer_id = null;
}
*/
};

/* LiteGraphCanvas input */

//used to block future mouse events (because of im gui)
RGraphCanvas.prototype.blockClick = function() {
  this.block_click = true;
  this.last_mouseclick = 0;
};

RGraphCanvas.prototype.processMouseDown = function(e) {
  if (this.set_canvas_dirty_on_mouse_event) this.dirty_canvas = true;

  if (!this.graph) {
    return;
  }

  this.adjustMouseEvent(e);

  var ref_window = this.getCanvasWindow();
  RGraphCanvas.active_canvas = this;

  //move mouse move event to the window in case it drags outside of the canvas
  this.canvas.removeEventListener("mousemove", this._mousemove_callback);
  ref_window.document.addEventListener(
    "mousemove",
    this._mousemove_callback,
    true
  ); //catch for the entire window
  ref_window.document.addEventListener("mouseup", this._mouseup_callback, true);

  var node = this.graph.getNodeOnPos(
    e.canvasX,
    e.canvasY,
    this.anchor_point,
    this.visible_nodes,
    5
  );
  var skip_action = false;
  var now = getTime();
  var is_double_click = now - this.last_mouseclick < 300;
  this.mouse[0] = e.localX;
  this.mouse[1] = e.localY;
  this.graph_mouse[0] = e.canvasX;
  this.graph_mouse[1] = e.canvasY;
  this.last_click_position = [this.mouse[0], this.mouse[1]];

  this.canvas.focus();

  closeAllContextMenus(ref_window);

  if (this.onMouse) {
    if (this.onMouse(e) == true) return;
  }

  //left button mouse
  if (e.which == 1) {
    if (e.ctrlKey) {
      this.dragging_rectangle = new Float32Array(4);
      this.dragging_rectangle[0] = e.canvasX;
      this.dragging_rectangle[1] = e.canvasY;
      this.dragging_rectangle[2] = 1;
      this.dragging_rectangle[3] = 1;
      skip_action = true;
    }

    var clicking_canvas_bg = false;

    //when clicked on top of a node
    //and it is not interactive
    if (node && this.allow_interaction && !skip_action && !this.read_only) {
      if (!this.live_mode && !node.flags.pinned) {
        this.bringToFront(node);
      } //if it wasn't selected?

      //not dragging mouse to connect two slots
      if (!this.connecting_node && !node.flags.collapsed && !this.live_mode) {
        //Search for corner for resize
        if (
          !skip_action &&
          node.resizable != false &&
          isInsideRectangle(
            e.canvasX,
            e.canvasY,
            node.pos[0] + node.size[0] - 5,
            node.pos[1] + node.size[1] - 5,
            10,
            10
          )
        ) {
          this.graph.beforeChange();
          this.resizing_node = node;
          this.canvas.style.cursor = "se-resize";
          skip_action = true;
        } else if (this.allow_link_interaction) {
          //search for outputs
          if (node.outputs) {
            for (let i = 0, l = node.outputs.length; i < l; ++i) {
              let output = node.outputs[i];
              let link_pos = node.getConnectionPos(false, i, this.anchor_point);
              if (
                isInsideRectangle(
                  e.canvasX,
                  e.canvasY,
                  link_pos[0] - 15,
                  link_pos[1] - 10,
                  30,
                  20
                )
              ) {
                this.connecting_node = node;
                this.connecting_output = output;
                this.connecting_pos = node.getConnectionPos(
                  false,
                  i,
                  this.anchor_point
                );
                this.connecting_slot = i;

                if (e.shiftKey) {
                  node.disconnectOutput(i);
                }

                if (is_double_click) {
                  if (node.onOutputDblClick) {
                    node.onOutputDblClick(i, e);
                  }
                } else {
                  if (node.onOutputClick) {
                    node.onOutputClick(i, e);
                  }
                }

                skip_action = true;
                break;
              }
            }
          }

          //search for inputs
          if (node.inputs) {
            for (let i = 0, l = node.inputs.length; i < l; ++i) {
              let input = node.inputs[i];
              let link_pos = node.getConnectionPos(true, i, this.anchor_point);
              if (
                isInsideRectangle(
                  e.canvasX,
                  e.canvasY,
                  link_pos[0] - 15,
                  link_pos[1] - 10,
                  30,
                  20
                )
              ) {
                if (is_double_click) {
                  if (node.onInputDblClick) {
                    node.onInputDblClick(i, e);
                  }
                } else {
                  if (node.onInputClick) {
                    node.onInputClick(i, e);
                  }
                }

                if (input.link != null) {
                  var link_info = this.graph.links[input.link]; //before disconnecting
                  node.disconnectInput(i);

                  if (this.allow_reconnect_links || e.shiftKey) {
                    this.connecting_node = this.graph._nodes_by_id[
                      link_info.origin_id
                    ];
                    this.connecting_slot = link_info.origin_slot;
                    this.connecting_output = this.connecting_node.outputs[
                      this.connecting_slot
                    ];
                    this.connecting_pos = this.connecting_node.getConnectionPos(
                      false,
                      this.connecting_slot,
                      this.anchor_point
                    );
                  }

                  this.dirty_bgcanvas = true;
                  skip_action = true;
                }
              }
            }
          }
        } //not resizing
      }

      //it wasn't clicked on the links boxes
      if (!skip_action) {
        let block_drag_node = false;
        let pos = [e.canvasX - node.pos[0], e.canvasY - node.pos[1]];

        //widgets
        let widget = this.processNodeWidgets(node, this.graph_mouse, e);
        if (widget) {
          block_drag_node = true;
          this.node_widget = [node, widget];
        }

        //double clicking
        if (is_double_click && this.selected_nodes[node.id]) {
          //double click node
          if (node.onDblClick) {
            const block = node.onDblClick(e, pos, this);
            if (!block) {
              this.processNodeDblClicked(node);
            }
          } else {
            this.processNodeDblClicked(node);
          }
          block_drag_node = true;
        }

        //if do not capture mouse
        if (node.onMouseDown && node.onMouseDown(e, pos, this)) {
          block_drag_node = true;
        } else {
          if (this.live_mode) {
            clicking_canvas_bg = true;
            block_drag_node = true;
          }
        }

        if (!block_drag_node) {
          if (this.allow_dragnodes) {
            this.graph.beforeChange();
            this.node_dragged = node;
          }
          if (!this.selected_nodes[node.id]) {
            this.processNodeSelected(node, e);
          }
        }

        this.dirty_canvas = true;
      }
    } //clicked outside of nodes
    else {
      //search for link connector
      if (!this.read_only)
        for (let i = 0; i < this.visible_links.length; ++i) {
          let link = this.visible_links[i];
          let center = link._pos;
          if (
            !center ||
            e.canvasX < center[0] - 4 ||
            e.canvasX > center[0] + 4 ||
            e.canvasY < center[1] - 4 ||
            e.canvasY > center[1] + 4
          ) {
            continue;
          }
          //link clicked
          if (this.onShowLinkMenu) {
            // return true to chain default behavior with custom callback
            if (this.onShowLinkMenu(link, e)) {
              this.showLinkMenu(link, e);
            }
          }
          this.over_link_center = null; //clear tooltip
          if (this.onUndrawLinkTooltip) {
            this.onUndrawLinkTooltip();
          }
          break;
        }

      this.selected_group = this.graph.getGroupOnPos(e.canvasX, e.canvasY);
      this.selected_group_resizing = false;
      if (this.selected_group && !this.read_only) {
        if (e.ctrlKey) {
          this.dragging_rectangle = null;
        }

        let dist = distance(
          [e.canvasX, e.canvasY],
          [
            this.selected_group.pos[0] + this.selected_group.size[0],
            this.selected_group.pos[1] + this.selected_group.size[1]
          ]
        );
        if (dist * this.ds.scale < 10) {
          this.selected_group_resizing = true;
        } else {
          this.selected_group.recomputeInsideNodes();
        }
      }

      if (is_double_click && !this.read_only && this.allow_searchbox) {
        this.showSearchBox(e);
      }

      clicking_canvas_bg = true;
      if (this.onCanvasClick) {
        this.onCanvasClick();
      }
    }

    if (!skip_action && clicking_canvas_bg && this.allow_dragcanvas) {
      this.dragging_canvas = true;
    }
  } else if (e.which == 2) {
    //middle button
  } else if (e.which == 3) {
    //right button
    if (!this.read_only && this.show_contextual_menu) {
      this.processContextMenu(node, e);
    }
  }

  //TODO
  //if(this.node_selected != prev_selected)
  //	this.onNodeSelectionChange(this.node_selected);

  this.last_mouse[0] = e.localX;
  this.last_mouse[1] = e.localY;
  this.last_mouseclick = getTime();
  this.last_mouse_dragging = true;

  /*
if( (this.dirty_canvas || this.dirty_bgcanvas) && this.rendering_timer_id == null)
    this.draw();
*/

  this.graph.change();

  //this is to ensure to defocus(blur) if a text input element is on focus
  if (
    !ref_window.document.activeElement ||
    (ref_window.document.activeElement.nodeName.toLowerCase() != "input" &&
      ref_window.document.activeElement.nodeName.toLowerCase() != "textarea")
  ) {
    e.preventDefault();
  }
  e.stopPropagation();

  if (this.onMouseDown) {
    this.onMouseDown(e);
  }

  return false;
};

/**
 * Called when a mouse move event has to be processed
 * @method processMouseMove
 **/
RGraphCanvas.prototype.processMouseMove = function(e) {
  if (this.autoresize) {
    this.resize();
  }

  if (this.set_canvas_dirty_on_mouse_event) this.dirty_canvas = true;

  if (!this.graph) {
    return;
  }

  RGraphCanvas.active_canvas = this;
  this.adjustMouseEvent(e);
  var mouse = [e.localX, e.localY];
  this.mouse[0] = mouse[0];
  this.mouse[1] = mouse[1];
  var delta = [mouse[0] - this.last_mouse[0], mouse[1] - this.last_mouse[1]];
  this.last_mouse = mouse;
  this.graph_mouse[0] = e.canvasX;
  this.graph_mouse[1] = e.canvasY;

  if (this.block_click) {
    e.preventDefault();
    return false;
  }

  e.dragging = this.last_mouse_dragging;

  if (this.node_widget) {
    this.processNodeWidgets(
      this.node_widget[0],
      this.graph_mouse,
      e,
      this.node_widget[1]
    );
    this.dirty_canvas = true;
  }

  if (this.dragging_rectangle) {
    this.dragging_rectangle[2] = e.canvasX - this.dragging_rectangle[0];
    this.dragging_rectangle[3] = e.canvasY - this.dragging_rectangle[1];
    this.dirty_canvas = true;
  } else if (this.selected_group && !this.read_only) {
    //moving/resizing a group
    if (this.selected_group_resizing) {
      this.selected_group.size = [
        e.canvasX - this.selected_group.pos[0],
        e.canvasY - this.selected_group.pos[1]
      ];
    } else {
      var deltax = delta[0] / this.ds.scale;
      var deltay = delta[1] / this.ds.scale;
      this.selected_group.move(deltax, deltay, e.ctrlKey);
      if (this.selected_group._nodes.length) {
        this.dirty_canvas = true;
      }
    }
    this.dirty_bgcanvas = true;
  } else if (this.dragging_canvas) {
    this.ds.offset[0] += delta[0] / this.ds.scale;
    this.ds.offset[1] += delta[1] / this.ds.scale;
    this.dirty_canvas = true;
    this.dirty_bgcanvas = true;
  } else if (this.allow_interaction && !this.read_only) {
    if (this.connecting_node) {
      this.dirty_canvas = true;
    }

    //get node over
    var node = this.graph.getNodeOnPos(
      e.canvasX,
      e.canvasY,
      this.anchor_point,
      this.visible_nodes
    );

    //remove mouseover flag
    for (let i = 0, l = this.graph._nodes.length; i < l; ++i) {
      if (this.graph._nodes[i].mouseOver && node != this.graph._nodes[i]) {
        //mouse leave
        this.graph._nodes[i].mouseOver = false;
        if (this.node_over && this.node_over.onMouseLeave) {
          this.node_over.onMouseLeave(e);
        }
        this.node_over = null;
        this.dirty_canvas = true;
      }
    }

    //mouse over a node
    if (node) {
      if (node.redraw_on_mouse) this.dirty_canvas = true;

      //this.canvas.style.cursor = "move";
      if (!node.mouseOver) {
        //mouse enter
        node.mouseOver = true;
        this.node_over = node;
        this.dirty_canvas = true;

        if (node.onMouseEnter) {
          node.onMouseEnter(e);
        }
      }

      //in case the node wants to do something
      if (node.onMouseMove) {
        node.onMouseMove(
          e,
          [e.canvasX - node.pos[0], e.canvasY - node.pos[1]],
          this
        );
      }

      //if dragging a link
      if (this.connecting_node) {
        var pos = this._highlight_input || [0, 0]; //to store the output of isOverNodeInput

        //on top of input
        if (this.isOverNodeBox(node, e.canvasX, e.canvasY)) {
          //mouse on top of the corner box, don't know what to do
        } else {
          //check if I have a slot below de mouse
          var slot = this.isOverNodeInput(node, e.canvasX, e.canvasY, pos);
          if (slot != -1 && node.inputs[slot]) {
            var slot_type = node.inputs[slot].type;
            if (isValidConnection(this.connecting_output.type, slot_type)) {
              this._highlight_input = pos;
            }
          } else {
            this._highlight_input = null;
          }
        }
      }

      //Search for corner
      if (this.canvas) {
        if (
          isInsideRectangle(
            e.canvasX,
            e.canvasY,
            node.pos[0] + node.size[0] - 5,
            node.pos[1] + node.size[1] - 5,
            5,
            5
          )
        ) {
          this.canvas.style.cursor = "se-resize";
        } else {
          this.canvas.style.cursor = "crosshair";
        }
      }
    } else {
      //not over a node

      //search for link connector
      var over_link = null;
      for (let i = 0; i < this.visible_links.length; ++i) {
        var link = this.visible_links[i];
        var center = link._pos;
        if (
          !center ||
          e.canvasX < center[0] - 4 ||
          e.canvasX > center[0] + 4 ||
          e.canvasY < center[1] - 4 ||
          e.canvasY > center[1] + 4
        ) {
          continue;
        }
        over_link = link;
        break;
      }
      if (over_link != this.over_link_center) {
        this.over_link_center = over_link;
        if (this.over_link_center == null) {
          if (this.onUndrawLinkTooltip) {
            this.onUndrawLinkTooltip();
          }
        }
        this.dirty_canvas = true;
      }

      if (this.canvas) {
        this.canvas.style.cursor = "";
      }
    } //end

    //send event to node if capturing input (used with widgets that allow drag outside of the area of the node)
    if (
      this.node_capturing_input &&
      this.node_capturing_input != node &&
      this.node_capturing_input.onMouseMove
    ) {
      this.node_capturing_input.onMouseMove(
        e,
        [
          e.canvasX - this.node_capturing_input.pos[0],
          e.canvasY - this.node_capturing_input.pos[1]
        ],
        this
      );
    }

    //node being dragged
    if (this.node_dragged && !this.live_mode) {
      //console.log("draggin!",this.selected_nodes);
      for (let i in this.selected_nodes) {
        var n = this.selected_nodes[i];
        n.pos[0] += delta[0] / this.ds.scale;
        n.pos[1] += delta[1] / this.ds.scale;
      }

      this.dirty_canvas = true;
      this.dirty_bgcanvas = true;
    }

    if (this.resizing_node && !this.live_mode) {
      //convert mouse to node space
      var desired_size = [
        e.canvasX - this.resizing_node.pos[0],
        e.canvasY - this.resizing_node.pos[1]
      ];
      var min_size = this.resizing_node.computeSize();
      desired_size[0] = Math.max(min_size[0], desired_size[0]);
      desired_size[1] = Math.max(min_size[1], desired_size[1]);
      this.resizing_node.setSize(desired_size);

      this.canvas.style.cursor = "se-resize";
      this.dirty_canvas = true;
      this.dirty_bgcanvas = true;
    }
  }

  e.preventDefault();
  return false;
};

/**
 * Called when a mouse up event has to be processed
 * @method processMouseUp
 **/
RGraphCanvas.prototype.processMouseUp = function(e) {
  if (this.set_canvas_dirty_on_mouse_event) this.dirty_canvas = true;

  if (!this.graph) return;

  var window = this.getCanvasWindow();
  var document = window.document;
  RGraphCanvas.active_canvas = this;

  //restore the mousemove event back to the canvas
  document.removeEventListener("mousemove", this._mousemove_callback, true);
  this.canvas.addEventListener("mousemove", this._mousemove_callback, true);
  document.removeEventListener("mouseup", this._mouseup_callback, true);

  this.adjustMouseEvent(e);
  var now = getTime();
  e.click_time = now - this.last_mouseclick;
  this.last_mouse_dragging = false;
  this.last_click_position = null;

  if (this.block_click) {
    console.log("foo");
    this.block_click = false; //used to avoid sending twice a click in a immediate button
  }

  if (e.which == 1) {
    if (this.node_widget) {
      this.processNodeWidgets(this.node_widget[0], this.graph_mouse, e);
    }

    //left button
    this.node_widget = null;

    if (this.selected_group) {
      let diffx =
        this.selected_group.pos[0] - Math.round(this.selected_group.pos[0]);
      let diffy =
        this.selected_group.pos[1] - Math.round(this.selected_group.pos[1]);
      this.selected_group.move(diffx, diffy, e.ctrlKey);
      this.selected_group.pos[0] = Math.round(this.selected_group.pos[0]);
      this.selected_group.pos[1] = Math.round(this.selected_group.pos[1]);
      if (this.selected_group._nodes.length) {
        this.dirty_canvas = true;
      }
      this.selected_group = null;
    }
    this.selected_group_resizing = false;

    if (this.dragging_rectangle) {
      if (this.graph) {
        let nodes = this.graph._nodes;
        let node_bounding = new Float32Array(4);
        this.deselectAllNodes();
        //compute bounding and flip if left to right
        let w = Math.abs(this.dragging_rectangle[2]);
        let h = Math.abs(this.dragging_rectangle[3]);
        let startx =
          this.dragging_rectangle[2] < 0
            ? this.dragging_rectangle[0] - w
            : this.dragging_rectangle[0];
        let starty =
          this.dragging_rectangle[3] < 0
            ? this.dragging_rectangle[1] - h
            : this.dragging_rectangle[1];
        this.dragging_rectangle[0] = startx;
        this.dragging_rectangle[1] = starty;
        this.dragging_rectangle[2] = w;
        this.dragging_rectangle[3] = h;

        //test against all nodes (not visible because the rectangle maybe start outside
        let to_select = [];
        for (let i = 0; i < nodes.length; ++i) {
          let node = nodes[i];
          node.getBounding(node_bounding);
          if (!overlapBounding(this.dragging_rectangle, node_bounding)) {
            continue;
          } //out of the visible area
          to_select.push(node);
        }
        if (to_select.length) {
          this.selectNodes(to_select);
        }
      }
      this.dragging_rectangle = null;
    } else if (this.connecting_node) {
      //dragging a connection
      this.dirty_canvas = true;
      this.dirty_bgcanvas = true;

      let node = this.graph.getNodeOnPos(
        e.canvasX,
        e.canvasY,
        this.anchor_point,
        this.visible_nodes
      );

      //node below mouse
      if (node) {
        if (
          this.connecting_output.type == EVENT &&
          this.isOverNodeBox(node, e.canvasX, e.canvasY)
        ) {
          this.connecting_node.connect(this.connecting_slot, node, EVENT);
        } else {
          //slot below mouse? connect
          var slot = this.isOverNodeInput(node, e.canvasX, e.canvasY);
          if (slot != -1) {
            this.connecting_node.connect(this.connecting_slot, node, slot);
          } else {
            //not on top of an input
            var input = node.getInputInfo(0);
            //auto connect
            if (this.connecting_output.type == EVENT) {
              this.connecting_node.connect(this.connecting_slot, node, EVENT);
            } else if (
              input &&
              !input.link &&
              isValidConnection(input.type && this.connecting_output.type)
            ) {
              this.connecting_node.connect(this.connecting_slot, node, 0);
            }
          }
        }
      }

      this.connecting_output = null;
      this.connecting_pos = null;
      this.connecting_node = null;
      this.connecting_slot = -1;
    } //not dragging connection
    else if (this.resizing_node) {
      this.dirty_canvas = true;
      this.dirty_bgcanvas = true;
      this.graph.afterChange(this.resizing_node);
      this.resizing_node = null;
    } else if (this.node_dragged) {
      this.dirty_canvas = true;
      this.dirty_bgcanvas = true;
      this.node_dragged.pos[0] = Math.round(this.node_dragged.pos[0]);
      this.node_dragged.pos[1] = Math.round(this.node_dragged.pos[1]);
      if (this.snap_grid.enabled) {
        this.node_dragged.alignToGrid(this.snap_grid.size);
      }
      if (this.onNodeMoved) this.onNodeMoved(this.node_dragged);
      this.graph.afterChange(this.node_dragged);
      this.node_dragged = null;
    } //no node being dragged
    else {
      //get node over
      let node = this.graph.getNodeOnPos(
        e.canvasX,
        e.canvasY,
        this.anchor_point,
        this.visible_nodes
      );

      if (!node && e.click_time < 300) {
        this.deselectAllNodes();
      }

      this.dirty_canvas = true;
      this.dragging_canvas = false;

      if (this.node_over && this.node_over.onMouseUp) {
        this.node_over.onMouseUp(
          e,
          [
            e.canvasX - this.node_over.pos[0],
            e.canvasY - this.node_over.pos[1]
          ],
          this
        );
      }
      if (this.node_capturing_input && this.node_capturing_input.onMouseUp) {
        this.node_capturing_input.onMouseUp(e, [
          e.canvasX - this.node_capturing_input.pos[0],
          e.canvasY - this.node_capturing_input.pos[1]
        ]);
      }
    }
  } else if (e.which == 2) {
    //middle button
    //trace("middle");
    this.dirty_canvas = true;
    this.dragging_canvas = false;
  } else if (e.which == 3) {
    //right button
    //trace("right");
    this.dirty_canvas = true;
    this.dragging_canvas = false;
  }

  /*
if((this.dirty_canvas || this.dirty_bgcanvas) && this.rendering_timer_id == null)
    this.draw();
*/

  this.graph.change();

  e.stopPropagation();
  e.preventDefault();
  return false;
};

/**
 * Called when a mouse wheel event has to be processed
 * @method processMouseWheel
 **/
RGraphCanvas.prototype.processMouseWheel = function(e) {
  if (!this.graph || !this.allow_dragcanvas || !this.allow_zoom_on_wheel) {
    return;
  }

  var delta = e.wheelDeltaY != null ? e.wheelDeltaY : e.detail * -60;

  this.adjustMouseEvent(e);

  var scale = this.ds.scale;

  if (delta > 0) {
    scale *= 1.1;
  } else if (delta < 0) {
    scale *= 1 / 1.1;
  }

  //this.setZoom( scale, [ e.localX, e.localY ] );
  this.ds.changeScale(scale, [e.localX, e.localY]);

  this.graph.change();

  e.preventDefault();
  return false; // prevent default
};

/**
 * returns true if a position (in graph space) is on top of a node little corner box
 * @method isOverNodeBox
 **/
RGraphCanvas.prototype.isOverNodeBox = function(node, canvasx, canvasy) {
  var title_height = NODE_TITLE_HEIGHT;
  if (
    isInsideRectangle(
      canvasx,
      canvasy,
      node.pos[0] + 2,
      node.pos[1] + 2 - title_height,
      title_height - 4,
      title_height - 4
    )
  ) {
    return true;
  }
  return false;
};

/**
 * returns true if a position (in graph space) is on top of a node input slot
 * @method isOverNodeInput
 **/
RGraphCanvas.prototype.isOverNodeInput = function(
  node,
  canvasx,
  canvasy,
  slot_pos
) {
  if (node.inputs) {
    for (let i = 0, l = node.inputs.length; i < l; ++i) {
      var link_pos = node.getConnectionPos(true, i, this.anchor_point);
      var is_inside = false;
      if (node.horizontal) {
        is_inside = isInsideRectangle(
          canvasx,
          canvasy,
          link_pos[0] - 5,
          link_pos[1] - 10,
          10,
          20
        );
      } else {
        is_inside = isInsideRectangle(
          canvasx,
          canvasy,
          link_pos[0] - 10,
          link_pos[1] - 5,
          40,
          10
        );
      }
      if (is_inside) {
        if (slot_pos) {
          slot_pos[0] = link_pos[0];
          slot_pos[1] = link_pos[1];
        }
        return i;
      }
    }
  }
  return -1;
};

/**
 * process a key event
 * @method processKey
 **/
RGraphCanvas.prototype.processKey = function(e) {
  if (!this.graph) {
    return;
  }

  var block_default = false;
  //console.log(e); //debug

  if (e.target.localName == "input") {
    return;
  }

  if (e.type == "keydown") {
    if (e.keyCode == 32) {
      //esc
      this.dragging_canvas = true;
      block_default = true;
    }

    //select all Control A
    if (e.keyCode == 65 && e.ctrlKey) {
      this.selectNodes();
      block_default = true;
    }

    if (e.code == "KeyC" && (e.metaKey || e.ctrlKey) && !e.shiftKey) {
      //copy
      if (this.selected_nodes) {
        this.copyToClipboard();
        block_default = true;
      }
    }

    if (e.code == "KeyV" && (e.metaKey || e.ctrlKey) && !e.shiftKey) {
      //paste
      this.pasteFromClipboard();
    }

    //delete or backspace
    if (e.keyCode == 46 || e.keyCode == 8) {
      if (e.target.localName != "input" && e.target.localName != "textarea") {
        this.deleteSelectedNodes();
        block_default = true;
      }
    }

    //collapse
    //...

    //TODO
    if (this.selected_nodes) {
      for (let i in this.selected_nodes) {
        if (this.selected_nodes[i].onKeyDown) {
          this.selected_nodes[i].onKeyDown(e);
        }
      }
    }
  } else if (e.type == "keyup") {
    if (e.keyCode == 32) {
      this.dragging_canvas = false;
    }

    if (this.selected_nodes) {
      for (let i in this.selected_nodes) {
        if (this.selected_nodes[i].onKeyUp) {
          this.selected_nodes[i].onKeyUp(e);
        }
      }
    }
  }

  this.graph.change();

  if (block_default) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
  }
};

RGraphCanvas.prototype.copyToClipboard = function() {
  let clipboard_info = {
    nodes: [],
    links: []
  };
  let index = 0;
  let selected_nodes_array = [];
  for (let i in this.selected_nodes) {
    let node = this.selected_nodes[i];
    node._relative_id = index;
    selected_nodes_array.push(node);
    index += 1;
  }

  for (let i = 0; i < selected_nodes_array.length; ++i) {
    let node = selected_nodes_array[i];
    let cloned = node.clone();
    if (!cloned) {
      console.warn("node type not found: " + node.type);
      continue;
    }
    clipboard_info.nodes.push(cloned.serialize());
    if (node.inputs && node.inputs.length) {
      for (let j = 0; j < node.inputs.length; ++j) {
        let input = node.inputs[j];
        if (!input || input.link == null) {
          continue;
        }
        let link_info = this.graph.links[input.link];
        if (!link_info) {
          continue;
        }
        let target_node = this.graph.getNodeById(link_info.origin_id);
        if (!target_node || !this.selected_nodes[target_node.id]) {
          //improve this by allowing connections to non-selected nodes
          continue;
        } //not selected
        clipboard_info.links.push([
          target_node._relative_id,
          link_info.origin_slot, //j,
          node._relative_id,
          link_info.target_slot
        ]);
      }
    }
  }
  localStorage.setItem(
    "runegrapheditor_clipboard",
    JSON.stringify(clipboard_info)
  );
};

RGraphCanvas.prototype.pasteFromClipboard = function() {
  let data = localStorage.getItem("runegrapheditor_clipboard");
  if (!data) {
    return;
  }

  this.graph.beforeChange();

  //create nodes
  let clipboard_info = JSON.parse(data);
  let nodes = [];
  for (let i = 0; i < clipboard_info.nodes.length; ++i) {
    let node_data = clipboard_info.nodes[i];
    let node = createNode(node_data.type);
    if (node) {
      node.configure(node_data);
      node.pos[0] += 5;
      node.pos[1] += 5;
      this.graph.add(node);
      nodes.push(node);
    }
  }

  //create links
  for (let i = 0; i < clipboard_info.links.length; ++i) {
    let link_info = clipboard_info.links[i];
    let origin_node = nodes[link_info[0]];
    let target_node = nodes[link_info[2]];
    if (origin_node && target_node)
      origin_node.connect(link_info[1], target_node, link_info[3]);
    else console.warn("Warning, nodes missing on pasting");
  }

  this.selectNodes(nodes);

  this.graph.afterChange();
};

/**
 * process a item drop event on top the canvas
 * @method processDrop
 **/
RGraphCanvas.prototype.processDrop = function(e) {
  e.preventDefault();
  this.adjustMouseEvent(e);

  let pos = [e.canvasX, e.canvasY];
  let node = this.graph
    ? this.graph.getNodeOnPos(pos[0], pos[1], this.anchor_point)
    : null;

  if (!node) {
    let r = null;
    if (this.onDropItem) {
      r = this.onDropItem(event);
    }
    if (!r) {
      this.checkDropItem(e);
    }
    return;
  }

  if (node.onDropFile || node.onDropData) {
    let files = e.dataTransfer.files;
    if (files && files.length) {
      for (let i = 0; i < files.length; i++) {
        let file = e.dataTransfer.files[0];
        let filename = file.name;

        if (node.onDropFile) {
          node.onDropFile(file);
        }

        if (node.onDropData) {
          //prepare reader
          var reader = new FileReader();
          reader.onload = function(event) {
            var data = event.target.result;
            node.onDropData(data, filename, file);
          };

          //read data
          var type = file.type.split("/")[0];
          if (type == "text" || type == "") {
            reader.readAsText(file);
          } else if (type == "image") {
            reader.readAsDataURL(file);
          } else {
            reader.readAsArrayBuffer(file);
          }
        }
      }
    }
  }

  if (node.onDropItem) {
    if (node.onDropItem(event)) {
      return true;
    }
  }

  if (this.onDropItem) {
    return this.onDropItem(event, node);
  }

  return false;
};

//called if the graph doesn't have a default drop item behaviour
RGraphCanvas.prototype.checkDropItem = function(e) {
  if (e.dataTransfer.files.length) {
    var file = e.dataTransfer.files[0];
    var ext = RGraphCanvas.getFileExtension(file.name).toLowerCase();
    var nodetype = getNodeTypesByFileExtension()[ext];
    if (nodetype) {
      this.graph.beforeChange();
      var node = createNode(nodetype.type);
      node.pos = [e.canvasX, e.canvasY];
      this.graph.add(node);
      if (node.onDropFile) {
        node.onDropFile(file);
      }
      this.graph.afterChange();
    }
  }
};

RGraphCanvas.prototype.processNodeDblClicked = function(n) {
  if (this.onShowNodePanel) {
    this.onShowNodePanel(n);
  } else {
    this.showShowNodePanel(n);
  }

  if (this.onNodeDblClicked) {
    this.onNodeDblClicked(n);
  }

  this.setDirty(true);
};

RGraphCanvas.prototype.processNodeSelected = function(node, e) {
  this.selectNode(node, e && e.shiftKey);
  if (this.onNodeSelected) {
    this.onNodeSelected(node);
  }
};

/**
 * selects a given node (or adds it to the current selection)
 * @method selectNode
 **/
RGraphCanvas.prototype.selectNode = function(node, add_to_current_selection) {
  if (node == null) {
    this.deselectAllNodes();
  } else {
    this.selectNodes([node], add_to_current_selection);
  }
};

/**
 * selects several nodes (or adds them to the current selection)
 * @method selectNodes
 **/
RGraphCanvas.prototype.selectNodes = function(nodes, add_to_current_selection) {
  if (!add_to_current_selection) {
    this.deselectAllNodes();
  }

  nodes = nodes || this.graph._nodes;
  for (let i = 0; i < nodes.length; ++i) {
    var node = nodes[i];
    if (node.is_selected) {
      continue;
    }

    if (!node.is_selected && node.onSelected) {
      node.onSelected();
    }
    node.is_selected = true;
    this.selected_nodes[node.id] = node;

    if (node.inputs) {
      for (let j = 0; j < node.inputs.length; ++j) {
        this.highlighted_links[node.inputs[j].link] = true;
      }
    }
    if (node.outputs) {
      for (let j = 0; j < node.outputs.length; ++j) {
        var out = node.outputs[j];
        if (out.links) {
          for (let k = 0; k < out.links.length; ++k) {
            this.highlighted_links[out.links[k]] = true;
          }
        }
      }
    }
  }

  if (this.onSelectionChange) this.onSelectionChange(this.selected_nodes);

  this.setDirty(true);
};

/**
 * removes a node from the current selection
 * @method deselectNode
 **/
RGraphCanvas.prototype.deselectNode = function(node) {
  if (!node.is_selected) {
    return;
  }
  if (node.onDeselected) {
    node.onDeselected();
  }
  node.is_selected = false;

  if (this.onNodeDeselected) {
    this.onNodeDeselected(node);
  }

  //remove highlighted
  if (node.inputs) {
    for (let i = 0; i < node.inputs.length; ++i) {
      delete this.highlighted_links[node.inputs[i].link];
    }
  }
  if (node.outputs) {
    for (let i = 0; i < node.outputs.length; ++i) {
      var out = node.outputs[i];
      if (out.links) {
        for (let j = 0; j < out.links.length; ++j) {
          delete this.highlighted_links[out.links[j]];
        }
      }
    }
  }
};

/**
 * removes all nodes from the current selection
 * @method deselectAllNodes
 **/
RGraphCanvas.prototype.deselectAllNodes = function() {
  if (!this.graph) {
    return;
  }
  var nodes = this.graph._nodes;
  for (let i = 0, l = nodes.length; i < l; ++i) {
    var node = nodes[i];
    if (!node.is_selected) {
      continue;
    }
    if (node.onDeselected) {
      node.onDeselected();
    }
    node.is_selected = false;
    if (this.onNodeDeselected) {
      this.onNodeDeselected(node);
    }
  }
  this.selected_nodes = {};
  this.current_node = null;
  this.highlighted_links = {};
  if (this.onSelectionChange) this.onSelectionChange(this.selected_nodes);
  this.setDirty(true);
};

/**
 * deletes all nodes in the current selection from the graph
 * @method deleteSelectedNodes
 **/
RGraphCanvas.prototype.deleteSelectedNodes = function() {
  this.graph.beforeChange();

  for (let i in this.selected_nodes) {
    var node = this.selected_nodes[i];

    if (node.block_delete) continue;

    //autoconnect when possible (very basic, only takes into account first input-output)
    if (
      node.inputs &&
      node.inputs.length &&
      node.outputs &&
      node.outputs.length &&
      isValidConnection(node.inputs[0].type, node.outputs[0].type) &&
      node.inputs[0].link &&
      node.outputs[0].links &&
      node.outputs[0].links.length
    ) {
      var input_link = node.graph.links[node.inputs[0].link];
      var output_link = node.graph.links[node.outputs[0].links[0]];
      var input_node = node.getInputNode(0);
      var output_node = node.getOutputNodes(0)[0];
      if (input_node && output_node)
        input_node.connect(
          input_link.origin_slot,
          output_node,
          output_link.target_slot
        );
    }
    this.graph.remove(node);
    if (this.onNodeDeselected) {
      this.onNodeDeselected(node);
    }
  }
  this.selected_nodes = {};
  this.current_node = null;
  this.highlighted_links = {};
  this.setDirty(true);
  this.graph.afterChange();
};

/**
 * centers the camera on a given node
 * @method centerOnNode
 **/
RGraphCanvas.prototype.centerOnNode = function(node) {
  this.ds.offset[0] =
    -node.pos[0] -
    node.size[0] * 0.5 +
    (this.canvas.width * 0.5) / this.ds.scale;
  this.ds.offset[1] =
    -node.pos[1] -
    node.size[1] * 0.5 +
    (this.canvas.height * 0.5) / this.ds.scale;
  this.setDirty(true, true);
};

/**
 * adds some useful properties to a mouse event, like the position in graph coordinates
 * @method adjustMouseEvent
 **/
RGraphCanvas.prototype.adjustMouseEvent = function(e) {
  if (this.canvas) {
    let b = this.canvas.getBoundingClientRect();
    e.localX = e.clientX - b.left;
    e.localY = e.clientY - b.top;
  } else {
    e.localX = e.clientX;
    e.localY = e.clientY;
  }

  // e.deltaX = e.localX - this.last_mouse_position[0];
  // e.deltaY = e.localY - this.last_mouse_position[1];

  this.last_mouse_position[0] = e.localX;
  this.last_mouse_position[1] = e.localY;

  e.canvasX = e.localX / this.ds.scale - this.ds.offset[0];
  e.canvasY = e.localY / this.ds.scale - this.ds.offset[1];
};

/**
 * changes the zoom level of the graph (default is 1), you can pass also a place used to pivot the zoom
 * @method setZoom
 **/
RGraphCanvas.prototype.setZoom = function(value, zooming_center) {
  this.ds.changeScale(value, zooming_center);
  /*
if(!zooming_center && this.canvas)
    zooming_center = [this.canvas.width * 0.5,this.canvas.height * 0.5];
var center = this.convertOffsetToCanvas( zooming_center );
this.ds.scale = value;
if(this.scale > this.max_zoom)
    this.scale = this.max_zoom;
else if(this.scale < this.min_zoom)
    this.scale = this.min_zoom;
var new_center = this.convertOffsetToCanvas( zooming_center );
var delta_offset = [new_center[0] - center[0], new_center[1] - center[1]];
this.offset[0] += delta_offset[0];
this.offset[1] += delta_offset[1];
*/

  this.dirty_canvas = true;
  this.dirty_bgcanvas = true;
};

/**
 * converts a coordinate from graph coordinates to canvas2D coordinates
 * @method convertOffsetToCanvas
 **/
RGraphCanvas.prototype.convertOffsetToCanvas = function(pos, out) {
  return this.ds.convertOffsetToCanvas(pos, out);
};

/**
 * converts a coordinate from Canvas2D coordinates to graph space
 * @method convertCanvasToOffset
 **/
RGraphCanvas.prototype.convertCanvasToOffset = function(pos, out) {
  return this.ds.convertCanvasToOffset(pos, out);
};

//converts event coordinates from canvas2D to graph coordinates
RGraphCanvas.prototype.convertEventToCanvasOffset = function(e) {
  var rect = this.canvas.getBoundingClientRect();
  return this.convertCanvasToOffset([
    e.clientX - rect.left,
    e.clientY - rect.top
  ]);
};

/**
 * brings a node to front (above all other nodes)
 * @method bringToFront
 **/
RGraphCanvas.prototype.bringToFront = function(node) {
  var i = this.graph._nodes.indexOf(node);
  if (i == -1) {
    return;
  }

  this.graph._nodes.splice(i, 1);
  this.graph._nodes.push(node);
};

/**
 * sends a node to the back (below all other nodes)
 * @method sendToBack
 **/
RGraphCanvas.prototype.sendToBack = function(node) {
  var i = this.graph._nodes.indexOf(node);
  if (i == -1) {
    return;
  }

  this.graph._nodes.splice(i, 1);
  this.graph._nodes.unshift(node);
};

/* Interaction */

/* RGraphCanvas render */
var temp = new Float32Array(4);

/**
 * checks which nodes are visible (inside the camera area)
 * @method computeVisibleNodes
 **/
RGraphCanvas.prototype.computeVisibleNodes = function(nodes, out) {
  let visible_nodes = out || [];
  visible_nodes.length = 0;
  nodes = nodes || this.graph._nodes;
  for (let i = 0, l = nodes.length; i < l; ++i) {
    let n = nodes[i];

    //skip rendering nodes in live mode
    if (this.live_mode && !n.onDrawBackground && !n.onDrawForeground) {
      continue;
    }

    if (!overlapBounding(this.visible_area, n.getBounding(temp))) {
      continue;
    } //out of the visible area

    visible_nodes.push(n);
  }
  return visible_nodes;
};

/**
 * renders the whole canvas content, by rendering in two separated canvas, one containing the background grid and the connections, and one containing the nodes)
 * @method draw
 **/
RGraphCanvas.prototype.draw = function(force_canvas, force_bgcanvas) {
  if (!this.canvas || this.canvas.width == 0 || this.canvas.height == 0) {
    return;
  }

  //fps counting
  let now = getTime();
  this.render_time = (now - this.last_draw_time) * 0.001;
  this.last_draw_time = now;

  if (this.graph) {
    this.ds.computeVisibleArea();
  }

  if (
    this.dirty_bgcanvas ||
    force_bgcanvas ||
    this.always_render_background ||
    (this.graph &&
      this.graph._last_trigger_time &&
      now - this.graph._last_trigger_time < 1000)
  ) {
    this.drawBackCanvas();
  }

  if (this.dirty_canvas || force_canvas) {
    this.drawFrontCanvas();
  }

  this.fps = this.render_time ? 1.0 / this.render_time : 0;
  this.frame += 1;
};

/**
 * draws the front canvas (the one containing all the nodes)
 * @method drawFrontCanvas
 **/
RGraphCanvas.prototype.drawFrontCanvas = function() {
  this.dirty_canvas = false;

  if (!this.ctx) {
    this.ctx = this.bgcanvas.getContext("2d");
  }
  let ctx = this.ctx;
  if (!ctx) {
    //maybe is using webgl...
    return;
  }

  if (ctx.start2D) {
    ctx.start2D();
  }

  let canvas = this.canvas;

  //reset in case of error
  ctx.restore();
  ctx.setTransform(1, 0, 0, 1, 0, 0);

  //clip dirty area if there is one, otherwise work in full canvas
  if (this.dirty_area) {
    ctx.save();
    ctx.beginPath();
    ctx.rect(
      this.dirty_area[0],
      this.dirty_area[1],
      this.dirty_area[2],
      this.dirty_area[3]
    );
    ctx.clip();
  }

  //clear
  //canvas.width = canvas.width;
  if (this.clear_background) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }

  //draw bg canvas
  if (this.bgcanvas == this.canvas) {
    this.drawBackCanvas();
  } else {
    ctx.drawImage(this.bgcanvas, 0, 0);
  }

  //rendering
  if (this.onRender) {
    this.onRender(canvas, ctx);
  }

  //info widget
  if (this.show_info) {
    this.renderInfo(ctx);
  }

  if (this.graph) {
    //apply transformations
    ctx.save();
    this.ds.toCanvasContext(ctx);

    //draw nodes
    let visible_nodes = this.computeVisibleNodes(null, this.visible_nodes);

    for (let i = 0; i < visible_nodes.length; ++i) {
      let node = visible_nodes[i];

      //transform coords system
      ctx.save();
      ctx.translate(node.pos[0], node.pos[1]);

      //Draw
      this.drawNode(node, ctx);

      //Restore
      ctx.restore();
    }

    //on top (debug)
    if (this.render_execution_order) {
      this.drawExecutionOrder(ctx);
    }

    //connections ontop?
    if (this.graph.config.links_ontop) {
      if (!this.live_mode) {
        this.drawConnections(ctx);
      }
    }

    //current connection (the one being dragged by the mouse)
    if (this.connecting_pos != null) {
      ctx.lineWidth = this.connections_width;
      let link_color = null;

      switch (this.connecting_output.type) {
      case EVENT:
        link_color = this.event_link_color;
        break;
      default:
        link_color = this.connecting_link_color;
      }

      //the connection being dragged by the mouse
      this.renderLink(
        ctx,
        this.connecting_pos,
        [this.graph_mouse[0], this.graph_mouse[1]],
        null,
        false,
        null,
        link_color,
        this.connecting_output.dir ||
          (this.connecting_node.horizontal ? DOWN : RIGHT),
        CENTER
      );

      ctx.beginPath();
      if (
        this.connecting_output.type == EVENT ||
        this.connecting_output.shape == BOX_SHAPE
      ) {
        ctx.rect(
          this.connecting_pos[0] - 6 + 0.5,
          this.connecting_pos[1] - 5 + 0.5,
          14,
          10
        );
      } else {
        ctx.arc(
          this.connecting_pos[0],
          this.connecting_pos[1],
          4,
          0,
          Math.PI * 2
        );
      }
      ctx.fill();

      ctx.fillStyle = "#ffcc00";
      if (this._highlight_input) {
        ctx.beginPath();
        ctx.arc(
          this._highlight_input[0],
          this._highlight_input[1],
          6,
          0,
          Math.PI * 2
        );
        ctx.fill();
      }
    }

    //the selection rectangle
    if (this.dragging_rectangle) {
      ctx.strokeStyle = "#FFF";
      ctx.strokeRect(
        this.dragging_rectangle[0],
        this.dragging_rectangle[1],
        this.dragging_rectangle[2],
        this.dragging_rectangle[3]
      );
    }

    //on top of link center
    if (this.over_link_center) {
      if (this.render_link_tooltip) {
        this.drawLinkTooltip(ctx, this.over_link_center);
      } else if (this.onDrawLinkTooltip) {
        this.onDrawLinkTooltip(ctx, null);
      }
    }

    //custom info
    if (this.onDrawForeground) {
      this.onDrawForeground(ctx, this.visible_rect);
    }

    ctx.restore();
  }

  if (this.onDrawOverlay) {
    this.onDrawOverlay(ctx);
  }

  if (this.dirty_area) {
    ctx.restore();
    //this.dirty_area = null;
  }

  if (ctx.finish2D) {
    //this is a function I use in webgl renderer
    ctx.finish2D();
  }
};

//Draws a button into the canvas overlay and computes if it was clicked using the immediate gui paradigm
RGraphCanvas.prototype.drawButton = function(
  x,
  y,
  w,
  h,
  text,
  bgcolor,
  hovercolor,
  textcolor
) {
  let ctx = this.ctx;
  bgcolor = bgcolor || this.node_default_color;
  hovercolor = hovercolor || "#555";
  textcolor = textcolor || this.node_text_color;

  let pos = this.mouse;
  let hover = isInsideRectangle(pos[0], pos[1], x, y, w, h);
  pos = this.last_click_position;
  let clicked = pos && isInsideRectangle(pos[0], pos[1], x, y, w, h);

  ctx.fillStyle = hover ? hovercolor : bgcolor;
  if (clicked) ctx.fillStyle = "#AAA";
  ctx.beginPath();
  ctx.roundRect(x, y, w, h, 4);
  ctx.fill();

  if (text != null) {
    if (text.constructor == String) {
      ctx.fillStyle = textcolor;
      ctx.textAlign = "center";
      ctx.font = ((h * 0.65) | 0) + "px Arial";
      ctx.fillText(text, x + w * 0.5, y + h * 0.75);
      ctx.textAlign = "left";
    }
  }

  let was_clicked = clicked && !this.block_click;
  if (clicked) this.blockClick();
  return was_clicked;
};

RGraphCanvas.prototype.isAreaClicked = function(x, y, w, h, hold_click) {
  let pos = this.mouse;
  // let hover = isInsideRectangle(pos[0], pos[1], x, y, w, h);
  pos = this.last_click_position;
  let clicked = pos && isInsideRectangle(pos[0], pos[1], x, y, w, h);
  let was_clicked = clicked && !this.block_click;
  if (clicked && hold_click) this.blockClick();
  return was_clicked;
};

/**
 * draws some useful stats in the corner of the canvas
 * @method renderInfo
 **/
RGraphCanvas.prototype.renderInfo = function(ctx, x, y) {
  x = x || 10;
  y = y || this.canvas.height - 80;

  ctx.save();
  ctx.translate(x, y);

  ctx.font = "10px Arial";
  ctx.fillStyle = "#888";
  if (this.graph) {
    ctx.fillText("T: " + this.graph.globaltime.toFixed(2) + "s", 5, 13 * 1);
    ctx.fillText("I: " + this.graph.iteration, 5, 13 * 2);
    ctx.fillText(
      "N: " + this.graph._nodes.length + " [" + this.visible_nodes.length + "]",
      5,
      13 * 3
    );
    ctx.fillText("V: " + this.graph._version, 5, 13 * 4);
    ctx.fillText("FPS:" + this.fps.toFixed(2), 5, 13 * 5);
  } else {
    ctx.fillText("No graph selected", 5, 13 * 1);
  }
  ctx.restore();
};

/**
 * draws the back canvas (the one containing the background and the connections)
 * @method drawBackCanvas
 **/
RGraphCanvas.prototype.drawBackCanvas = function() {
  let canvas = this.bgcanvas;
  if (
    canvas.width != this.canvas.width ||
    canvas.height != this.canvas.height
  ) {
    canvas.width = this.canvas.width;
    canvas.height = this.canvas.height;
  }

  if (!this.bgctx) {
    this.bgctx = this.bgcanvas.getContext("2d");
  }
  let ctx = this.bgctx;
  if (ctx.start) {
    ctx.start();
  }

  //clear
  if (this.clear_background) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }

  var bg_already_painted = false;
  if (this.onRenderBackground) {
    bg_already_painted = this.onRenderBackground(canvas, ctx);
  }

  //reset in case of error
  ctx.restore();
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  this.visible_links.length = 0;

  if (this.graph) {
    //apply transformations
    ctx.save();
    this.ds.toCanvasContext(ctx);

    //render BG
    if (!bg_already_painted) {
      ctx.fillStyle = this.background_color;
      ctx.fillRect(
        this.visible_area[0],
        this.visible_area[1],
        this.visible_area[2] - 2,
        this.visible_area[3] - 2
      );
      ctx.fillStyle = "transparent";
    }

    // (optional grid)
    if (this.background_grid.enabled && this.ds.scale > 0.5 && !bg_already_painted) {
      this.recomputeGrid(ctx);
    }

    //groups
    if (this.graph._groups.length && !this.live_mode) {
      this.drawGroups(canvas, ctx);
    }

    if (this.onDrawBackground) {
      this.onDrawBackground(ctx, this.visible_area);
    }
    if (this.onBackgroundRender) {
      //LEGACY
      console.error(
        "WARNING! onBackgroundRender deprecated, now is named onDrawBackground "
      );
      this.onBackgroundRender = null;
    }

    //DEBUG: show clipping area
    //ctx.fillStyle = "red";
    //ctx.fillRect( this.visible_area[0] + 10, this.visible_area[1] + 10, this.visible_area[2] - 20, this.visible_area[3] - 20);

    //bg
    if (this.render_canvas_border) {
      ctx.strokeStyle = "#235";
      ctx.strokeRect(0, 0, canvas.width, canvas.height);
    }

    if (this.render_connections_shadows) {
      ctx.shadowColor = "#000";
      ctx.shadowOffsetX = 0;
      ctx.shadowOffsetY = 0;
      ctx.shadowBlur = 6;
    } else {
      ctx.shadowColor = "rgba(0,0,0,0)";
    }

    //draw connections
    if (!this.live_mode) {
      this.drawConnections(ctx);
    }

    ctx.shadowColor = "rgba(0,0,0,0)";

    //restore state
    ctx.restore();
  }

  if (ctx.finish) {
    ctx.finish();
  }

  this.dirty_bgcanvas = false;
  this.dirty_canvas = true; //to force to repaint the front canvas with the bgcanvas
};

var temp_vec2 = new Float32Array(2);

/**
 * draws the given node inside the canvas
 * @method drawNode
 **/
RGraphCanvas.prototype.drawNode = function(node, ctx) {
  // var glow = false;
  this.current_node = node;

  let color = node.color || node.constructor.color || this.node_default_color;
  let bgcolor =
    node.bgcolor || node.constructor.bgcolor || this.node_default_bgcolor;

  let render_text =
    this.render_slot_labels &&
    (node.render_text ||
     node.constructor.render_text ||
     (!low_quality && RENDER_TEXT));
  let title_height =
    node.node_title_height ||
    node.constructor.node_title_height ||
    NODE_TITLE_HEIGHT;

  //shadow and glow
  // if (node.mouseOver) {
  //   glow = true;
  // }

  var low_quality = this.ds.scale < 0.6; //zoomed out

  //only render if it forces it to do it
  if (this.live_mode) {
    if (!node.flags.collapsed) {
      ctx.shadowColor = "transparent";
      if (node.onDrawForeground) {
        node.onDrawForeground(ctx, this, this.canvas);
      }
    }
    return;
  }

  var editor_alpha = this.editor_alpha;
  ctx.globalAlpha = editor_alpha;

  if (this.render_shadows && !low_quality) {
    ctx.shadowColor = DEFAULT_SHADOW_COLOR;
    ctx.shadowOffsetX = 2 * this.ds.scale;
    ctx.shadowOffsetY = 2 * this.ds.scale;
    ctx.shadowBlur = 3 * this.ds.scale;
  } else {
    ctx.shadowColor = "transparent";
  }

  //custom draw collapsed method (draw after shadows because they are affected)
  if (
    node.flags.collapsed &&
    node.onDrawCollapsed &&
    node.onDrawCollapsed(ctx, this) == true
  ) {
    return;
  }

  //clip if required (mask)
  var shape = node._shape || BOX_SHAPE;
  var size = temp_vec2;
  temp_vec2.set(node.size);
  var horizontal = node.horizontal; // || node.flags.horizontal;
  var point_size = node.point_size || node.constructor.point_size || POINT_SIZE;

  if (shape !== BULGE_SHAPE && node.flags.collapsed) {
    ctx.font = this.inner_text_font;
    var title = node.getTitle ? node.getTitle() : node.title;
    if (title != null) {
      node._collapsed_width =
        node.collapsed_width ||
        node.constructor.collapsed_width ||
        NODE_COLLAPSED_WIDTH;
      size[0] = node._collapsed_width;
      size[1] = 0;
    }
  }

  if (node.clip_area) {
    //Start clipping
    ctx.save();
    ctx.beginPath();
    if (shape === BOX_SHAPE) {
      ctx.rect(0, 0, size[0], size[1]);
    } else if (shape === ROUND_SHAPE) {
      ctx.roundRect(0, 0, size[0], size[1], 10);
    } else if (shape === CIRCLE_SHAPE) {
      ctx.arc(size[0] * 0.5, size[1] * 0.5, size[0] * 0.5, 0, Math.PI * 2);
    } else if (shape === BULGE_SHAPE) {
      ctx.rect(-size[0] * 0.5, -size[1] * 0.5, size[0], size[1]);
    }
    ctx.clip();
  }

  //draw shape
  if (node.has_errors) {
    bgcolor = "red";
  }
  this.drawNodeShape(
    node,
    ctx,
    size,
    color,
    bgcolor,
    node.is_selected,
    node.mouseOver
  );
  ctx.shadowColor = "transparent";

  //draw foreground
  if (node.onDrawForeground) {
    node.onDrawForeground(ctx, this, this.canvas);
  }

  //connection slots
  ctx.textAlign = horizontal ? "center" : "left";
  ctx.font = this.inner_text_font;

  let out_slot = this.connecting_output;
  ctx.lineWidth = 1;

  let max_y = 0;
  let slot_pos = new Float32Array(2); //to reuse
  let slot;

  //render inputs and outputs
  if (!node.flags.collapsed) {
    //input connection slots
    if (this.render_slots && node.inputs) {
      for (let i = 0; i < node.inputs.length; i++) {
        slot = node.inputs[i];

        ctx.globalAlpha = editor_alpha;
        //change opacity of incompatible slots when dragging a connection
        if (
          this.connecting_node &&
          !isValidConnection(slot.type, out_slot.type)
        ) {
          ctx.globalAlpha = 0.4 * editor_alpha;
        }

        ctx.fillStyle =
          slot.link != null
            ? slot.color_on || this.default_connection_color.input_on
            : slot.color_off || this.default_connection_color.input_off;

        let pos = node.getConnectionPos(true, i, this.anchor_point, slot_pos);
        pos[0] -= node.pos[0];
        pos[1] -= node.pos[1];
        if (max_y < pos[1] + NODE_SLOT_HEIGHT * 0.5) {
          max_y = pos[1] + NODE_SLOT_HEIGHT * 0.5;
        }

        ctx.beginPath();

        if (slot.type == EVENT || slot.shape == BOX_SHAPE) {
          if (horizontal) {
            ctx.rect(pos[0] - 5 + 0.5, pos[1] - 8 + 0.5, 10, 14);
          } else {
            ctx.rect(pos[0] - 6 + 0.5, pos[1] - 5 + 0.5, 14, 10);
          }
        } else if (slot.shape == ARROW_SHAPE) {
          ctx.moveTo(pos[0] + 8, pos[1] + 0.5);
          ctx.lineTo(pos[0] - 4, pos[1] + 6 + 0.5);
          ctx.lineTo(pos[0] - 4, pos[1] - 6 + 0.5);
          ctx.closePath();
        } else {
          if (low_quality) ctx.rect(pos[0] - 4, pos[1] - 4, 8, 8);
          //faster
          else ctx.arc(pos[0], pos[1], 4, 0, Math.PI * 2);
        }
        ctx.fill();

        //render name
        if (render_text) {
          var text = slot.label != null ? slot.label : slot.name;
          if (text) {
            ctx.fillStyle = this.node_text_color;
            if (horizontal || slot.dir == UP) {
              ctx.fillText(text, pos[0], pos[1] - 10);
            } else {
              ctx.fillText(text, pos[0] + 10, pos[1] + 5);
            }
          }
        }
      }
    }

    //output connection slots
    if (this.connecting_node) {
      ctx.globalAlpha = 0.4 * editor_alpha;
    }

    ctx.textAlign = horizontal ? "center" : "right";
    ctx.strokeStyle = "black";
    if (this.render_slots && node.outputs) {
      for (let i = 0; i < node.outputs.length; i++) {
        slot = node.outputs[i];

        let pos = node.getConnectionPos(false, i, this.anchor_point, slot_pos);
        pos[0] -= node.pos[0];
        pos[1] -= node.pos[1];
        if (max_y < pos[1] + NODE_SLOT_HEIGHT * 0.5) {
          max_y = pos[1] + NODE_SLOT_HEIGHT * 0.5;
        }

        ctx.fillStyle =
          slot.links && slot.links.length
            ? slot.color_on || this.default_connection_color.output_on
            : slot.color_off || this.default_connection_color.output_off;
        ctx.beginPath();
        //ctx.rect( node.size[0] - 14,i*14,10,10);

        if (slot.type == EVENT || slot.shape == BOX_SHAPE) {
          if (horizontal) {
            ctx.rect(pos[0] - 5 + 0.5, pos[1] - 8 + 0.5, 10, 14);
          } else {
            ctx.rect(pos[0] - 6 + 0.5, pos[1] - 5 + 0.5, 14, 10);
          }
        } else if (slot.shape == ARROW_SHAPE) {
          ctx.moveTo(pos[0] + 8, pos[1] + 0.5);
          ctx.lineTo(pos[0] - 4, pos[1] + 6 + 0.5);
          ctx.lineTo(pos[0] - 4, pos[1] - 6 + 0.5);
          ctx.closePath();
        } else {
          if (low_quality) ctx.rect(pos[0] - 4, pos[1] - 4, 8, 8);
          else ctx.arc(pos[0], pos[1], 4, 0, Math.PI * 2);
        }

        //trigger
        //if(slot.node_id != null && slot.slot == -1)
        //	ctx.fillStyle = "#F85";

        //if(slot.links != null && slot.links.length)
        ctx.fill();
        if (!low_quality) ctx.stroke();

        //render output name
        if (render_text) {
          let text = slot.label != null ? slot.label : slot.name;
          if (text) {
            ctx.fillStyle = NODE_TEXT_COLOR;
            if (horizontal || slot.dir == DOWN) {
              ctx.fillText(text, pos[0], pos[1] - 8);
            } else {
              ctx.fillText(text, pos[0] - 10, pos[1] + 5);
            }
          }
        }
      }
    }

    ctx.textAlign = "left";
    ctx.globalAlpha = 1;

    let show_widgets = shape !== BULGE_SHAPE;
    if ("show_widgets" in node) {
      show_widgets = node.show_widgets;
    } else if ("show_widgets" in node.constructor) {
      show_widgets = node.constructor.show_widgets;
    }
    if (node.widgets && show_widgets) {
      let widgets_y = max_y;
      if (horizontal || node.widgets_up) {
        widgets_y = 2;
      }
      if (node.widgets_start_y != null) widgets_y = node.widgets_start_y;
      this.drawNodeWidgets(
        node,
        widgets_y,
        ctx,
        this.node_widget && this.node_widget[0] == node
          ? this.node_widget[1]
          : null
      );
    }
  } else {
    let input_slot = null;
    let output_slot = null;

    //get first connected slot to render
    if (this.render_collapsed_slots && node.inputs) {
      for (let i = 0; i < node.inputs.length; i++) {
        slot = node.inputs[i];
        if (slot.link == null) {
          continue;
        }
        input_slot = slot;
        break;
      }
    }
    if (this.render_collapsed_slots && node.outputs) {
      for (let i = 0; i < node.outputs.length; i++) {
        slot = node.outputs[i];
        if (!slot.links || !slot.links.length) {
          continue;
        }
        output_slot = slot;
      }
    }

    // if bulge shape
    if (shape === BULGE_SHAPE) {
      if (input_slot) {
        ctx.fillStyle =
          input_slot && input_slot.link
            ? input_slot.color_on || this.default_connection_color.output_on
            : input_slot.color_off || this.default_connection_color.output_off;

        let m = 1;
        if (this.anchor_point.startsWith("n")) m = -1;
        if (this.anchor_point.startsWith("s")) m = 1;
        const { x: offX, y: offY } = getAnchorOffset(this.anchor_point, size);
        let x = offX;
        let y = offY + m * point_size * 0.5; //center
        if (horizontal) {
          x = node._collapsed_width * 0.5;
          y = -NODE_TITLE_HEIGHT;
        }
        ctx.beginPath();
        ctx.arc(x, y, 4, 0, Math.PI * 2);
        ctx.fill();
      }

      if (output_slot) {
        ctx.fillStyle =
          output_slot && output_slot.links && output_slot.links.length
            ? output_slot.color_on || this.default_connection_color.output_on
            : output_slot.color_off || this.default_connection_color.output_off;

        let m = 1;
        if (this.anchor_point.startsWith("n")) m = -1;
        if (this.anchor_point.startsWith("s")) m = 1;
        const { x: offX, y: offY } = getAnchorOffset(this.anchor_point, size);
        let x = offX + point_size;
        let y = offY + m * point_size * 0.5; //center
        if (horizontal) {
          x = node._collapsed_width * 0.5;
          y = -title_height;
        }
        ctx.beginPath();
        ctx.arc(x, y, 4, 0, Math.PI * 2);
        ctx.fill();
      }
    } else if (this.render_collapsed_slots) {
      // else if collapsed
      if (input_slot) {
        let m = 1;
        if (this.anchor_point.startsWith("n")) m = -1;
        const { x: offX, y: offY } = getAnchorOffset(this.anchor_point, [
          size[0],
          title_height
        ]);
        let x = offX;
        let y = offY + m * title_height * 0.5; //center
        if (horizontal) {
          x = node._collapsed_width * 0.5;
          y = -title_height;
        }
        ctx.fillStyle = this.default_connection_color.collapsed;
        ctx.beginPath();
        if (slot.type == EVENT || slot.shape == BOX_SHAPE) {
          ctx.rect(x - 7 + 0.5, y - 4, 14, 8);
        } else if (slot.shape == ARROW_SHAPE) {
          ctx.moveTo(x + 8, y);
          ctx.lineTo(x + -4, y - 4);
          ctx.lineTo(x + -4, y + 4);
          ctx.closePath();
        } else {
          ctx.arc(x, y, 4, 0, Math.PI * 2);
        }
        ctx.fill();
      }

      if (output_slot) {
        let m = 1;
        if (this.anchor_point.startsWith("n")) m = -1;
        const { x: offX, y: offY } = getAnchorOffset(this.anchor_point, [
          size[0],
          title_height
        ]);
        let x = offX + node._collapsed_width + 1;
        let y = offY + m * title_height * 0.5; //center
        if (horizontal) {
          x = node._collapsed_width * 0.5;
          y = 0;
        }
        ctx.fillStyle = this.default_connection_color.collapsed;
        ctx.beginPath();
        if (slot.type == EVENT || slot.shape == BOX_SHAPE) {
          ctx.rect(x - 7 + 0.5, y - 4, 14, 8);
        } else if (slot.shape == ARROW_SHAPE) {
          ctx.moveTo(x + 8, y);
          ctx.lineTo(x + -4, y - 4);
          ctx.lineTo(x + -4, y + 4);
          ctx.closePath();
        } else {
          ctx.arc(x, y, 4, 0, Math.PI * 2);
        }
        ctx.fill();
      }
    }
  }

  ctx.globalAlpha = 1.0;
};

//used by this.over_link_center
RGraphCanvas.prototype.drawLinkTooltip = function(ctx, link) {
  var pos = link._pos;
  if (this.link_tooltip_style.with_point) {
    ctx.fillStyle = "black";
    ctx.beginPath();
    ctx.arc(pos[0], pos[1], 3, 0, Math.PI * 2);
    ctx.fill();
  }

  var text = null;

  if (this.link_tooltip_text) {
    text = this.link_tooltip_text;
  } else {
    if (link.data == null) return;
  
    if (this.onDrawLinkTooltip)
      if (this.onDrawLinkTooltip(ctx, link, this) == true) return;
  
    var data = link.data;
  
    if (data.constructor == Number) text = data.toFixed(2);
    else if (data.constructor == String) text = "\"" + data + "\"";
    else if (data.constructor == Boolean) text = String(data);
    else if (data.toToolTip) text = data.toToolTip();
    else text = "[" + data.constructor.name + "]";
  
    if (text == null) return;
    text = text.substr(0, 30); //avoid weird
  }

  ctx.font = `${this.link_tooltip_style.font_size}px ${this.link_tooltip_style.font}`;
  var info = ctx.measureText(text);
  var w = info.width + 20;
  var h = 24;
  ctx.shadowColor = "rgba(0, 0, 0, 0.15)";
  ctx.shadowOffsetX = 2;
  ctx.shadowOffsetY = 2;
  ctx.shadowBlur = 3;
  ctx.fillStyle = this.link_tooltip_style.fill;
  ctx.lineWidth = this.link_tooltip_style.line_width;
  ctx.strokeStyle = this.link_tooltip_style.stroke_color;
  ctx.beginPath();
  switch (this.link_tooltip_style.shape) {
  case BOX_SHAPE:
    ctx.rect(pos[0] - w * 0.5, pos[1] - 15 - h, w, h, 3, 3);
    break;
  case ROUND_SHAPE:
  default:
    ctx.roundRect(pos[0] - w * 0.5, pos[1] - 15 - h, w, h, 3, 3);
    break;
  }
  if (this.link_tooltip_style.with_arrow) {
    ctx.moveTo(pos[0] - 10, pos[1] - 15);
    ctx.lineTo(pos[0] + 10, pos[1] - 15);
    ctx.lineTo(pos[0], pos[1] - 5);
  }
  ctx.fill();
  ctx.stroke();
  ctx.shadowColor = "transparent";
  ctx.textAlign = "center";
  ctx.fillStyle = this.link_tooltip_style.text_color;
  ctx.fillText(text, pos[0], pos[1] - 15 - h * 0.3);
};

/**
 * draws the shape of the given node in the canvas
 * @method drawNodeShape
 **/
var tmp_area = new Float32Array(4);

RGraphCanvas.prototype.drawNodeShape = function(
  node,
  ctx,
  size,
  fgcolor,
  bgcolor,
  selected,
  mouse_over
) {
  //bg rect
  ctx.strokeStyle = fgcolor;
  ctx.fillStyle = bgcolor;

  var title_height =
    node.node_title_height ||
    node.constructor.node_title_height ||
    NODE_TITLE_HEIGHT;
  var low_quality = this.ds.scale < 0.5;

  //render node area depending on shape
  var shape = node._shape || node.constructor.shape || NODE_DEFAULT_SHAPE;
  var point_size = node.point_size || node.constructor.point_size || POINT_SIZE;

  var title_mode = node.title_mode || node.constructor.title_mode || this.title_mode;

  let _status;
  // eslint-disable-next-line no-prototype-builtins
  if (node.hasOwnProperty("_status")) {
    _status = node._status;
  }
  // eslint-disable-next-line no-prototype-builtins
  else if (node.constructor.hasOwnProperty("_status")) {
    _status = node.constructor._status;
  }

  var render_title = true;
  if (title_mode == TRANSPARENT_TITLE) {
    render_title = false;
  } else if (title_mode == AUTOHIDE_TITLE && mouse_over) {
    render_title = true;
  }
  var render_title_box = RENDER_TITLE_BOX;
  // eslint-disable-next-line no-prototype-builtins
  if (node.hasOwnProperty("render_title_box")) {
    render_title_box = node.render_title_box;
  }
  // eslint-disable-next-line no-prototype-builtins
  else if (node.constructor.hasOwnProperty("render_title_box")) {
    render_title_box = node.constructor.render_title_box;
  }
  var render_title_label = true;
  // eslint-disable-next-line no-prototype-builtins
  if (node.hasOwnProperty("render_title_label")) {
    render_title_label = node.render_title_label;
  }
  // eslint-disable-next-line no-prototype-builtins
  else if (node.constructor.hasOwnProperty("render_title_label")) {
    render_title_label = node.constructor.render_title_label;
  }

  var title_text_offset = node.title_text_offset ||
    node.constructor.title_text_offset || { x: 0, y: 0 };
  var node_title_align =
    node.node_title_align || node.constructor.node_title_align || "left";
  var node_title_anchor =
    node.node_title_anchor || node.constructor.node_title_anchor || "nw";
  var node_title_padding =
    node.node_title_padding || node.constructor.node_title_padding || 10;

  var stroke = node.stroke || node.constructor.stroke;

  var area = tmp_area;
  area[2] = size[0] + 1; //w
  area[3] = (shape !== BULGE_SHAPE && render_title) ? size[1] + title_height : size[1]; //h
  let title_y;
  switch (this.anchor_point) {
  case "nw":
    area[0] = 0; //x
    area[1] = 0; //y
    title_y = 0;
    break;
  case "n":
    area[0] = -size[0] * 0.5;
    area[1] = 0;
    title_y = 0;
    break;
  case "ne":
    area[0] = -size[0];
    area[1] = 0;
    title_y = 0;
    break;
  case "w":
    area[0] = 0;
    area[1] = -area[3] * 0.5;
    title_y = -area[3] * 0.5;
    break;
  case "middle":
    area[0] = -size[0] * 0.5;
    area[1] = -area[3] * 0.5;
    title_y = -area[3] * 0.5;
    break;
  case "e":
    area[0] = -size[0];
    area[1] = -area[3] * 0.5;
    title_y = -area[3] * 0.5;
    break;
  case "sw":
    area[0] = 0;
    area[1] = -area[3];
    title_y = -area[3];
    break;
  case "s":
    area[0] = -size[0] * 0.5;
    area[1] = -area[3];
    title_y = -area[3];
    break;
  case "se":
    area[0] = -size[0];
    area[1] = -area[3];
    title_y = -area[3];
    break;
  }

  var old_alpha = ctx.globalAlpha;

  ctx.drawGeometry({
    node,
    shape,
    area,
    size,
    stroke,
    low_quality,
  });
  ctx.fill();

  //separator
  if (shape !== BULGE_SHAPE && !node.flags.collapsed) {
    ctx.shadowColor = "transparent";
    ctx.fillStyle = "rgba(0,0,0,0.2)";
    ctx.fillRect(area[0], title_y + title_height - 1, area[2], 2);
  }

  //border
  if (shape !== BULGE_SHAPE && this.node_border) {
    ctx.shadowColor = "transparent";
    ctx.fillStyle = this.node_border.fill;
    ctx.fillRect(0, -1, area[2], this.node_border.height);
    area[3] += this.node_border.height;
  }
  
  ctx.shadowColor = "transparent";

  ctx.anchor_point = this.anchor_point;
  if (node.onDrawBackground) {
    node.onDrawBackground(ctx, this, this.canvas, this.graph_mouse);
  }
  delete ctx.anchor_point;

  //title bg (remember, it is rendered ABOVE the node)
  if (render_title || title_mode != TRANSPARENT_TITLE) {
    //title bar (except for bulge shape)
    if (shape !== BULGE_SHAPE) {
      if (node.onDrawTitleBar) {
        node.onDrawTitleBar(ctx, title_height, size, this.ds.scale, fgcolor);
      } else if (
        title_mode != TRANSPARENT_TITLE &&
        (node.constructor.title_color || this.render_title_colored)
      ) {
        var title_color = node.constructor.title_color || fgcolor;

        if (node.flags.collapsed && this.render_shadows_if_collapsed) {
          ctx.shadowColor = DEFAULT_SHADOW_COLOR;
        }

        //* gradient test
        if (this.use_gradients) {
          var grad = RGraphCanvas.gradients[title_color];
          if (!grad) {
            grad = RGraphCanvas.gradients[
              title_color
            ] = ctx.createLinearGradient(0, 0, 400, 0);
            grad.addColorStop(0, title_color);
            grad.addColorStop(1, "#000");
          }
          ctx.fillStyle = grad;
        } else {
          ctx.fillStyle = title_color;
        }

        //ctx.globalAlpha = 0.5 * old_alpha;
        ctx.beginPath();
        if (shape == BOX_SHAPE || low_quality) {
          ctx.rect(area[0], title_y, size[0] + 1, title_height);
        } else if (shape == ROUND_SHAPE || shape == CARD_SHAPE) {
          ctx.roundRect(
            area[0],
            title_y,
            size[0] + 1,
            title_height,
            this.node_round_radius,
            node.flags.collapsed ? this.node_round_radius : 0
          );
        }
        ctx.fill();
        ctx.shadowColor = "transparent";
      }

      //title box
      var box_size = 10;
      if (render_title_box) {
        let boxColor;
        if (_status in COLORS[this.color_mode].NODE_STATUS_COLORS) {
          boxColor = COLORS[this.color_mode].NODE_STATUS_COLORS[_status];
        } else {
          boxColor = node.boxcolor || this.node_default_boxcolor;
        }
        ctx.fillStyle = boxColor;
        if (node.onDrawTitleBox) {
          node.onDrawTitleBox(ctx, title_y - title_height, size, this.ds.scale);
        } else if (
          shape == ROUND_SHAPE ||
          shape == CIRCLE_SHAPE ||
          shape == CARD_SHAPE
        ) {
          if (low_quality)
            ctx.fillRect(
              area[0] + title_height * 0.5 - box_size * 0.5,
              title_y - title_height * -0.5 - box_size * 0.5,
              box_size,
              box_size
            );
          else {
            ctx.beginPath();
            ctx.arc(
              area[0] + title_height * 0.5,
              title_y - title_height * -0.5,
              box_size * 0.5,
              0,
              Math.PI * 2
            );
            ctx.fill();
          }
        } else {
          ctx.fillRect(
            area[0] + (title_height - box_size) * 0.5,
            title_y - (title_height - box_size) * -0.5,
            box_size,
            box_size
          );
        }
      }
      ctx.globalAlpha = old_alpha;
    }

    //title text
    if (node.onDrawTitleText) {
      node.onDrawTitleText(
        ctx,
        title_height,
        size,
        this.ds.scale,
        this.title_text_font,
        selected
      );
    }
    if (render_title_label && !low_quality) {
      let title = String(node.getTitle());
      if (title) {
        ctx.font = this.title_text_font;
        ctx.textAlign = node_title_align;
        if (selected) {
          ctx.fillStyle = this.node_selected_title_color;
        } else {
          ctx.fillStyle =
            node.constructor.title_text_color || this.node_title_color;
        }
        const title_width = getTextWidth(title, this.title_text_font);
        let position;
        if (shape === BULGE_SHAPE) {
          let m = 0;
          if (this.anchor_point.startsWith("n")) m = -1;
          const { x: offX, y: offY } = getAnchorOffset(this.anchor_point, [
            size[0],
            size[1]
          ]);
          position = [
            offX + (size[0] - title_width) * 0.5,
            offY + m * size[1] - node_title_padding
          ];
          ctx.fillText(title, position[0], position[1]);
        } else {
          position = (shape !== BULGE_SHAPE && render_title_box)
            ? [
              title_height * 0.5 + box_size + 10,
              (title_height + this.title_text_size - 2) * 0.5
            ]
            : getAnchoredPosition(
              node_title_anchor,
              size[0],
              title_height,
              title_width,
              this.title_text_size + 1,
              node_title_padding
            );
          position[0] += area[0] + title_text_offset.x;
          position[1] += title_y + title_text_offset.y;
          if (node.flags.collapsed) {
            ctx.fillText(
              shape === BULGE_SHAPE ? title : title.substr(0, 20), // avoid too long titles
              position[0],
              position[1]
            );
          } else {
            ctx.fillText(title, position[0], position[1]);
          }
        }
      }
    }

    //custom title render
    if (node.onDrawTitle) {
      node.onDrawTitle(ctx);
    }
  }

  // for non-bulge shapes, check for the stroke
  // and draw it if need be
  if (shape !== BULGE_SHAPE && stroke) {
    ctx.drawGeometry({
      node,
      shape,
      area,
      size,
      stroke,
      low_quality,
    });
    ctx.strokeStyle = stroke.color;
    ctx.lineWidth = stroke.width;
    ctx.stroke();
  }

  if (node.clip_area) {
    ctx.restore();
  }

  //render selection marker
  if (selected) {
    if (node.onBounding) {
      node.onBounding(area);
    }

    if (this.render_selection_marker) {
      const selection_style =
        node.selection_style || node.constructor.selection_style || { line_width: 1, color: this.node_box_outline_color, dash: null, offset: 6 };
      if (shape !== BOX_SHAPE && title_mode === TRANSPARENT_TITLE) {
        area[1] -= title_height;
        area[3] += title_height;
      }
      ctx.beginPath();
      if (shape === BOX_SHAPE) {
        ctx.rect(
          -selection_style.offset + area[0],
          -selection_style.offset + area[1],
          selection_style.offset * 2 + area[2],
          selection_style.offset * 2 + area[3]
        );
      } else if (
        shape === ROUND_SHAPE ||
        (shape === CARD_SHAPE && node.flags.collapsed)
      ) {
        ctx.roundRect(
          -selection_style.offset + area[0],
          -selection_style.offset + area[1],
          selection_style.offset * 2 + area[2],
          selection_style.offset * 2 + area[3],
          this.node_round_radius * 2
        );
      } else if (shape === CARD_SHAPE) {
        ctx.roundRect(
          -selection_style.offset + area[0],
          -selection_style.offset + area[1],
          selection_style.offset * 2 + area[2],
          selection_style.offset * 2 + area[3],
          this.node_round_radius * 2,
          2
        );
      } else if (shape === CIRCLE_SHAPE) {
        let [x, y] = [size[0] * 0.5, -size[0] * 0.5];
        if (!render_title) y -= title_height * 0.5;
        ctx.arc(x + 1, y + 1, size[0] * 0.5 + selection_style.offset, 0, Math.PI * 2);
      } else if (shape === BULGE_SHAPE) {
        let bulge = this.node_bulge;
        // eslint-disable-next-line no-prototype-builtins
        if (node.hasOwnProperty("node_bulge")) {
          bulge = node.node_bulge;
        }
        // eslint-disable-next-line no-prototype-builtins
        else if (node.constructor.hasOwnProperty("node_bulge")) {
          bulge = node.constructor.node_bulge;
        }    
        const offset = selection_style.offset - bulge * 0.125 * size[0];
        ctx.drawClosedSpline([
          - size[0] * 0.5 - offset,
          - size[1] * 0.5 - offset,
          size[0] * 0.5 + offset,
          - size[1] * 0.5 - offset,
          size[0] * 0.5 + offset,
          size[1] * 0.5 + offset,
          - size[0] * 0.5 - offset,
          size[1] * 0.5 + offset,
        ], bulge * 0.5);
      }
      ctx.save();
      ctx.lineWidth = selection_style.line_width;
      ctx.globalAlpha = 0.8;
      ctx.strokeStyle = selection_style.color;
      if (selection_style.dash != null) {
        ctx.setLineDash(selection_style.dash);
      }
      ctx.stroke();
      ctx.restore();
      ctx.strokeStyle = fgcolor;
      ctx.globalAlpha = 1;
    }
  }

  // render icon in middle of shape if any
  let icon = node.icon || node.constructor.icon;
  let node_icon_padding = node.node_icon_padding || node.constructor.node_icon_padding || [0, 0];
  if (icon) {
    if (typeof icon === "string") {
      icon = {
        src: icon,
        width: 32,
        height: 32,
      };
    } else {
      if (!icon.width) icon.width = 32;
      if (!icon.height) icon.height = 32;
    }
    const { width: iw, height: ih } = icon;
    const pos = getAnchoredPosition("middle", size[0], size[1], iw, ih, 0);
    if (icon.parts) {
      ctx.lineWidth = icon.lineWidth || 1;
      ctx.save();
      ctx.translate(area[0] + pos[0] + node_icon_padding[0], area[1] - pos[1] + ih * 0.5 + node_icon_padding[1]);
      for (let part of icon.parts) {
        ctx.fillStyle = part.fillColor;
        ctx.strokeStyle = part.strokeColor;
        var p = new Path2D(part.path);
        ctx.stroke(p);
        ctx.fill(p);
      }
      ctx.restore();
    } else if (icon.src) {
      // create fake image to load the source and add it to the canvas
      // if it hasn't been mounted yet (do not add it to the DOM)
      if (!Object.prototype.hasOwnProperty.call(this.canvas_images, icon)) {
        const img = document.createElement("img");
        img.src = icon.src;
        this.canvas_images[icon.src] = img;
        // force redraw (in case img loading is not immediate)
        this.dirty_canvas = true;
      }
      // optional offset for BULGE_SHAPE
      if (shape === BULGE_SHAPE) {
        let mx = 0;
        if (this.anchor_point === "middle") mx = 0;
        else if (this.anchor_point.endsWith("w")) mx = 1;
        else if (this.anchor_point.endsWith("e")) mx = -1;
        let my = 0;
        if (this.anchor_point.startsWith("n")) my = -1;
        if (this.anchor_point.startsWith("s")) my = 1;
        pos[0] += mx * point_size * 0.5;
        pos[1] += my * point_size * 0.5;
      }
      // draw image
      ctx.drawImage(
        this.canvas_images[icon.src],
        - size[0] * 0.5 + pos[0] + node_icon_padding[0],
        - size[1] * 0.5 - pos[1] + node_icon_padding[1],
        iw,
        ih
      );
    }
  }
};

var margin_area = new Float32Array(4);
var link_bounding = new Float32Array(4);
var tempA = new Float32Array(2);
var tempB = new Float32Array(2);

/**
 * draws every connection visible in the canvas
 * OPTIMIZE THIS: pre-catch connections position instead of recomputing them every time
 * @method drawConnections
 **/
RGraphCanvas.prototype.drawConnections = function(ctx) {
  var now = getTime();
  var visible_area = this.visible_area;
  margin_area[0] = visible_area[0] - 20;
  margin_area[1] = visible_area[1] - 20;
  margin_area[2] = visible_area[2] + 40;
  margin_area[3] = visible_area[3] + 40;

  //draw connections
  ctx.lineWidth = this.connections_width;

  ctx.fillStyle = "#AAA";
  ctx.strokeStyle = "#AAA";
  ctx.globalAlpha = this.editor_alpha;
  //for every node
  var nodes = this.graph._nodes;
  for (let n = 0, l = nodes.length; n < l; ++n) {
    var node = nodes[n];
    //for every input (we render just inputs because it is easier as every slot can only have one input)
    if (!node.inputs || !node.inputs.length) {
      continue;
    }

    for (let i = 0; i < node.inputs.length; ++i) {
      var input = node.inputs[i];
      if (!input || input.link == null) {
        continue;
      }
      var link_id = input.link;
      var link = this.graph.links[link_id];
      if (!link) {
        continue;
      }

      //find link info
      var start_node = this.graph.getNodeById(link.origin_id);
      if (start_node == null) {
        continue;
      }
      var start_node_slot = link.origin_slot;
      var start_node_slotpos = null;
      if (start_node_slot == -1) {
        start_node_slotpos = [start_node.pos[0] + 10, start_node.pos[1] + 10];
      } else {
        start_node_slotpos = start_node.getConnectionPos(
          false,
          start_node_slot,
          this.anchor_point,
          tempA
        );
      }
      var end_node_slotpos = node.getConnectionPos(
        true,
        i,
        this.anchor_point,
        tempB
      );

      //compute link bounding
      link_bounding[0] = start_node_slotpos[0];
      link_bounding[1] = start_node_slotpos[1];
      link_bounding[2] = end_node_slotpos[0] - start_node_slotpos[0];
      link_bounding[3] = end_node_slotpos[1] - start_node_slotpos[1];
      if (link_bounding[2] < 0) {
        link_bounding[0] += link_bounding[2];
        link_bounding[2] = Math.abs(link_bounding[2]);
      }
      if (link_bounding[3] < 0) {
        link_bounding[1] += link_bounding[3];
        link_bounding[3] = Math.abs(link_bounding[3]);
      }

      //skip links outside of the visible area of the canvas
      if (!overlapBounding(link_bounding, margin_area)) {
        continue;
      }

      var start_slot = start_node.outputs[start_node_slot];
      var end_slot = node.inputs[i];
      if (!start_slot || !end_slot) {
        continue;
      }
      var start_dir = start_slot.dir || (start_node.horizontal ? DOWN : RIGHT);
      var end_dir = end_slot.dir || (node.horizontal ? UP : LEFT);

      this.renderLink(
        ctx,
        start_node_slotpos,
        end_node_slotpos,
        link,
        false,
        0,
        null,
        start_dir,
        end_dir,
        i
      );

      //event triggered rendered on top
      if (link && link._last_time && now - link._last_time < 1000) {
        var f = 2.0 - (now - link._last_time) * 0.002;
        var tmp = ctx.globalAlpha;
        ctx.globalAlpha = tmp * f;
        this.renderLink(
          ctx,
          start_node_slotpos,
          end_node_slotpos,
          link,
          true,
          f,
          "white",
          start_dir,
          end_dir
        );
        ctx.globalAlpha = tmp;
      }
    }
  }
  ctx.globalAlpha = 1;
};

/**
 * draws a link between two points
 * @method renderLink
 * @param {vec2} a start pos
 * @param {vec2} b end pos
 * @param {Object} link the link object with all the link info
 * @param {boolean} skip_border ignore the shadow of the link
 * @param {boolean} flow show flow animation (for events)
 * @param {string} color the color for the link
 * @param {number} start_dir the direction enum
 * @param {number} end_dir the direction enum
 * @param {number} num_sublines number of sublines (useful to represent vec3 or rgb)
 * @param {number} link_index index of the link in the inputs or outputs
 **/
RGraphCanvas.prototype.renderLink = function(
  ctx,
  a,
  b,
  link,
  skip_border,
  flow,
  color,
  start_dir,
  end_dir,
  link_index = null,
  num_sublines = 1
) {
  if (link) {
    this.visible_links.push(link);
  }

  //choose color
  if (!color && link) {
    color = link.color || this.link_type_colors[link.type];
  }
  if (!color) {
    color = this.default_link_color;
  }
  if (link != null && this.highlighted_links[link.id]) {
    color = this.highlighted_link_color || this.default_highlighted_link_color;
  }

  const link_offset = 8;
  start_dir = start_dir || RIGHT;
  end_dir = end_dir || LEFT;

  var dist = distance(a, b);

  if (this.render_connections_border && this.ds.scale > 0.6) {
    ctx.lineWidth = this.connections_width + 4;
  }
  ctx.lineJoin = "round";
  num_sublines = num_sublines || 1;
  if (num_sublines > 1) {
    ctx.lineWidth = 0.5;
  }

  // if there is no index, then link is being created with drag:
  // draw a linear node between the two positions
  const start_bend_offset_x = this.snake_link_bend_start_offset_x;
  const mid_bend_offset_x = this.snake_link_bend_mid_offset_x;
  // path is "weirdly stretch" if angle is < 30°
  const path_is_weirdly_stretch = a[0] - b[0] > 0.5 * (b[1] - a[1]);
  let start_x, start_y, end_x, end_y, half_y, mx, my;

  if (link_index === null) {
    ctx.moveTo(a[0], a[1]);
    ctx.lineTo(b[0], b[1]);
  } else {
    //begin line shape
    ctx.beginPath();
    for (let i = 0; i < num_sublines; i += 1) {
      let offsety = (i - (num_sublines - 1) * 0.5) * 5;
      if (this.links_render_mode == SPLINE_LINK) {
        ctx.moveTo(a[0], a[1] + offsety);
        let start_offset_x = 0;
        let start_offset_y = 0;
        let end_offset_x = 0;
        let end_offset_y = 0;
        switch (start_dir) {
        case LEFT:
          start_offset_x = dist * -0.25;
          break;
        case RIGHT:
          start_offset_x = dist * 0.25;
          break;
        case UP:
          start_offset_y = dist * -0.25;
          break;
        case DOWN:
          start_offset_y = dist * 0.25;
          break;
        }
        switch (end_dir) {
        case LEFT:
          end_offset_x = dist * -0.25;
          break;
        case RIGHT:
          end_offset_x = dist * 0.25;
          break;
        case UP:
          end_offset_y = dist * -0.25;
          break;
        case DOWN:
          end_offset_y = dist * 0.25;
          break;
        }
        if (this.allow_snake_links && path_is_weirdly_stretch) {
          start_y = a[1] + start_offset_y + offsety;
          half_y = (a[1] + start_offset_y + offsety + b[1] + offsety) * 0.5;
          end_y = b[1] + offsety;
          ctx.moveTo(a[0], start_y);
          ctx.lineTo(a[0] + start_bend_offset_x, start_y);
          ctx.bezierCurveTo(
            a[0] + mid_bend_offset_x,
            start_y,
            a[0] + mid_bend_offset_x,
            half_y,
            a[0] + start_bend_offset_x,
            half_y
          );
          ctx.lineTo(b[0] - start_bend_offset_x, half_y);
          ctx.bezierCurveTo(
            b[0] - mid_bend_offset_x,
            half_y,
            b[0] - mid_bend_offset_x,
            end_y,
            b[0] - start_bend_offset_x,
            end_y
          );
          ctx.lineTo(b[0], end_y);
        } else {
          ctx.bezierCurveTo(
            a[0] + start_offset_x,
            a[1] + start_offset_y + offsety,
            b[0] + end_offset_x,
            b[1] + end_offset_y + offsety,
            b[0],
            b[1] + offsety
          );
        }
      } else if (this.links_render_mode == LINEAR_LINK) {
        ctx.moveTo(a[0], a[1] + offsety);
        let start_offset_x = 0;
        let start_offset_y = 0;
        let end_offset_x = 0;
        let end_offset_y = 0;
        switch (start_dir) {
        case LEFT:
          start_offset_x = -1;
          break;
        case RIGHT:
          start_offset_x = 1;
          break;
        case UP:
          start_offset_y = -1;
          break;
        case DOWN:
          start_offset_y = 1;
          break;
        }
        switch (end_dir) {
        case LEFT:
          end_offset_x = -1;
          break;
        case RIGHT:
          end_offset_x = 1;
          break;
        case UP:
          end_offset_y = -1;
          break;
        case DOWN:
          end_offset_y = 1;
          break;
        }
        if (this.allow_snake_links && path_is_weirdly_stretch) {
          start_y = a[1] + start_offset_y + offsety;
          half_y = (a[1] + start_offset_y + offsety + b[1] + offsety) * 0.5;
          end_y = b[1] + offsety;
          ctx.moveTo(a[0], start_y);
          ctx.lineTo(a[0] + mid_bend_offset_x, start_y);
          ctx.lineTo(a[0] + mid_bend_offset_x, half_y);
          ctx.lineTo(b[0] - mid_bend_offset_x, half_y);
          ctx.lineTo(b[0] - mid_bend_offset_x, end_y);
          ctx.lineTo(b[0], end_y);
        } else {
          let l = 15;
          ctx.lineTo(
            a[0] + start_offset_x * l,
            a[1] + start_offset_y * l + offsety
          );
          ctx.lineTo(b[0] + end_offset_x * l, b[1] + end_offset_y * l + offsety);
          ctx.lineTo(b[0], b[1] + offsety);
        }
      } else if (this.links_render_mode == ROUNDED_LINK) {
        ctx.moveTo(a[0], a[1]);
        start_x = a[0];
        start_y = a[1];
        end_x = b[0];
        end_y = b[1];
        if (start_dir == RIGHT) {
          start_x += 10;
        } else {
          start_y += 10;
        }
        if (end_dir == LEFT) {
          end_x -= 10;
        } else {
          end_y -= 10;
        }
        mx = start_x < end_x ? 1 : -1;
        my = start_y < end_y ? 1 : -1;
        const corner_radius = this.links_corner_radius_size;
        if (this.allow_snake_links && path_is_weirdly_stretch) {
          start_y = a[1] + offsety;
          half_y = (a[1] + offsety + b[1] + offsety) * 0.5;
          end_y = b[1] + offsety;
          ctx.moveTo(a[0], start_y);
          ctx.lineTo(a[0] + mid_bend_offset_x - corner_radius, start_y);
          ctx.arcTo(
            a[0] + mid_bend_offset_x,
            start_y,
            a[0] + mid_bend_offset_x,
            start_y + my * corner_radius,
            corner_radius
          );
          ctx.lineTo(a[0] + mid_bend_offset_x, start_y + my * corner_radius);
          ctx.lineTo(a[0] + mid_bend_offset_x, half_y - my * corner_radius);
          ctx.arcTo(
            a[0] + mid_bend_offset_x,
            half_y,
            a[0] + mid_bend_offset_x - corner_radius,
            half_y,
            corner_radius
          );
          ctx.lineTo(a[0] + mid_bend_offset_x - corner_radius, half_y);
          ctx.lineTo(b[0] - mid_bend_offset_x + corner_radius, half_y);
          ctx.arcTo(
            b[0] - mid_bend_offset_x,
            half_y,
            b[0] - mid_bend_offset_x,
            half_y + my * corner_radius,
            corner_radius
          );
          ctx.lineTo(b[0] - mid_bend_offset_x, half_y + my * corner_radius);
          ctx.lineTo(b[0] - mid_bend_offset_x, end_y - my * corner_radius);
          ctx.arcTo(
            b[0] - mid_bend_offset_x,
            end_y,
            b[0] - mid_bend_offset_x + corner_radius,
            end_y,
            corner_radius
          );
          ctx.lineTo(b[0] - mid_bend_offset_x + corner_radius, end_y);
          ctx.lineTo(b[0], end_y);
        } else {
          if (Math.abs(start_x - end_x) < corner_radius * 2 ||
              Math.abs(start_y - end_y) < corner_radius * 2) {
            ctx.lineTo(start_x, start_y);
            ctx.lineTo(end_x, end_y);
            ctx.lineTo(b[0], b[1]);
          } else {
            const mid_x = (start_x + end_x) * 0.5 - link_index * mx * my * link_offset;
            ctx.lineTo(start_x, start_y);
            ctx.lineTo(mid_x - mx * corner_radius, start_y);
            ctx.arcTo(
              mid_x,
              start_y,
              mid_x,
              start_y + my * corner_radius,
              corner_radius
            );
            ctx.lineTo(mid_x, start_y + my * corner_radius);
            ctx.lineTo(mid_x, end_y - my * corner_radius);
            ctx.arcTo(
              mid_x,
              end_y,
              mid_x + mx * corner_radius,
              end_y,
              corner_radius
            );
            ctx.lineTo(mid_x + mx * corner_radius, end_y);
            ctx.lineTo(end_x, end_y);
            ctx.lineTo(b[0], b[1]);
          }
        }
      } else if (this.links_render_mode == STRAIGHT_LINK) {
        ctx.moveTo(a[0], a[1]);
        start_x = a[0];
        start_y = a[1];
        end_x = b[0];
        end_y = b[1];
        if (start_dir == RIGHT) {
          start_x += 10;
        } else {
          start_y += 10;
        }
        if (end_dir == LEFT) {
          end_x -= 10;
        } else {
          end_y -= 10;
        }
        mx = start_x < end_x ? 1 : -1;
        my = start_y < end_y ? 1 : -1;
        const mid_x = (start_x + end_x) * 0.5 - link_index * mx * my * link_offset;
        ctx.lineTo(start_x, start_y);
        ctx.lineTo(mid_x, start_y);
        ctx.lineTo(mid_x, end_y);
        ctx.lineTo(end_x, end_y);
        ctx.lineTo(b[0], b[1]);
      } else {
        return;
      } //unknown
    }
  }

  //rendering the outline of the connection can be a little bit slow
  if (this.render_connections_border && this.ds.scale > 0.6 && !skip_border) {
    ctx.strokeStyle = "rgba(0,0,0,0.5)";
    ctx.stroke();
  }

  ctx.lineWidth = this.connections_width;
  ctx.fillStyle = ctx.strokeStyle = color;
  ctx.stroke();
  //end line shape

  // if need be, add flow arrows
  const draw_arrows = this.render_links_arrows && this.allow_snake_links && path_is_weirdly_stretch;
  if (draw_arrows) {
    const arrow_size = 4;
    ctx.lineWidth = 2;

    let dx, dy;
    if (this.links_render_mode === SPLINE_LINK) {
      dx = (start_bend_offset_x + mid_bend_offset_x) * 0.666;
    } else if (this.links_render_mode === LINEAR_LINK || this.links_render_mode === ROUNDED_LINK) {
      dx = mid_bend_offset_x;
    }
    dy = (start_y + half_y) * 0.5;

    // if the source node is above the target node, then the arrows
    // are pointing down; else they are pointing up
    const arrow_rotation = start_y < end_y ? 1 : -1;

    ctx.beginPath();
    ctx.moveTo(a[0] + dx - 1.5 * arrow_size, dy - arrow_rotation * arrow_size);
    ctx.lineTo(a[0] + dx, dy);
    ctx.lineTo(a[0] + dx + 1.5 * arrow_size, dy - arrow_rotation * arrow_size);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo((a[0] + b[0]) * 0.5 + arrow_size, half_y - 1.5 * arrow_size);
    ctx.lineTo((a[0] + b[0]) * 0.5, half_y);
    ctx.lineTo((a[0] + b[0]) * 0.5 + arrow_size, half_y + 1.5 * arrow_size);
    ctx.stroke();

    ctx.beginPath();
    dy = (half_y + end_y) * 0.5;
    ctx.moveTo(b[0] - dx - 1.5 * arrow_size, dy - arrow_rotation * arrow_size);
    ctx.lineTo(b[0] - dx, dy);
    ctx.lineTo(b[0] - dx + 1.5 * arrow_size, dy - arrow_rotation * arrow_size);
    ctx.stroke();
  }

  let pos = this.computeConnectionPoint(a, b, draw_arrows ? 1 : 0.5, start_dir, end_dir);
  if (draw_arrows) {
    pos[0] -= start_bend_offset_x + 5;
  }
  if (this.links_render_mode === STRAIGHT_LINK || this.links_render_mode === ROUNDED_LINK) {
    pos[0] -= link_index * mx * my * link_offset;
  }
  if (link && link._pos) {
    link._pos[0] = pos[0];
    link._pos[1] = pos[1];
  }

  //render arrow in the middle
  if (this.ds.scale >= 0.6 && this.highquality_render && end_dir != CENTER) {
    //render arrow
    if (this.render_connection_arrows) {
      //compute two points in the connection
      let posA = this.computeConnectionPoint(a, b, 0.25, start_dir, end_dir);
      let posB = this.computeConnectionPoint(a, b, 0.26, start_dir, end_dir);
      let posC = this.computeConnectionPoint(a, b, 0.75, start_dir, end_dir);
      let posD = this.computeConnectionPoint(a, b, 0.76, start_dir, end_dir);

      //compute the angle between them so the arrow points in the right direction
      let angleA = 0;
      let angleB = 0;
      if (this.render_curved_connections) {
        angleA = -Math.atan2(posB[0] - posA[0], posB[1] - posA[1]);
        angleB = -Math.atan2(posD[0] - posC[0], posD[1] - posC[1]);
      } else {
        angleB = angleA = b[1] > a[1] ? 0 : Math.PI;
      }

      //render arrow
      ctx.save();
      ctx.translate(posA[0], posA[1]);
      ctx.rotate(angleA);
      ctx.beginPath();
      ctx.moveTo(-5, -3);
      ctx.lineTo(0, +7);
      ctx.lineTo(+5, -3);
      ctx.fill();
      ctx.restore();
      ctx.save();
      ctx.translate(posC[0], posC[1]);
      ctx.rotate(angleB);
      ctx.beginPath();
      ctx.moveTo(-5, -3);
      ctx.lineTo(0, +7);
      ctx.lineTo(+5, -3);
      ctx.fill();
      ctx.restore();
    }

    //circle
    ctx.beginPath();
    ctx.save();
    ctx.fillStyle = this.link_middle_point_style.fill;
    ctx.strokeStyle = this.link_middle_point_style.stroke_color;
    ctx.lineWidth = this.link_middle_point_style.line_width;
    ctx.arc(pos[0], pos[1], this.link_middle_point_radius, 0, Math.PI * 2);
    ctx.fill();
    ctx.stroke();
    ctx.restore();
    if (this.link_middle_point_label) {
      ctx.save();
      const font = `${this.link_middle_point_label_font_size}px Arial`;
      ctx.font = font;
      const text_width = getTextWidth(this.link_middle_point_label, font);
      ctx.fillText(
        this.link_middle_point_label,
        pos[0] - text_width * 0.5,
        pos[1] + this.link_middle_point_label_font_size * 0.333
      );
      ctx.restore();
    }
  }

  //render flowing points
  if (flow) {
    ctx.fillStyle = "red";
    for (let i = 0; i < 5; ++i) {
      let f = (getTime() * 0.001 + i * 0.2) % 1;
      let pos = this.computeConnectionPoint(a, b, f, start_dir, end_dir);
      ctx.beginPath();
      ctx.arc(pos[0], pos[1], 5, 0, 2 * Math.PI);
      ctx.fill();
    }
  }
};

//returns the link center point based on curvature
RGraphCanvas.prototype.computeConnectionPoint = function(
  a,
  b,
  t,
  start_dir,
  end_dir
) {
  start_dir = start_dir || RIGHT;
  end_dir = end_dir || LEFT;

  let dist = distance(a, b);
  let p0 = a;
  let p1 = [a[0], a[1]];
  let p2 = [b[0], b[1]];
  let p3 = b;

  switch (start_dir) {
  case LEFT:
    p1[0] += dist * -0.25;
    break;
  case RIGHT:
    p1[0] += dist * 0.25;
    break;
  case UP:
    p1[1] += dist * -0.25;
    break;
  case DOWN:
    p1[1] += dist * 0.25;
    break;
  }
  switch (end_dir) {
  case LEFT:
    p2[0] += dist * -0.25;
    break;
  case RIGHT:
    p2[0] += dist * 0.25;
    break;
  case UP:
    p2[1] += dist * -0.25;
    break;
  case DOWN:
    p2[1] += dist * 0.25;
    break;
  }

  let c1 = (1 - t) * (1 - t) * (1 - t);
  let c2 = 3 * ((1 - t) * (1 - t)) * t;
  let c3 = 3 * (1 - t) * (t * t);
  let c4 = t * t * t;

  let x = c1 * p0[0] + c2 * p1[0] + c3 * p2[0] + c4 * p3[0];
  let y = c1 * p0[1] + c2 * p1[1] + c3 * p2[1] + c4 * p3[1];
  return [x, y];
};

RGraphCanvas.prototype.drawExecutionOrder = function(ctx) {
  ctx.shadowColor = "transparent";
  ctx.globalAlpha = 0.25;

  ctx.textAlign = "center";
  ctx.strokeStyle = "white";
  ctx.globalAlpha = 0.75;

  let visible_nodes = this.visible_nodes;
  for (let i = 0; i < visible_nodes.length; ++i) {
    let node = visible_nodes[i];
    ctx.fillStyle = "black";
    ctx.fillRect(
      node.pos[0] - NODE_TITLE_HEIGHT,
      node.pos[1] - NODE_TITLE_HEIGHT,
      NODE_TITLE_HEIGHT,
      NODE_TITLE_HEIGHT
    );
    if (node.order == 0) {
      ctx.strokeRect(
        node.pos[0] - NODE_TITLE_HEIGHT + 0.5,
        node.pos[1] - NODE_TITLE_HEIGHT + 0.5,
        NODE_TITLE_HEIGHT,
        NODE_TITLE_HEIGHT
      );
    }
    ctx.fillStyle = "#FFF";
    ctx.fillText(
      node.order,
      node.pos[0] + NODE_TITLE_HEIGHT * -0.5,
      node.pos[1] - 6
    );
  }
  ctx.globalAlpha = 1;
};

/**
 * draws the widgets stored inside a node
 * @method drawNodeWidgets
 **/
RGraphCanvas.prototype.drawNodeWidgets = function(
  node,
  posY,
  ctx,
  active_widget
) {
  if (!node.widgets || !node.widgets.length) {
    return 0;
  }

  let title_height =
    node.node_title_height ||
    node.constructor.node_title_height ||
    NODE_TITLE_HEIGHT;
  const size = ["w", "middle", "e"].includes(this.anchor_point)
    ? [node.size[0], node.size[1] - title_height]
    : node.size;
  const { x: offX, y: offY } = getAnchorOffset(this.anchor_point, size);

  let width = node.size[0];
  let widgets = node.widgets;
  posY += 2 + offY;
  let H = NODE_WIDGET_HEIGHT;
  let show_text = this.ds.scale > 0.5;
  ctx.save();
  ctx.globalAlpha = this.editor_alpha;
  let outline_color = this.widget_outline_color;
  let background_color = this.widget_bgcolor;
  let text_color = this.widget_text_color;
  let secondary_text_color = this.widget_secondary_text_color;
  let margin = 15;

  for (let i = 0; i < widgets.length; ++i) {
    var w = widgets[i];
    var y = posY;
    if (w.y) {
      y = w.y;
    }
    w.last_y = y;
    ctx.strokeStyle = outline_color;
    ctx.fillStyle = "#222";
    ctx.textAlign = "left";
    if (w.disabled) ctx.globalAlpha *= 0.5;
    var widget_width = w.width || width;

    switch (w.type) {
    case "button":
      if (w.clicked) {
        ctx.fillStyle = "#AAA";
        w.clicked = false;
        this.dirty_canvas = true;
      }
      ctx.fillRect(margin + offX, y, widget_width - margin * 2, H);
      if (show_text && !w.disabled)
        ctx.strokeRect(margin + offX, y, widget_width - margin * 2, H);
      if (show_text) {
        ctx.textAlign = "center";
        ctx.fillStyle = text_color;
        ctx.fillText(w.name, widget_width * 0.5, y + H * 0.7);
      }
      break;
    case "toggle":
      ctx.textAlign = "left";
      ctx.strokeStyle = outline_color;
      ctx.fillStyle = background_color;
      ctx.beginPath();
      if (show_text) {
        ctx.roundRect(
          margin + offX,
          posY,
          widget_width - margin * 2,
          H,
          H * 0.5
        );
      } else {
        ctx.rect(margin + offX, posY, widget_width - margin * 2, H);
      }
      ctx.fill();
      if (show_text && !w.disabled) ctx.stroke();
      ctx.fillStyle = w.value ? "#89A" : "#333";
      ctx.beginPath();
      ctx.arc(
        widget_width - margin * 2 + offX,
        y + H * 0.5,
        H * 0.36,
        0,
        Math.PI * 2
      );
      ctx.fill();
      if (show_text) {
        ctx.fillStyle = secondary_text_color;
        if (w.name != null) {
          ctx.fillText(w.name, margin * 2 + offX, y + H * 0.7);
        }
        ctx.fillStyle = w.value ? text_color : secondary_text_color;
        ctx.textAlign = "right";
        ctx.fillText(
          w.value ? w.options.on || "true" : w.options.off || "false",
          widget_width - 40 + offX,
          y + H * 0.7
        );
      }
      break;
    case "slider":
      ctx.fillStyle = background_color;
      ctx.fillRect(margin + offX, y, widget_width - margin * 2, H);
      var range = w.options.max - w.options.min;
      var nvalue = (w.value - w.options.min) / range;
      ctx.fillStyle = active_widget == w ? "#89A" : "#678";
      ctx.fillRect(margin + offX, y, nvalue * (widget_width - margin * 2), H);
      if (show_text && !w.disabled)
        ctx.strokeRect(margin + offX, y, widget_width - margin * 2, H);
      if (w.marker) {
        var marker_nvalue = (w.marker - w.options.min) / range;
        ctx.fillStyle = "#AA9";
        ctx.fillRect(
          margin + marker_nvalue * (widget_width - margin * 2) + offX,
          y,
          2,
          H
        );
      }
      if (show_text) {
        ctx.textAlign = "center";
        ctx.fillStyle = text_color;
        ctx.fillText(
          w.name + "  " + Number(w.value).toFixed(2),
          widget_width * 0.5 + offX,
          y + H * 0.7
        );
      }
      break;
    case "number":
    case "combo":
      ctx.textAlign = "left";
      ctx.strokeStyle = outline_color;
      ctx.fillStyle = background_color;
      ctx.beginPath();
      if (show_text) {
        ctx.roundRect(
          margin + offX,
          posY,
          widget_width - margin * 2,
          H,
          H * 0.5
        );
      } else {
        ctx.rect(margin + offX, posY, widget_width - margin * 2, H);
      }
      ctx.fill();
      if (show_text) {
        if (!w.disabled) ctx.stroke();
        ctx.fillStyle = text_color;
        if (!w.disabled) {
          ctx.beginPath();
          ctx.moveTo(margin + 16 + offX, posY + 5);
          ctx.lineTo(margin + 6 + offX, posY + H * 0.5);
          ctx.lineTo(margin + 16 + offX, posY + H - 5);
          ctx.fill();
          ctx.beginPath();
          ctx.moveTo(widget_width - margin - 16 + offX, posY + 5);
          ctx.lineTo(widget_width - margin - 6 + offX, posY + H * 0.5);
          ctx.lineTo(widget_width - margin - 16 + offX, posY + H - 5);
          ctx.fill();
        }
        ctx.fillStyle = secondary_text_color;
        ctx.fillText(w.name, margin * 2 + 5 + offX, y + H * 0.7);
        ctx.fillStyle = text_color;
        ctx.textAlign = "right";
        if (w.type == "number") {
          ctx.fillText(
            Number(w.value).toFixed(
              w.options.precision != undefined ? w.options.precision : 3
            ),
            widget_width - margin * 2 - 20 + offX,
            y + H * 0.7
          );
        } else {
          var v = w.value;
          if (w.options.values) {
            var values = w.options.values;
            if (values.constructor == Function) values = values();
            if (values && values.constructor != Array) v = values[w.value];
          }
          ctx.fillText(v, widget_width - margin * 2 - 20 + offX, y + H * 0.7);
        }
      }
      break;
    case "string":
    case "text":
      ctx.textAlign = "left";
      ctx.strokeStyle = outline_color;
      ctx.fillStyle = background_color;
      ctx.beginPath();
      if (show_text) {
        ctx.roundRect(
          margin + offX,
          posY,
          widget_width - margin * 2,
          H,
          H * 0.5
        );
      } else {
        ctx.rect(margin + offX, posY, widget_width - margin * 2, H);
      }
      ctx.fill();
      if (show_text) {
        ctx.save();
        ctx.beginPath();
        ctx.rect(margin + offX, posY, widget_width - margin * 2, H);
        ctx.clip();

        ctx.stroke();
        ctx.fillStyle = secondary_text_color;
        if (w.name != null) {
          ctx.fillText(w.name, margin * 2 + offX, y + H * 0.7);
        }
        ctx.fillStyle = text_color;
        ctx.textAlign = "right";
        ctx.fillText(
          String(w.value).substr(0, 30),
          widget_width - margin * 2 + offX,
          y + H * 0.7
        ); //30 chars max
        ctx.restore();
      }
      break;
    default:
      if (w.draw) {
        w.draw(ctx, node, widget_width, y, H);
      }
      break;
    }
    posY += (w.computeSize ? w.computeSize(widget_width)[1] : H) + 4;
    ctx.globalAlpha = this.editor_alpha;
  }
  ctx.restore();
  ctx.textAlign = "left";
};

/**
 * process an event on widgets
 * @method processNodeWidgets
 **/
RGraphCanvas.prototype.processNodeWidgets = function(
  node,
  pos,
  event,
  active_widget
) {
  if (node.flags.collapsed || !node.widgets || !node.widgets.length) {
    return null;
  }

  var x = pos[0] - node.pos[0];
  var y = pos[1] - node.pos[1];
  var width = node.size[0];
  var that = this;
  var ref_window = this.getCanvasWindow();

  for (let i = 0; i < node.widgets.length; ++i) {
    var w = node.widgets[i];
    if (!w || w.disabled) continue;
    var widget_height = w.computeSize
      ? w.computeSize(width)[1]
      : NODE_WIDGET_HEIGHT;
    var widget_width = w.width || width;
    //outside
    if (
      w != active_widget &&
      (x < 6 ||
        x > widget_width - 12 ||
        y < w.last_y ||
        y > w.last_y + widget_height)
    )
      continue;

    var old_value = w.value;

    //if ( w == active_widget || (x > 6 && x < widget_width - 12 && y > w.last_y && y < w.last_y + widget_height) ) {
    //inside widget
    switch (w.type) {
    case "button":
      if (event.type == "mousemove") {
        break;
      }
      if (w.callback) {
        setTimeout(function() {
          w.callback(w, that, node, pos, event);
        }, 20);
      }
      w.clicked = true;
      this.dirty_canvas = true;
      break;
    case "slider":
      // var range = w.options.max - w.options.min;
      var nvalue = Math.clamp((x - 10) / (widget_width - 20), 0, 1);
      w.value = w.options.min + (w.options.max - w.options.min) * nvalue;
      if (w.callback) {
        setTimeout(function() {
          inner_value_change(w, w.value);
        }, 20);
      }
      this.dirty_canvas = true;
      break;
    case "number":
    case "combo": {
      let old_value = w.value;
      if (event.type == "mousemove" && w.type == "number") {
        w.value += event.deltaX * 0.1 * (w.options.step || 1);
        if (w.options.min != null && w.value < w.options.min) {
          w.value = w.options.min;
        }
        if (w.options.max != null && w.value > w.options.max) {
          w.value = w.options.max;
        }
      } else if (event.type == "mousedown") {
        let values = w.options.values;
        if (values && values.constructor == Function) {
          values = w.options.values(w, node);
        }
        let values_list = null;

        if (w.type != "number")
          values_list =
              values.constructor == Array ? values : Object.keys(values);

        let delta = x < 40 ? -1 : x > widget_width - 40 ? 1 : 0;
        if (w.type == "number") {
          w.value += delta * 0.1 * (w.options.step || 1);
          if (w.options.min != null && w.value < w.options.min) {
            w.value = w.options.min;
          }
          if (w.options.max != null && w.value > w.options.max) {
            w.value = w.options.max;
          }
        } else if (delta) {
          //clicked in arrow, used for combos
          let index = -1;
          this.last_mouseclick = 0; //avoids dobl click event
          if (values.constructor == Object)
            index = values_list.indexOf(String(w.value)) + delta;
          else index = values_list.indexOf(w.value) + delta;
          if (index >= values_list.length) {
            index = values_list.length - 1;
          }
          if (index < 0) {
            index = 0;
          }
          if (values.constructor == Array) w.value = values[index];
          else w.value = index;
        } else {
          //combo clicked
          let text_values =
              values != values_list ? Object.values(values) : values;
          new ContextMenu(
            text_values,
            {
              scale: Math.max(1, this.ds.scale),
              event: event,
              color_mode: this.color_mode,
              callback: inner_clicked.bind(w)
            },
            ref_window
          );
          /* eslint-disable-next-line no-inner-declarations */
          function inner_clicked(v /*, option, event*/) {
            if (values != values_list) v = text_values.indexOf(v);
            this.value = v;
            inner_value_change(this, v);
            that.dirty_canvas = true;
            return false;
          }
        }
      } //end mousedown
      else if (event.type == "mouseup" && w.type == "number") {
        var delta = x < 40 ? -1 : x > widget_width - 40 ? 1 : 0;
        if (event.click_time < 200 && delta == 0) {
          this.prompt(
            "Value",
            w.value,
            function(v) {
              this.value = Number(v);
              inner_value_change(this, this.value);
            }.bind(w),
            event
          );
        }
      }

      if (old_value != w.value)
        setTimeout(
          function() {
            inner_value_change(this, this.value);
          }.bind(w),
          20
        );
      this.dirty_canvas = true;
      break;
    }
    case "toggle":
      if (event.type == "mousedown") {
        w.value = !w.value;
        setTimeout(function() {
          inner_value_change(w, w.value);
        }, 20);
      }
      break;
    case "string":
    case "text":
      if (event.type == "mousedown") {
        this.prompt(
          "Value",
          w.value,
          function(v) {
            this.value = v;
            inner_value_change(this, v);
          }.bind(w),
          event
        );
      }
      break;
    default:
      if (w.mouse) {
        this.dirty_canvas = w.mouse(event, [x, y], node);
      }
      break;
    } //end switch

    //value changed
    if (old_value != w.value) {
      if (node.onWidgetChanged)
        node.onWidgetChanged(w.name, w.value, old_value, w);
      node.graph._version++;
    }

    return w;
  } //end for

  function inner_value_change(widget, value) {
    widget.value = value;
    if (
      widget.options &&
      widget.options.property &&
      node.properties[widget.options.property] != undefined
    ) {
      node.setProperty(widget.options.property, value);
    }
    if (widget.callback) {
      widget.callback(widget.value, that, node, pos, event);
    }
  }

  return null;
};

/**
 * draws every group area in the background
 * @method drawGroups
 **/
RGraphCanvas.prototype.drawGroups = function(canvas, ctx) {
  if (!this.graph) {
    return;
  }

  var groups = this.graph._groups;

  ctx.save();
  ctx.globalAlpha = 0.5 * this.editor_alpha;

  for (let i = 0; i < groups.length; ++i) {
    var group = groups[i];

    if (!overlapBounding(this.visible_area, group._bounding)) {
      continue;
    } //out of the visible area

    ctx.fillStyle = group.color || "#335";
    ctx.strokeStyle = group.color || "#335";
    var pos = group._pos;
    var size = group._size;
    ctx.globalAlpha = 0.25 * this.editor_alpha;
    ctx.beginPath();
    ctx.rect(pos[0] + 0.5, pos[1] + 0.5, size[0], size[1]);
    ctx.fill();
    ctx.globalAlpha = this.editor_alpha;
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(pos[0] + size[0], pos[1] + size[1]);
    ctx.lineTo(pos[0] + size[0] - 10, pos[1] + size[1]);
    ctx.lineTo(pos[0] + size[0], pos[1] + size[1] - 10);
    ctx.fill();

    var font_size = group.font_size || DEFAULT_GROUP_FONT_SIZE;
    ctx.font = font_size + "px Arial";
    ctx.fillText(group.title, pos[0] + 4, pos[1] + font_size);
  }

  ctx.restore();
};

RGraphCanvas.prototype.adjustNodesSize = function() {
  var nodes = this.graph._nodes;
  for (let i = 0; i < nodes.length; ++i) {
    nodes[i].size = nodes[i].computeSize();
  }
  this.setDirty(true, true);
};

/**
 * resizes the canvas to a given size, if no size is passed, then it tries to fill the parentNode
 * @method resize
 **/
RGraphCanvas.prototype.resize = function(width, height) {
  if (!width && !height) {
    var parent = this.canvas.parentNode;
    width = parent.offsetWidth;
    height = parent.offsetHeight;
  }

  if (this.canvas.width == width && this.canvas.height == height) {
    return;
  }

  this.canvas.width = width;
  this.canvas.height = height;
  this.bgcanvas.width = this.canvas.width;
  this.bgcanvas.height = this.canvas.height;
  this.setDirty(true, true);
};

/**
 * switches to live mode (node shapes are not rendered, only the content)
 * this feature was designed when graphs where meant to create user interfaces
 * @method switchLiveMode
 **/
RGraphCanvas.prototype.switchLiveMode = function(transition) {
  if (!transition) {
    this.live_mode = !this.live_mode;
    this.dirty_canvas = true;
    this.dirty_bgcanvas = true;
    return;
  }

  var self = this;
  var delta = this.live_mode ? 1.1 : 0.9;
  if (this.live_mode) {
    this.live_mode = false;
    this.editor_alpha = 0.1;
  }

  var t = setInterval(function() {
    self.editor_alpha *= delta;
    self.dirty_canvas = true;
    self.dirty_bgcanvas = true;

    if (delta < 1 && self.editor_alpha < 0.01) {
      clearInterval(t);
      if (delta < 1) {
        self.live_mode = true;
      }
    }
    if (delta > 1 && self.editor_alpha > 0.99) {
      clearInterval(t);
      self.editor_alpha = 1;
    }
  }, 1);
};

RGraphCanvas.prototype.onNodeSelectionChange = function(/*node*/) {
  return; //disabled
};

RGraphCanvas.prototype.touchHandler = function(event) {
  //alert("foo");
  var touches = event.changedTouches,
    first = touches[0],
    type = "";

  switch (event.type) {
  case "touchstart":
    type = "mousedown";
    break;
  case "touchmove":
    type = "mousemove";
    break;
  case "touchend":
    type = "mouseup";
    break;
  default:
    return;
  }

  //initMouseEvent(type, canBubble, cancelable, view, clickCount,
  //           screenX, screenY, clientX, clientY, ctrlKey,
  //           altKey, shiftKey, metaKey, button, relatedTarget);

  var window = this.getCanvasWindow();
  var document = window.document;

  var simulatedEvent = document.createEvent("MouseEvent");
  simulatedEvent.initMouseEvent(
    type,
    true,
    true,
    window,
    1,
    first.screenX,
    first.screenY,
    first.clientX,
    first.clientY,
    false,
    false,
    false,
    false,
    0 /*left*/,
    null
  );
  first.target.dispatchEvent(simulatedEvent);
  event.preventDefault();
};

/* CONTEXT MENU ********************/
/* eslint-disable-next-line no-unused-vars */
RGraphCanvas.onGroupAdd = function(info, entry, mouse_event) {
  var canvas = RGraphCanvas.active_canvas;
  // var ref_window = canvas.getCanvasWindow();

  var group = new RGraphGroup();
  group.pos = canvas.convertEventToCanvasOffset(mouse_event);
  canvas.graph.add(group);
};

/* eslint-disable-next-line no-unused-vars */
RGraphCanvas.onMenuAdd = function(node, options, e, prev_menu, callback) {
  var canvas = RGraphCanvas.active_canvas;
  var ref_window = canvas.getCanvasWindow();
  var graph = canvas.graph;
  if (!graph) return;

  var values = getNodeTypesCategories(canvas.filter || graph.filter);
  var entries = [];
  for (let i = 0; i < values.length; i++) {
    if (values[i]) {
      var name = values[i];
      if (name.indexOf("::") != -1)
        //in case it has a namespace like "shader::math/rand" it hides the namespace
        name = name.split("::")[1];
      entries.push({ value: values[i], content: name, has_submenu: true });
    }
  }

  //show categories
  const color_mode = options.color_mode;
  var menu = new ContextMenu(
    entries,
    {
      event: e,
      callback: inner_clicked,
      parentMenu: prev_menu,
      color_mode
    },
    ref_window
  );

  function inner_clicked(v, option, e) {
    var category = v.value;
    var node_types = getNodeTypesInCategory(
      category,
      canvas.filter || graph.filter
    );
    var values = [];
    for (let i = 0; i < node_types.length; i++) {
      if (!node_types[i].skip_list) {
        values.push({
          content: node_types[i].title,
          value: node_types[i].type
        });
      }
    }

    new ContextMenu(
      values,
      {
        event: e,
        callback: inner_create,
        parentMenu: menu,
        color_mode
      },
      ref_window
    );
    return false;
  }

  function inner_create(v) {
    var first_event = prev_menu.getFirstEvent();
    canvas.graph.beforeChange();
    var node = createNode(v.value);
    if (node) {
      node.pos = canvas.convertEventToCanvasOffset(first_event);
      canvas.graph.add(node);
    }
    if (callback) callback(node);
    canvas.graph.afterChange();
  }

  return false;
};

RGraphCanvas.onMenuCollapseAll = function() {};

RGraphCanvas.onMenuNodeEdit = function() {};

RGraphCanvas.showMenuNodeOptionalInputs = function(
  v,
  options,
  e,
  prev_menu,
  node
) {
  if (!node) {
    return;
  }

  var that = this;
  var canvas = RGraphCanvas.active_canvas;
  var ref_window = canvas.getCanvasWindow();

  options = node.optional_inputs;
  if (node.onGetInputs) {
    options = node.onGetInputs();
  }

  var entries = [];
  if (options) {
    for (let i = 0; i < options.length; i++) {
      var entry = options[i];
      if (!entry) {
        entries.push(null);
        continue;
      }
      var label = entry[0];
      if (entry[2] && entry[2].label) {
        label = entry[2].label;
      }
      var data = { content: label, value: entry };
      if (entry[1] == ACTION) {
        data.className = "event";
      }
      entries.push(data);
    }
  }

  if (this.onMenuNodeInputs) {
    entries = this.onMenuNodeInputs(entries);
  }

  if (!entries.length) {
    console.log("no input entries");
    return;
  }

  new ContextMenu(
    entries,
    {
      event: e,
      callback: inner_clicked,
      parentMenu: prev_menu,
      color_mode: options.color_mode,
      node: node
    },
    ref_window
  );

  function inner_clicked(v, e, prev) {
    if (!node) {
      return;
    }

    if (v.callback) {
      v.callback.call(that, node, v, e, prev);
    }

    if (v.value) {
      node.graph.beforeChange();
      node.addInput(v.value[0], v.value[1], v.value[2]);
      node.setDirtyCanvas(true, true);
      node.graph.afterChange();
    }
  }

  return false;
};

RGraphCanvas.showMenuNodeOptionalOutputs = function(
  v,
  options,
  e,
  prev_menu,
  node
) {
  if (!node) {
    return;
  }

  let that = this;
  let canvas = RGraphCanvas.active_canvas;
  let ref_window = canvas.getCanvasWindow();

  options = node.optional_outputs;
  if (node.onGetOutputs) {
    options = node.onGetOutputs();
  }

  let entries = [];
  if (options) {
    for (let i = 0; i < options.length; i++) {
      let entry = options[i];
      if (!entry) {
        //separator?
        entries.push(null);
        continue;
      }

      if (
        node.flags &&
        node.flags.skip_repeated_outputs &&
        node.findOutputSlot(entry[0]) != -1
      ) {
        continue;
      } //skip the ones already on
      let label = entry[0];
      if (entry[2] && entry[2].label) {
        label = entry[2].label;
      }
      let data = { content: label, value: entry };
      if (entry[1] == EVENT) {
        data.className = "event";
      }
      entries.push(data);
    }
  }

  if (this.onMenuNodeOutputs) {
    entries = this.onMenuNodeOutputs(entries);
  }

  if (!entries.length) {
    return;
  }

  const color_mode = options.color_mode;
  new ContextMenu(
    entries,
    {
      event: e,
      callback: inner_clicked,
      parentMenu: prev_menu,
      node: node,
      color_mode
    },
    ref_window
  );

  function inner_clicked(v, e, prev) {
    if (!node) {
      return;
    }

    if (v.callback) {
      v.callback.call(that, node, v, e, prev);
    }

    if (!v.value) {
      return;
    }

    let value = v.value[1];

    if (
      value &&
      (value.constructor == Object || value.constructor == Array)
    ) {
      //submenu why?
      let entries = [];
      for (let i in value) {
        entries.push({ content: i, value: value[i] });
      }
      new ContextMenu(entries, {
        event: e,
        callback: inner_clicked,
        parentMenu: prev_menu,
        node: node,
        color_mode
      });
      return false;
    } else {
      node.graph.beforeChange();
      node.addOutput(v.value[0], v.value[1], v.value[2]);
      node.setDirtyCanvas(true, true);
      node.graph.afterChange();
    }
  }

  return false;
};

RGraphCanvas.onShowMenuNodeProperties = function(
  value,
  options,
  e,
  prev_menu,
  node
) {
  if (!node || !node.properties) {
    return;
  }

  let canvas = RGraphCanvas.active_canvas;
  let ref_window = canvas.getCanvasWindow();

  let entries = [];
  for (let i in node.properties) {
    let value = node.properties[i] != undefined ? node.properties[i] : " ";
    if (typeof value == "object") value = JSON.stringify(value);
    let info = node.getPropertyInfo(i);
    if (info.type == "enum" || info.type == "combo")
      value = RGraphCanvas.getPropertyPrintableValue(value, info.values);

    //value could contain invalid html characters, clean that
    value = RGraphCanvas.decodeHTML(value);
    entries.push({
      content:
        "<span class='property_name'>" +
        (info.label ? info.label : i) +
        "</span>" +
        "<span class='property_value'>" +
        value +
        "</span>",
      value: i
    });
  }
  if (!entries.length) {
    return;
  }

  new ContextMenu(
    entries,
    {
      event: e,
      callback: inner_clicked,
      parentMenu: prev_menu,
      allow_html: true,
      color_mode: options.color_mode,
      node: node
    },
    ref_window
  );

  function inner_clicked(v /*, options, e, prev*/) {
    if (!node) {
      return;
    }
    var rect = this.getBoundingClientRect();
    canvas.showEditPropertyValue(node, v.value, {
      position: [rect.left, rect.top]
    });
  }

  return false;
};

RGraphCanvas.decodeHTML = function(str) {
  var e = document.createElement("div");
  e.innerText = str;
  return e.innerHTML;
};

/* eslint-disable-next-line no-unused-vars */
RGraphCanvas.onResizeNode = function(value, options, e, menu, node) {
  if (!node) {
    return;
  }
  node.size = node.computeSize();
  if (node.onResize) node.onResize(node.size);
  node.setDirtyCanvas(true, true);
};

RGraphCanvas.prototype.showLinkMenu = function(link, e) {
  var $this = this;
  var options = ["Add Node", null, "Delete"];
  const color_mode = this.color_mode;
  var menu = new ContextMenu(options, {
    event: e,
    title: link.data != null ? link.data.constructor.name : null,
    callback: inner_clicked,
    color_mode
  });

  /* eslint-disable-next-line no-unused-vars */
  function inner_clicked(v, options, e) {
    switch (v) {
    case "Add Node":
      RGraphCanvas.onMenuAdd(null, { color_mode }, e, menu, function(node) {
        if (!getOptions().allow_auto_connect) return;
        let node_left = $this.graph.getNodeById(link.origin_id);
        let node_right = $this.graph.getNodeById(link.target_id);
        $this.graph.tryReconnect(node, node_left, node_right);
      });
      break;
    case "Delete":
      $this.graph.removeLink(link.id);
      break;
    default:
    }
  }

  return false;
};

/* eslint-disable-next-line no-unused-vars */
RGraphCanvas.onShowPropertyEditor = function(item, options, e, menu, node) {
  var property = item.property || "title";
  var value = node[property];

  var dialog = document.createElement("div");
  dialog.className = "graphdialog";
  dialog.innerHTML =
    "<span class='name'></span><input autofocus type='text' class='value'/><button>OK</button>";
  var title = dialog.querySelector(".name");
  title.innerText = property;
  var input = dialog.querySelector("input");
  if (input) {
    input.value = value;
    input.addEventListener("blur", function() {
      this.focus();
    });
    input.addEventListener("keydown", function(e) {
      if (e.key == "Enter") {
        return;
      }
      inner();
      e.preventDefault();
      e.stopPropagation();
    });
  }

  var graphcanvas = RGraphCanvas.active_canvas;
  var canvas = graphcanvas.canvas;

  var rect = canvas.getBoundingClientRect();
  var offsetx = -20;
  var offsety = -20;
  if (rect) {
    offsetx -= rect.left;
    offsety -= rect.top;
  }

  if (event) {
    dialog.style.left = event.clientX + offsetx + "px";
    dialog.style.top = event.clientY + offsety + "px";
  } else {
    dialog.style.left = canvas.width * 0.5 + offsetx + "px";
    dialog.style.top = canvas.height * 0.5 + offsety + "px";
  }

  var button = dialog.querySelector("button");
  button.addEventListener("click", inner);
  canvas.parentNode.appendChild(dialog);

  function inner() {
    setValue(input.value);
  }

  function setValue(value) {
    if (item.type == "Number") {
      value = Number(value);
    } else if (item.type == "Boolean") {
      value = Boolean(value);
    }
    node[property] = value;
    if (dialog.parentNode) {
      dialog.parentNode.removeChild(dialog);
    }
    node.setDirtyCanvas(true, true);
  }
};

RGraphCanvas.prototype.prompt = function(title, value, callback, event) {
  var that = this;
  title = title || "";

  var modified = false;

  var dialog = document.createElement("div");
  dialog.className = "graphdialog rounded";
  dialog.innerHTML =
    "<span class='name'></span> <input autofocus type='text' class='value'/><button class='rounded'>OK</button>";
  dialog.close = function() {
    that.prompt_box = null;
    if (dialog.parentNode) {
      dialog.parentNode.removeChild(dialog);
    }
  };

  if (this.ds.scale > 1) {
    dialog.style.transform = "scale(" + this.ds.scale + ")";
  }

  dialog.addEventListener("mouseleave", function() {
    if (!modified) {
      dialog.close();
    }
  });

  if (that.prompt_box) {
    that.prompt_box.close();
  }
  that.prompt_box = dialog;

  var name_element = dialog.querySelector(".name");
  name_element.innerText = title;
  var value_element = dialog.querySelector(".value");
  value_element.value = value;

  var input = dialog.querySelector("input");
  input.addEventListener("keydown", function(e) {
    modified = true;
    if (e.keyCode == 27) {
      //ESC
      dialog.close();
    } else if (e.keyCode == 13) {
      if (callback) {
        callback(this.value);
      }
      dialog.close();
    } else {
      return;
    }
    e.preventDefault();
    e.stopPropagation();
  });

  var button = dialog.querySelector("button");
  button.addEventListener("click", function() {
    if (callback) {
      callback(input.value);
    }
    that.setDirty(true);
    dialog.close();
  });

  var graphcanvas = RGraphCanvas.active_canvas;
  var canvas = graphcanvas.canvas;

  var rect = canvas.getBoundingClientRect();
  var offsetx = -20;
  var offsety = -20;
  if (rect) {
    offsetx -= rect.left;
    offsety -= rect.top;
  }

  if (event) {
    dialog.style.left = event.clientX + offsetx + "px";
    dialog.style.top = event.clientY + offsety + "px";
  } else {
    dialog.style.left = canvas.width * 0.5 + offsetx + "px";
    dialog.style.top = canvas.height * 0.5 + offsety + "px";
  }

  canvas.parentNode.appendChild(dialog);
  setTimeout(function() {
    input.focus();
  }, 10);

  return dialog;
};

RGraphCanvas.search_limit = -1;
RGraphCanvas.prototype.showSearchBox = function(event) {
  var that = this;
  var graphcanvas = RGraphCanvas.active_canvas;
  var canvas = graphcanvas.canvas;
  var root_document = canvas.ownerDocument || document;

  var dialog = document.createElement("div");
  dialog.className = "runegraph runesearchbox graphdialog rounded";
  dialog.innerHTML =
    "<span class='name'>Search</span> <input autofocus type='text' class='value rounded'/><div class='helper'></div>";
  dialog.close = function() {
    that.search_box = null;
    root_document.body.focus();
    root_document.body.style.overflow = "";

    setTimeout(function() {
      that.canvas.focus();
    }, 20); //important, if canvas loses focus keys wont be captured
    if (dialog.parentNode) {
      dialog.parentNode.removeChild(dialog);
    }
  };

  var timeout_close = null;

  if (this.ds.scale > 1) {
    dialog.style.transform = "scale(" + this.ds.scale + ")";
  }

  dialog.addEventListener("mouseenter", function() {
    if (timeout_close) {
      clearTimeout(timeout_close);
      timeout_close = null;
    }
  });

  dialog.addEventListener("mouseleave", function() {
    //dialog.close();
    timeout_close = setTimeout(function() {
      dialog.close();
    }, 500);
  });

  if (that.search_box) {
    that.search_box.close();
  }
  that.search_box = dialog;

  var helper = dialog.querySelector(".helper");

  var first = null;
  var timeout = null;
  var selected = null;

  var input = dialog.querySelector("input");
  if (input) {
    input.addEventListener("blur", function() {
      this.focus();
    });
    input.addEventListener("keydown", function(e) {
      if (e.keyCode == 38) {
        //UP
        changeSelection(false);
      } else if (e.keyCode == 40) {
        //DOWN
        changeSelection(true);
      } else if (e.keyCode == 27) {
        //ESC
        dialog.close();
      } else if (e.keyCode == 13) {
        if (selected) {
          select(selected.innerHTML);
        } else if (first) {
          select(first);
        } else {
          dialog.close();
        }
      } else {
        if (timeout) {
          clearInterval(timeout);
        }
        timeout = setTimeout(refreshHelper, 10);
        return;
      }
      e.preventDefault();
      e.stopPropagation();
      e.stopImmediatePropagation();
      return true;
    });
  }

  if (root_document.fullscreenElement)
    root_document.fullscreenElement.appendChild(dialog);
  else {
    root_document.body.appendChild(dialog);
    root_document.body.style.overflow = "hidden";
  }

  //compute best position
  var rect = canvas.getBoundingClientRect();

  var left = (event ? event.clientX : rect.left + rect.width * 0.5) - 80;
  var top = (event ? event.clientY : rect.top + rect.height * 0.5) - 20;
  dialog.style.left = left + "px";
  dialog.style.top = top + "px";

  //To avoid out of screen problems
  if (event.layerY > rect.height - 200)
    helper.style.maxHeight = rect.height - event.layerY - 20 + "px";

  /*
    var offsetx = -20;
    var offsety = -20;
    if (rect) {
        offsetx -= rect.left;
        offsety -= rect.top;
    }
    if (event) {
        dialog.style.left = event.clientX + offsetx + "px";
        dialog.style.top = event.clientY + offsety + "px";
    } else {
        dialog.style.left = canvas.width * 0.5 + offsetx + "px";
        dialog.style.top = canvas.height * 0.5 + offsety + "px";
    }
    canvas.parentNode.appendChild(dialog);
    */

  input.focus();

  function select(name) {
    if (name) {
      if (that.onSearchBoxSelection) {
        that.onSearchBoxSelection(name, event, graphcanvas);
      } else {
        var extra = getSearchboxExtras()[name.toLowerCase()];
        if (extra) {
          name = extra.type;
        }

        graphcanvas.graph.beforeChange();
        var node = createNode(name);
        if (node) {
          node.pos = graphcanvas.convertEventToCanvasOffset(event);
          graphcanvas.graph.add(node);
        }

        if (extra && extra.data) {
          if (extra.data.properties) {
            for (let i in extra.data.properties) {
              node.addProperty(i, extra.data.properties[i]);
            }
          }
          if (extra.data.inputs) {
            node.inputs = [];
            for (let i in extra.data.inputs) {
              node.addOutput(extra.data.inputs[i][0], extra.data.inputs[i][1]);
            }
          }
          if (extra.data.outputs) {
            node.outputs = [];
            for (let i in extra.data.outputs) {
              node.addOutput(
                extra.data.outputs[i][0],
                extra.data.outputs[i][1]
              );
            }
          }
          if (extra.data.title) {
            node.title = extra.data.title;
          }
          if (extra.data.json) {
            node.configure(extra.data.json);
          }

          graphcanvas.graph.afterChange();
        }
      }
    }

    dialog.close();
  }

  function changeSelection(forward) {
    var prev = selected;
    if (selected) {
      selected.classList.remove("selected");
    }
    if (!selected) {
      selected = forward
        ? helper.childNodes[0]
        : helper.childNodes[helper.childNodes.length];
    } else {
      selected = forward ? selected.nextSibling : selected.previousSibling;
      if (!selected) {
        selected = prev;
      }
    }
    if (!selected) {
      return;
    }
    selected.classList.add("selected");
    selected.scrollIntoView({ block: "end", behavior: "smooth" });
  }

  function refreshHelper() {
    timeout = null;
    let str = input.value;
    first = null;
    helper.innerHTML = "";
    if (!str) {
      return;
    }

    if (that.onSearchBox) {
      let list = that.onSearchBox(helper, str, graphcanvas);
      if (list) {
        for (let i = 0; i < list.length; ++i) {
          addResult(list[i]);
        }
      }
    } else {
      let c = 0;
      str = str.toLowerCase();
      let filter = graphcanvas.filter || graphcanvas.graph.filter;

      //extras
      for (let i in getSearchboxExtras()) {
        let extra = getSearchboxExtras()[i];
        if (extra.desc.toLowerCase().indexOf(str) == -1) {
          continue;
        }
        let ctor = getRegisteredNodeTypes()[extra.type];
        if (ctor && ctor.filter != filter) continue;
        addResult(extra.desc, "searchbox_extra");
        if (
          RGraphCanvas.search_limit != -1 &&
          c++ > RGraphCanvas.search_limit
        ) {
          break;
        }
      }

      let filtered = null;
      if (Array.prototype.filter) {
        //filter supported
        let keys = Object.keys(getRegisteredNodeTypes()); //types
        filtered = keys.filter(inner_test_filter);
      } else {
        filtered = [];
        for (let i in getRegisteredNodeTypes()) {
          if (inner_test_filter(i)) filtered.push(i);
        }
      }

      for (let i = 0; i < filtered.length; i++) {
        addResult(filtered[i]);
        if (
          RGraphCanvas.search_limit != -1 &&
          c++ > RGraphCanvas.search_limit
        ) {
          break;
        }
      }
      /* eslint-disable-next-line no-inner-declarations */
      function inner_test_filter(type) {
        let ctor = getRegisteredNodeTypes()[type];
        if (filter && ctor.filter != filter) return false;
        return type.toLowerCase().indexOf(str) != -1;
      }
    }

    function addResult(type, className) {
      var help = document.createElement("div");
      if (!first) {
        first = type;
      }
      help.innerText = type;
      help.dataset["type"] = escape(type);
      help.className = "runegraph rune-search-item";
      if (className) {
        help.className += " " + className;
      }
      help.addEventListener("click", function() {
        select(unescape(this.dataset["type"]));
      });
      helper.appendChild(help);
    }
  }

  return dialog;
};

RGraphCanvas.prototype.showEditPropertyValue = function(
  node,
  property,
  options
) {
  if (!node || node.properties[property] == undefined) {
    return;
  }

  options = options || {};
  let info = node.getPropertyInfo(property);
  let type = info.type;

  let input_html = "";

  if (
    type == "string" ||
    type == "number" ||
    type == "array" ||
    type == "object"
  ) {
    input_html = "<input autofocus type='text' class='value'/>";
  } else if ((type == "enum" || type == "combo") && info.values) {
    input_html = "<select autofocus type='text' class='value'>";
    for (let i in info.values) {
      var v = i;
      if (info.values.constructor == Array) v = info.values[i];

      input_html +=
        "<option value='" +
        v +
        "' " +
        (v == node.properties[property] ? "selected" : "") +
        ">" +
        info.values[i] +
        "</option>";
    }
    input_html += "</select>";
  } else if (type == "boolean") {
    input_html =
      "<input autofocus type='checkbox' class='value' " +
      (node.properties[property] ? "checked" : "") +
      "/>";
  } else {
    console.warn("unknown type: " + type);
    return;
  }

  let dialog = this.createDialog(
    "<span class='name'>" +
      (info.label ? info.label : property) +
      "</span>" +
      input_html +
      "<button>OK</button>",
    options
  );

  let input;
  if ((type == "enum" || type == "combo") && info.values) {
    input = dialog.querySelector("select");
    input.addEventListener("change", function(e) {
      setValue(e.target.value);
      //var index = e.target.value;
      //setValue( e.options[e.selectedIndex].value );
    });
  } else if (type == "boolean") {
    input = dialog.querySelector("input");
    if (input) {
      input.addEventListener("click", function() {
        setValue(!!input.checked);
      });
    }
  } else {
    input = dialog.querySelector("input");
    if (input) {
      input.addEventListener("blur", function() {
        this.focus();
      });

      let v =
        node.properties[property] != undefined
          ? node.properties[property]
          : "";
      if (type != "string") {
        v = JSON.stringify(v);
      }

      input.value = v;
      input.addEventListener("keydown", function(e) {
        if (e.keyCode != 13) {
          return;
        }
        inner();
        e.preventDefault();
        e.stopPropagation();
      });
    }
  }

  let button = dialog.querySelector("button");
  button.addEventListener("click", inner);

  function inner() {
    setValue(input.value);
  }

  function setValue(value) {
    if (
      info &&
      info.values &&
      info.values.constructor == Object &&
      info.values[value] != undefined
    )
      value = info.values[value];

    if (typeof node.properties[property] == "number") {
      value = Number(value);
    }
    if (type == "array" || type == "object") {
      value = JSON.parse(value);
    }
    node.properties[property] = value;
    if (node.graph) {
      node.graph._version++;
    }
    if (node.onPropertyChanged) {
      node.onPropertyChanged(property, value);
    }
    if (options.onclose) options.onclose();
    dialog.close();
    node.setDirtyCanvas(true, true);
  }

  return dialog;
};

RGraphCanvas.prototype.createDialog = function(html, options) {
  options = options || {};

  let dialog = document.createElement("div");
  dialog.className = "graphdialog";
  dialog.innerHTML = html;

  let rect = this.canvas.getBoundingClientRect();
  let offsetx = -20;
  let offsety = -20;
  if (rect) {
    offsetx -= rect.left;
    offsety -= rect.top;
  }

  if (options.position) {
    offsetx += options.position[0];
    offsety += options.position[1];
  } else if (options.event) {
    offsetx += options.event.clientX;
    offsety += options.event.clientY;
  } //centered
  else {
    offsetx += this.canvas.width * 0.5;
    offsety += this.canvas.height * 0.5;
  }

  dialog.style.left = offsetx + "px";
  dialog.style.top = offsety + "px";

  this.canvas.parentNode.appendChild(dialog);

  dialog.close = function() {
    if (this.parentNode) {
      this.parentNode.removeChild(this);
    }
  };

  return dialog;
};

RGraphCanvas.prototype.createPanel = function(title, options) {
  options = options || {};

  let ref_window = options.window || window;
  let root = document.createElement("div");
  root.className = "runegraph dialog";
  root.innerHTML =
    "<div class='dialog-header'><span class='dialog-title'></span></div><div class='dialog-content'></div><div class='dialog-footer'></div>";
  root.header = root.querySelector(".dialog-header");

  if (options.width)
    root.style.width =
      options.width + (options.width.constructor == Number ? "px" : "");
  if (options.height)
    root.style.height =
      options.height + (options.height.constructor == Number ? "px" : "");
  if (options.closable) {
    let close = document.createElement("span");
    close.innerHTML = "&#10005;";
    close.classList.add("close");
    close.addEventListener("click", function() {
      root.close();
    });
    root.header.appendChild(close);
  }
  root.title_element = root.querySelector(".dialog-title");
  root.title_element.innerText = title;
  root.content = root.querySelector(".dialog-content");
  root.footer = root.querySelector(".dialog-footer");

  root.close = function() {
    this.parentNode.removeChild(this);
  };

  root.clear = function() {
    this.content.innerHTML = "";
  };

  root.addHTML = function(code, classname, on_footer) {
    let elem = document.createElement("div");
    if (classname) elem.className = classname;
    elem.innerHTML = code;
    if (on_footer) root.footer.appendChild(elem);
    else root.contentappendChild(elem);
    return elem;
  };

  root.addButton = function(name, callback, options) {
    let elem = document.createElement("button");
    elem.innerText = name;
    elem.options = options;
    elem.classList.add("btn");
    elem.addEventListener("click", callback);
    root.footer.appendChild(elem);
    return elem;
  };

  root.addSeparator = function() {
    let elem = document.createElement("div");
    elem.className = "separator";
    root.content.appendChild(elem);
  };

  root.addWidget = function(type, name, value, options, callback) {
    options = options || {};
    let str_value = String(value);
    type = type.toLowerCase();
    if (type == "number") str_value = value.toFixed(2);

    let elem = document.createElement("div");
    elem.className = "property";
    elem.innerHTML =
      "<span class='property_name'></span><span class='property_value'></span>";
    elem.querySelector(".property_name").innerText = name;
    let value_element = elem.querySelector(".property_value");
    value_element.innerText = str_value;
    elem.dataset["property"] = name;
    elem.dataset["type"] = options.type || type;
    elem.options = options;
    elem.value = value;

    //if( type == "code" )
    //	elem.addEventListener("click", function(){ inner_showCodePad( node, this.dataset["property"] ); });
    if (type == "boolean") {
      elem.classList.add("boolean");
      if (value) elem.classList.add("bool-on");
      elem.addEventListener("click", function() {
        //var v = node.properties[this.dataset["property"]];
        //node.setProperty(this.dataset["property"],!v); this.innerText = v ? "true" : "false";
        let propname = this.dataset["property"];
        this.value = !this.value;
        this.classList.toggle("bool-on");
        this.querySelector(".property_value").innerText = this.value
          ? "true"
          : "false";
        innerChange(propname, this.value);
      });
    } else if (type == "string" || type == "number") {
      value_element.setAttribute("contenteditable", true);
      value_element.addEventListener("keydown", function(e) {
        if (e.code == "Enter") {
          e.preventDefault();
          this.blur();
        }
      });
      value_element.addEventListener("blur", function() {
        let v = this.innerText;
        let propname = this.parentNode.dataset["property"];
        let proptype = this.parentNode.dataset["type"];
        if (proptype == "number") v = Number(v);
        innerChange(propname, v);
      });
    } else if (type == "enum" || type == "combo")
      str_value = RGraphCanvas.getPropertyPrintableValue(value, options.values);
    value_element.innerText = str_value;

    const color_mode = this.color_mode;
    value_element.addEventListener("click", function(event) {
      let values = options.values || [];
      let propname = this.parentNode.dataset["property"];
      let elem_that = this;
      new ContextMenu(
        values,
        {
          event: event,
          color_mode,
          callback: inner_clicked
        },
        ref_window
      );
      function inner_clicked(v /*, option, event*/) {
        //node.setProperty(propname,v);
        //graphcanvas.dirty_canvas = true;
        elem_that.innerText = v;
        innerChange(propname, v);
        return false;
      }
    });

    root.content.appendChild(elem);

    function innerChange(name, value) {
      console.log("change", name, value);
      //that.dirty_canvas = true;
      if (options.callback) options.callback(name, value);
      if (callback) callback(name, value);
    }

    return elem;
  };

  return root;
};

RGraphCanvas.getPropertyPrintableValue = function(value, values) {
  if (!values) return String(value);

  if (values.constructor == Array) {
    return String(value);
  }

  if (values.constructor == Object) {
    var desc_value = "";
    for (let k in values) {
      if (values[k] != value) continue;
      desc_value = k;
      break;
    }
    return String(value) + " (" + desc_value + ")";
  }
};

RGraphCanvas.prototype.showShowNodePanel = function(node) {
  window.SELECTED_NODE = node;
  var panel = document.querySelector("#node-panel");
  if (panel) panel.close();
  var ref_window = this.getCanvasWindow();
  panel = this.createPanel(node.title || "", {
    closable: true,
    window: ref_window
  });
  panel.id = "node-panel";
  panel.node = node;
  panel.classList.add("settings");
  let graphcanvas = this;

  function inner_refresh() {
    panel.content.innerHTML = ""; //clear
    panel.addHTML(
      "<span class='node_type'>" +
        node.type +
        "</span><span class='node_desc'>" +
        (node.constructor.desc || "") +
        "</span><span class='separator'></span>"
    );

    panel.addHTML("<h3>Properties</h3>");

    for (let i in node.properties) {
      let value = node.properties[i];
      let info = node.getPropertyInfo(i);

      //in case the user wants control over the side panel widget
      if (node.onAddPropertyToPanel && node.onAddPropertyToPanel(i, panel))
        continue;

      panel.addWidget(info.widget || info.type, i, value, info, function(
        name,
        value
      ) {
        graphcanvas.graph.beforeChange(node);
        node.setProperty(name, value);
        graphcanvas.graph.afterChange();
        graphcanvas.dirty_canvas = true;
      });
    }

    panel.addSeparator();

    if (node.onShowCustomPanelInfo) node.onShowCustomPanelInfo(panel);

    panel
      .addButton("Delete", function() {
        if (node.block_delete) return;
        node.graph.remove(node);
        panel.close();
      })
      .classList.add("delete");
  }

  inner_refresh();

  this.canvas.parentNode.appendChild(panel);
};

RGraphCanvas.prototype.checkPanels = function() {
  if (!this.canvas) return;
  var panels = this.canvas.parentNode.querySelectorAll(".runegraph.dialog");
  for (let i = 0; i < panels.length; ++i) {
    var panel = panels[i];
    if (!panel.node) continue;
    if (!panel.node.graph || panel.graph != this.graph) panel.close();
  }
};

/* eslint-disable-next-line no-unused-vars */
RGraphCanvas.onMenuNodeCollapse = function(value, options, e, menu, node) {
  node.graph.beforeChange(node);
  node.collapse();
  node.graph.afterChange(node);
};

/* eslint-disable-next-line no-unused-vars */
RGraphCanvas.onMenuNodePin = function(value, options, e, menu, node) {
  node.pin();
};

/* eslint-disable-next-line no-unused-vars */
RGraphCanvas.onMenuNodeMode = function(value, options, e, menu, node) {
  new ContextMenu(["Always", "On Event", "On Trigger", "Never"], {
    event: e,
    callback: inner_clicked,
    parentMenu: menu,
    color_mode: options.color_mode,
    node: node
  });

  function inner_clicked(v) {
    if (!node) {
      return;
    }
    switch (v) {
    case "On Event":
      node.mode = ON_EVENT;
      break;
    case "On Trigger":
      node.mode = ON_TRIGGER;
      break;
    case "Never":
      node.mode = NEVER;
      break;
    case "Always":
    default:
      node.mode = ALWAYS;
      break;
    }
  }

  return false;
};

/* eslint-disable-next-line no-unused-vars */
RGraphCanvas.onMenuNodeColors = function(value, options, e, menu, node) {
  if (!node) {
    throw "no node for color";
  }

  let values = [];
  values.push({
    value: null,
    content: "<span style='display: block; padding-left: 4px;'>No color</span>"
  });

  for (let i in RGraphCanvas.node_colors) {
    let color = RGraphCanvas.node_colors[i];
    let v = {
      value: i,
      content:
        "<span style='display: block; color: #999; padding-left: 4px; border-left: 8px solid " +
        color.color +
        "; background-color:" +
        color.bgcolor +
        "'>" +
        i +
        "</span>"
    };
    values.push(v);
  }
  new ContextMenu(values, {
    event: e,
    callback: inner_clicked,
    parentMenu: menu,
    color_mode: options.color_mode,
    node: node
  });

  function inner_clicked(v) {
    if (!node) {
      return;
    }

    var color = v.value ? RGraphCanvas.node_colors[v.value] : null;
    if (color) {
      if (node.constructor == RGraphGroup) {
        node.color = color.groupcolor;
      } else {
        node.color = color.color;
        node.bgcolor = color.bgcolor;
      }
    } else {
      delete node.color;
      delete node.bgcolor;
    }
    node.setDirtyCanvas(true, true);
  }

  return false;
};

/* eslint-disable-next-line no-unused-vars */
RGraphCanvas.onMenuNodeShapes = function(value, options, e, menu, node) {
  if (!node) {
    throw "no node passed";
  }

  new ContextMenu(VALID_SHAPES, {
    event: e,
    callback: inner_clicked,
    parentMenu: menu,
    color_mode: options.color_mode,
    node: node
  });

  function inner_clicked(v) {
    if (!node) {
      return;
    }
    node.graph.beforeChange(node);
    node.shape = v;
    node.graph.afterChange(node);
    node.setDirtyCanvas(true);
  }

  return false;
};

RGraphCanvas.onMenuNodeRemove = function(value, options, e, menu, node) {
  if (!node) {
    throw "no node passed";
  }

  if (node.removable == false) {
    return;
  }

  var graph = node.graph;
  graph.beforeChange();
  graph.remove(node);
  graph.afterChange();
  node.setDirtyCanvas(true, true);
};

RGraphCanvas.onMenuNodeClone = function(value, options, e, menu, node) {
  if (node.clonable == false) {
    return;
  }
  var newnode = node.clone();
  if (!newnode) {
    return;
  }
  newnode.pos = [node.pos[0] + 5, node.pos[1] + 5];

  node.graph.beforeChange();
  node.graph.add(newnode);
  node.graph.afterChange();

  node.setDirtyCanvas(true, true);
};

RGraphCanvas.node_colors = {
  red: { color: "#322", bgcolor: "#533", groupcolor: "#A88" },
  brown: { color: "#332922", bgcolor: "#593930", groupcolor: "#b06634" },
  green: { color: "#232", bgcolor: "#353", groupcolor: "#8A8" },
  blue: { color: "#223", bgcolor: "#335", groupcolor: "#88A" },
  pale_blue: {
    color: "#2a363b",
    bgcolor: "#3f5159",
    groupcolor: "#3f789e"
  },
  cyan: { color: "#233", bgcolor: "#355", groupcolor: "#8AA" },
  purple: { color: "#323", bgcolor: "#535", groupcolor: "#a1309b" },
  yellow: { color: "#432", bgcolor: "#653", groupcolor: "#b58b2a" },
  black: { color: "#222", bgcolor: "#000", groupcolor: "#444" }
};

RGraphCanvas.prototype.getCanvasMenuOptions = function() {
  var options = null;
  if (this.getMenuOptions) {
    options = this.getMenuOptions();
  } else {
    options = [
      {
        content: "Add Node",
        has_submenu: true,
        callback: RGraphCanvas.onMenuAdd
      },
      { content: "Add Group", callback: RGraphCanvas.onGroupAdd }
      //{content:"Collapse All", callback: RGraphCanvas.onMenuCollapseAll }
    ];
  }

  if (this.getExtraMenuOptions) {
    var extra = this.getExtraMenuOptions(this, options);
    if (extra) {
      options = options.concat(extra);
    }
  }

  return options;
};

//called by processContextMenu to extract the menu list
RGraphCanvas.prototype.getNodeMenuOptions = function(node) {
  var options = null;

  if (node.getMenuOptions) {
    options = node.getMenuOptions(this);
  } else {
    options = [
      {
        content: "Inputs",
        has_submenu: true,
        disabled: true,
        callback: RGraphCanvas.showMenuNodeOptionalInputs
      },
      {
        content: "Outputs",
        has_submenu: true,
        disabled: true,
        callback: RGraphCanvas.showMenuNodeOptionalOutputs
      },
      null,
      {
        content: "Properties",
        has_submenu: true,
        callback: RGraphCanvas.onShowMenuNodeProperties
      },
      null,
      {
        content: "Title",
        callback: RGraphCanvas.onShowPropertyEditor
      },
      {
        content: "Mode",
        has_submenu: true,
        callback: RGraphCanvas.onMenuNodeMode
      },
      {
        content: "Resize",
        callback: function() {
          if (node.resizable) return RGraphCanvas.onResizeNode;
        }
      },
      {
        content: "Collapse",
        callback: RGraphCanvas.onMenuNodeCollapse
      },
      { content: "Pin", callback: RGraphCanvas.onMenuNodePin },
      {
        content: "Colors",
        has_submenu: true,
        callback: RGraphCanvas.onMenuNodeColors
      },
      {
        content: "Shapes",
        has_submenu: true,
        callback: RGraphCanvas.onMenuNodeShapes
      },
      null
    ];
  }

  if (node.onGetInputs) {
    var inputs = node.onGetInputs();
    if (inputs && inputs.length) {
      options[0].disabled = false;
    }
  }

  if (node.onGetOutputs) {
    var outputs = node.onGetOutputs();
    if (outputs && outputs.length) {
      options[1].disabled = false;
    }
  }

  if (node.getExtraMenuOptions) {
    var extra = node.getExtraMenuOptions(this, options);
    if (extra) {
      extra.push(null);
      options = extra.concat(options);
    }
  }

  if (node.clonable != false) {
    options.push({
      content: "Clone",
      callback: RGraphCanvas.onMenuNodeClone
    });
  }

  options.push(null, {
    content: "Remove",
    disabled: !(node.removable != false && !node.block_delete),
    callback: RGraphCanvas.onMenuNodeRemove
  });

  if (node.graph && node.graph.onGetNodeMenuOptions) {
    node.graph.onGetNodeMenuOptions(options, node);
  }

  return options;
};

RGraphCanvas.prototype.getGroupMenuOptions = function(/*node*/) {
  var o = [
    { content: "Title", callback: RGraphCanvas.onShowPropertyEditor },
    {
      content: "Color",
      has_submenu: true,
      callback: RGraphCanvas.onMenuNodeColors
    },
    {
      content: "Font size",
      property: "font_size",
      type: "Number",
      callback: RGraphCanvas.onShowPropertyEditor
    },
    null,
    { content: "Remove", callback: RGraphCanvas.onMenuNodeRemove }
  ];

  return o;
};

RGraphCanvas.prototype.processContextMenu = function(node, event) {
  var that = this;
  var canvas = RGraphCanvas.active_canvas;
  var ref_window = canvas.getCanvasWindow();

  var menu_info = null;
  var options = {
    event: event,
    callback: inner_option_clicked,
    color_mode: this.color_mode,
    extra: node
  };

  if (node) options.title = node.type;

  //check if mouse is in input
  var slot = null;
  if (node) {
    slot = node.getSlotInPosition(
      event.canvasX,
      event.canvasY,
      this.anchor_point
    );
    RGraphCanvas.active_node = node;
  }

  if (slot && this.allow_link_interaction) {
    //on slot
    menu_info = [];
    if (node.getSlotMenuOptions) {
      menu_info = node.getSlotMenuOptions(slot);
    } else {
      if (
        slot &&
        slot.output &&
        slot.output.links &&
        slot.output.links.length
      ) {
        menu_info.push({ content: "Disconnect Links", slot: slot });
      }
      var _slot = slot.input || slot.output;
      menu_info.push(
        _slot.locked ? "Cannot remove" : { content: "Remove Slot", slot: slot }
      );
      menu_info.push(
        _slot.nameLocked
          ? "Cannot rename"
          : { content: "Rename Slot", slot: slot }
      );
    }
    options.title = (slot.input ? slot.input.type : slot.output.type) || "*";
    if (slot.input && slot.input.type == ACTION) {
      options.title = "Action";
    }
    if (slot.output && slot.output.type == EVENT) {
      options.title = "Event";
    }
  } else {
    if (node) {
      //on node
      menu_info = this.getNodeMenuOptions(node);
    } else {
      menu_info = this.getCanvasMenuOptions();
      var group = this.graph.getGroupOnPos(event.canvasX, event.canvasY);
      if (group) {
        //on group
        menu_info.push(null, {
          content: "Edit Group",
          has_submenu: true,
          submenu: {
            title: "Group",
            extra: group,
            options: this.getGroupMenuOptions(group)
          }
        });
      }
    }
  }

  //show menu
  if (!menu_info) {
    return;
  }

  new ContextMenu(menu_info, options, ref_window);

  function inner_option_clicked(v, options) {
    if (!v) {
      return;
    }

    let info;
    if (v.content == "Remove Slot") {
      info = v.slot;
      if (info.input) {
        node.removeInput(info.slot);
      } else if (info.output) {
        node.removeOutput(info.slot);
      }
      return;
    } else if (v.content == "Disconnect Links") {
      info = v.slot;
      if (info.output) {
        node.disconnectOutput(info.slot);
      } else if (info.input) {
        node.disconnectInput(info.slot);
      }
      return;
    } else if (v.content == "Rename Slot") {
      info = v.slot;
      let slot_info = info.input
        ? node.getInputInfo(info.slot)
        : node.getOutputInfo(info.slot);
      let dialog = that.createDialog(
        "<span class='name'>Name</span><input autofocus type='text'/><button>OK</button>",
        options
      );
      let input = dialog.querySelector("input");
      if (input && slot_info) {
        input.value = slot_info.label || "";
      }
      dialog.querySelector("button").addEventListener("click", function() {
        if (input.value) {
          if (slot_info) {
            slot_info.label = input.value;
          }
          that.setDirty(true);
        }
        dialog.close();
      });
    }

    //if(v.callback)
    //	return v.callback.call(that, node, options, e, menu, that, event );
  }
};

module.exports = RGraphCanvas;
