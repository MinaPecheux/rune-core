const { getOptions, setOption, setOptions } = require("./config.js");
const RGraph = require("./graph.js");
const RGraphCanvas = require("./canvas.js");
const RGraphLayout = require("./layout.js");
const { RGraphNode, initNode, createNode, addNodeMethod, reloadNodes } = require("./node.js");
const RGraphNodeTypes = require("./node-types.js");
const RGraphTags = require("./tags.js");
const RGraphEnums = require("./helpers/enums.js");
const RGraphShapes = require("./helpers/shapes.js");
const RGraphUtils = require("./helpers/utils.js");

const basic = require("../tags/basic.js");

require("../styles/main.css");

module.exports = {
  Graph: RGraph,
  Canvas: RGraphCanvas,
  nodes: {
    Node: RGraphNode.default,
    initNode,
    createNode,
    addNodeMethod,
    reloadNodes
  },
  layouts: RGraphLayout,
  tags: RGraphTags,
  types: RGraphNodeTypes,
  enums: RGraphEnums,
  shapes: RGraphShapes,
  utils: RGraphUtils,
  getOptions,
  setOption,
  setOptions,
  basic,
  VERSION: 1.0
};
